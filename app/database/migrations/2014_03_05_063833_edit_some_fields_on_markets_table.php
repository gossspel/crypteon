<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditSomeFieldsOnMarketsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('markets', function(Blueprint $table)
		{
            $table->dropColumn('buy_coin');
            $table->dropColumn('sell_coin');
            $table->string('coin', 20);
            $table->string('base', 20);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('markets', function(Blueprint $table)
		{
            $table->dropColumn('coin');
            $table->dropColumn('base');
            $table->string('buy_coin', 20);
            $table->string('sell_coin', 20);
		});
	}

}