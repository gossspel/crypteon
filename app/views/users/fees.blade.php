<div class="row">
    <div class="col-lg-12 basic-mainframe alert-holder">
        <!-- Include Alert Message here, better than showing it in the layout to more easily control css -->
        @include('alert')
        <div class="alert alert-success alert-dismissable row-alert" id="alert-container">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            Crypteon is operating in private beta mode with 0.00% trade fee until all legal matters of business establishment are taken care of.
            {{ HTML::link('https://bitcointalk.org/index.php?topic=605486.0', 'Check out our Bitcointalk thread', array('target'=>'_blank')) }}
        </div>
    </div>
    <div class="col-lg-12">
        <div class="main-title">
            Crypteon Fees
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading div-form-title">Trading Fees</div>
            <div>
                <table class="table table-striped table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>Market</th>
                            <th>Buy Fee</th>
                            <th>Sell Fee</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="buy_order">
                            <td>CINNI/BTC</td>
                            <td>0.00%</td>
                            <td>0.00%</td>
                        </tr>
                        <tr class="buy_order">
                            <td>DOGE/BTC</td>
                            <td>0.00%</td>
                            <td>0.00%</td>
                        </tr>
                        <tr class="buy_order">
                            <td>DRK/BTC</td>
                            <td>0.00%</td>
                            <td>0.00%</td>
                        </tr>
                        <tr class="buy_order">
                            <td>LTC/BTC</td>
                            <td>0.00%</td>
                            <td>0.00%</td>
                        </tr>
                        <tr class="buy_order">
                            <td>KIWI/BTC</td>
                            <td>0.00%</td>
                            <td>0.00%</td>
                        </tr>
                        <tr class="buy_order">
                            <td>KORE/BTC</td>
                            <td>0.00%</td>
                            <td>0.00%</td>
                        </tr>
                        <tr class="buy_order">
                            <td>OZC/BTC</td>
                            <td>0.00%</td>
                            <td>0.00%</td>
                        </tr>
                        <tr class="buy_order">
                            <td>DOGE/LTC</td>
                            <td>0.00%</td>
                            <td>0.00%</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading div-form-title">Deposit and Withdrawal Requirement</div>
            <div>
                <table class="table table-striped table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>Coin</th>
                            <th>Min Conf (Deposit)</th>
                            <th>Tx Fee (Withdrawal)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="buy_order">
                            <td>BTC</td>
                            <td>2 Confirmations</td>
                            <td>0.00010000 BTC</td>
                        </tr>
                        <tr class="buy_order">
                            <td>CINNI</td>
                            <td>5 Confirmations</td>
                            <td>0.01000000 CINNI</td>
                        </tr>
                        <tr class="buy_order">
                            <td>DOGE</td>
                            <td>3 Confirmations</td>
                            <td>1.00000000 DOGE</td>
                        </tr>
                        <tr class="buy_order">
                            <td>DRK</td>
                            <td>5 Confirmations</td>
                            <td>0.01000000 DRK</td>
                        </tr>
                        <tr class="buy_order">
                            <td>KIWI</td>
                            <td>5 Confirmations</td>
                            <td>0.00100000 KIWI</td>
                        </tr>
                        <tr class="buy_order">
                            <td>KORE</td>
                            <td>5 Confirmations</td>
                            <td>0.01000000 KORE</td>
                        </tr>
                        <tr class="buy_order">
                            <td>LTC</td>
                            <td>3 Confirmations</td>
                            <td>0.00100000 LTC</td>
                        </tr>
                        <tr class="buy_order">
                            <td>OZC</td>
                            <td>5 Confirmations</td>
                            <td>0.01000000 OZC</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>