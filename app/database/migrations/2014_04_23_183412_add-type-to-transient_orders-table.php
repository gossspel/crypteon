<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeToTransientOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('transient_orders', function(Blueprint $table)
        {
            $table->string('mode', 20); // Sell or Buy
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('transient_orders', function(Blueprint $table)
        {
            $table->dropColumn('mode');
        });
	}

}
