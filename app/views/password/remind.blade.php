<div class="row">
    <div class="col-lg-12 basic-mainframe">
        <!-- Include Alert Message here, better than showing it in the layout to more easily control css -->
        @include('row-alert')
    </div>
    <div class="col-lg-6 col-lg-offset-3">
        <div class="panel panel-default div-form-top">
            <div class="panel-heading div-form-title">
                Enter your account's email, we will send you a link to reset password.
            </div>
            <div class="panel-body">
                {{ Form::open(array('url'=>'password/remind', 'class'=>'form-signin', 'role'=>'form')) }}
                <div class="form-group">
                    {{ Form::label('email', 'E-Mail Address') }}
                    {{ Form::text('email', null, array('class'=>'form-control', 'placeholder'=>'Email Address')) }}
                </div>
                {{ Form::submit('Request Password Reset', array('class'=>'btn btn-success btn-block'))}}
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>