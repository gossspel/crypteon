<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFeeModeToMarketsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('markets', function(Blueprint $table)
        {
            $table->string('fee_mode', 20)->default('normal'); // normal => uses fee_rate from profiles, special => uses fee_rate from markets
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('markets', function(Blueprint $table)
        {
            $table->dropColumn('fee_mode');
        });
	}

}
