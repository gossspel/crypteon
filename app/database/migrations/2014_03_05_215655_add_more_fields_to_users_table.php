<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreFieldsToUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function(Blueprint $table)
		{
            $table->decimal('ltc_order_balance', 16, 8);
            $table->decimal('ltc_available_balance', 16, 8);
            $table->decimal('doge_order_balance', 20, 8);
            $table->decimal('doge_available_balance', 20, 8);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users', function(Blueprint $table)
		{
            $table->dropColumn('ltc_order_balance');
            $table->dropColumn('ltc_available_balance');
            $table->dropColumn('doge_order_balance');
            $table->dropColumn('doge_available_balance');
		});
	}

}