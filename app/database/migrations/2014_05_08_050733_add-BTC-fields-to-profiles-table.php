<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBTCFieldsToProfilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('profiles', function(Blueprint $table)
        {
            $table->string('BTC_deposit_addr', 100)->nullable();
            $table->decimal('BTC_total_received', 20, 8)->default(0); // used for checking and inserting deposit records
            $table->decimal('BTC_available_balance', 20, 8)->default(0); // combine with BTC_order_balance, you will get BTC_current_balance
            $table->decimal('BTC_order_balance', 20, 8)->default(0);
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('profiles', function(Blueprint $table)
        {
            $table->dropColumn('BTC_deposit_addr');
            $table->dropColumn('BTC_total_received');
            $table->dropColumn('BTC_available_balance');
            $table->dropColumn('BTC_order_balance');
        });
	}

}
