<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersistentOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('persistent_orders', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('buy_coin',20);
            $table->decimal('buy_amount',20,8);
            $table->string('sell_coin',20);
            $table->decimal('sell_amount',20,8);
            $table->decimal('available_sell_amount',20,8);
            $table->decimal('rate',16,8);
            $table->string('status',20);
            $table->integer('order_id');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('persistent_orders');
	}

}
