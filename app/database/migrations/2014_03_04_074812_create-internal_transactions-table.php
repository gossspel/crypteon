<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInternalTransactionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        // The Transaction within coin daemon account system, see https://en.bitcoin.it/wiki/Accounts_explained
		Schema::create('internal_transactions', function(Blueprint $table)
		{
			$table->increments('id');
            $table->decimal('amount', 20, 8);
            $table->string('coin', 20);
            $table->string('from_account', 100);
            $table->string('to_account', 100);
            $table->string('status', 20);
            $table->string('type', 60); // "order_sold", "order_bought", "withdrawal_related"
            $table->integer('order_id')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('internal_transactions');
	}

}
