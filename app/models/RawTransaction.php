<?php

class RawTransaction extends Eloquent {

    protected $table = 'raw_transactions';

    // There exists a 1 to N relationship between RawTransaction{Entity} and Withdrawals
    public function withdrawals()
    {
        return $this->hasMany('Withdrawal');
    }

    // There exists a N to 1 relationship between RawTransaction{Entity} and DaemonServer{Entity}.
    public function daemonServer()
    {
        return $this->belongsTo('DaemonServer');
    }


}