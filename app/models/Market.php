<?php

class Market extends Eloquent {

    protected $table = 'markets';

    protected $fillable = array('pair', 'coin', 'base');

    // There exists a 1 to N relationship between Market{Entity} and Order{Entity}.
    public function orders()
    {
        return $this->hasMany('Order');
    }

    // There exists a 1 to N relationship between Market{Entity} and TransientOrder{Entity}.
    public function transientOrders()
    {
        return $this->hasMany('TransientOrder');
    }
}