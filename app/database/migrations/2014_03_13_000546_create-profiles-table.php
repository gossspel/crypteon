<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('profiles', function(Blueprint $table)
		{
			$table->integer('id')->primary()->unsigned(); // one to one relationship with users table, so this is the foreign key
            $table->string('LTC_deposit_addr', 100)->nullable();
            $table->decimal('LTC_total_received', 20, 8)->default(0); // used for checking and inserting deposit records
            $table->decimal('LTC_available_balance', 20, 8)->default(0); // combine with LTC_order_balance, you will get LTC_current_balance
            $table->decimal('LTC_order_balance', 20, 8)->default(0);
            $table->string('DOGE_deposit_addr', 100)->nullable();
            $table->decimal('DOGE_total_received', 20, 8)->default(0);
            $table->decimal('DOGE_available_balance', 20, 8)->default(0);
            $table->decimal('DOGE_order_balance', 20, 8)->default(0);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('profiles');
	}

}
