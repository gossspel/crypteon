<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
        <p>Hi {{ $user->firstname . ' ' . $user->lastname }},</p>

		<p>
			To reset your password, complete this form: {{ URL::to('password/reset', array($token)) }}.
		</p>
        <p>
            <div>Best,</div>
            <div>Team Crypteon</div>
        </p>
	</body>
</html>