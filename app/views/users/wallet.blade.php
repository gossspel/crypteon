<div class="row">
    <div class="col-lg-12 basic-mainframe alert-holder">
        <!-- Include Alert Message here, better than showing it in the layout to more easily control css -->
        @include('alert')
    </div>
    <div class="col-lg-12">
        <div class="main-title">
            Wallet
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <!-- Data Table -->
        <div class="div-table panel panel-default div-table-no-max-height">
            <table class="table table-striped table-hover table-bordered">
                <thead>
                <tr>
                    <th>Coin</th>
                    <th class="hidden-xs">Available Balance</th>
                    <th class="visible-lg visible-md">Deposit Address</th>
                    <th>Detail</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <!-- Ajax POST Request Implementation -->
                @foreach ($available_coins as $available_coin)
                <tr>
                    <td>{{ $available_coin }}</td>
                    <td class="hidden-xs">{{ $profile->{$available_coin.'_available_balance'} . ' ' . $available_coin }}</td>
                    <td class="visible-lg visible-md {{ $available_coin . '_wrap'}}">
                        @if (empty($profile->{$available_coin.'_deposit_addr'}))
                        <button type="button" class="btn btn-success {{ $available_coin }}" value="{{ $available_coin }}" data-loading-text="Generating...">Generate Deposit Address</button>
                        @else
                        {{ $profile->{$available_coin.'_deposit_addr'} }}
                        @endif
                    </td>
                    <td>
                        <!-- Button trigger modal -->
                        <button class="btn btn-info" data-toggle="modal" data-target="#myModal-{{ $available_coin }}">
                            Show
                        </button>

                        <!-- Modal -->
                        <div class="modal detail-modal fade" id="myModal-{{ $available_coin }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel-{{ $available_coin }}" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" id="myModalLabel-{{ $available_coin }}">Wallet Detail</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="detail-div-row">
                                            <div>Coin:</div>
                                            <div class="up">{{ $available_coin }}</div>
                                        </div>
                                        <div class="detail-div-row">
                                            <div>Deposit Address:</div>
                                            <div class="up {{ $available_coin . '_wrap'}} too-long-break">
                                                @if (empty($profile->{$available_coin.'_deposit_addr'}))
                                                <button type="button" class="btn btn-success {{ $available_coin }}" value="{{ $available_coin }}" data-loading-text="Generating...">Generate Deposit Address</button>
                                                @else
                                                {{ $profile->{$available_coin.'_deposit_addr'} }}
                                                @endif
                                            </div>
                                        </div>
                                        <div class="detail-div-row">
                                            <div>Available Balance:</div>
                                            <div class="up">{{ $profile->{$available_coin.'_available_balance'} . ' ' . $available_coin }}</div>
                                        </div>
                                        <div class="detail-div-row">
                                            <div>Open Order Balance:</div>
                                            <div class="up">{{ $profile->{$available_coin.'_order_balance'} . ' ' . $available_coin }}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td>
                        {{
                        link_to_action('UsersController@getWithdraw',
                        'Withdraw',
                        $parameters = array($available_coin),
                        $attributes = array("class" => "btn btn-primary", "role" => "button"))
                        }}
                    </td>
                </tr>
                @endforeach
                <!-- End of Ajax POST Request Implementation -->
                </tbody>
            </table>
        </div>
    </div>
</div>

@section('plscripts')
    <script>
        $( document ).ready(function() {
            var json_available_coins = <?php echo json_encode($available_coins); ?>;
            var jac_length = json_available_coins.length;

            for (var jac = 0; jac < jac_length; jac++) {
                var ajax_selector = 'button.' + json_available_coins[jac];

                $(ajax_selector).on('click', function() {
                    // Put button to loading state
                    var btn = $(this);
                    btn.button('loading');

                    // Make Ajax Call
                    var button = $(this).val();
                    var ajax_class = '.' + button + '_wrap';
                    var ajax_id = 'button.' + button;
                    var ajax_call = $.ajax({
                        url: "{{ action('UsersController@postAjaxGenerate') }}",
                        type: "POST",
                        data: { coin : button, _token : "{{ csrf_token() }}" },
                        dataType: "json"
                    });

                    // Display Returned JSON data
                    ajax_call.done(function(data) {
                        var first = '<div class="alert ' + data['mode'] + ' alert-dismissable row-alert" id="alert-container">';
                        var second = '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                        var third = data['message'];
                        var fourth = '</div>';
                        var all = first + second + third + fourth;
                        if (data['mode'] == 'alert-success') {
                            $(ajax_id).remove();
                            $(ajax_class).append(data['deposit_addr']);
                        }
                        $("#alert-container").remove();
                        $(".alert-holder").prepend(all);
                        window.scrollTo(0, 0);
                        btn.button('reset');
                    });
                });
            }
        });
    </script>
@stop