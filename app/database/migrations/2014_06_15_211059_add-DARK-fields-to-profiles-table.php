<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDARKFieldsToProfilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('profiles', function(Blueprint $table)
        {
            $table->string('DRK_deposit_addr', 100)->nullable();
            $table->decimal('DRK_total_received', 20, 8)->default(0); // used for checking and inserting deposit records
            $table->decimal('DRK_available_balance', 20, 8)->default(0); // combine with DRK_order_balance, you will get DRK_current_balance
            $table->decimal('DRK_order_balance', 20, 8)->default(0);
            $table->string('DRK_activity', 20)->default('inactive');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('profiles', function(Blueprint $table)
        {
            $table->dropColumn('DRK_deposit_addr');
            $table->dropColumn('DRK_total_received');
            $table->dropColumn('DRK_available_balance');
            $table->dropColumn('DRK_order_balance');
            $table->dropColumn('DRK_activity');
        });
	}

}
