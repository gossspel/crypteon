<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		Eloquent::unguard();
        // $this->call('UserTableSeeder');
        $this->call('MarketTableSeeder');
        // $this->call('DaemonServerTableSeeder');
	}

}

class UserTableSeeder extends Seeder {

    public function run() {
        $mui = array(); // Mass User Insert
        $mpi = array(); // Mass Profile Insert
        for ($i = 1; $i <= 50; $i++) {
            // Create New User Array
            $date = date('Y-m-d H:i:s');
            $user = array();
            $user['firstname'] = 'Sunny';
            $user['lastname'] = 'Chan';
            $user['email'] = 'curl' . $i . '@integrnet.com';
            $user['password'] = Hash::make('password1');
            $user['status'] = 'active';
            $user['daemon_server_id'] = 1;
            $user['created_at'] = $date;
            $user['updated_at'] = $date;

            // Insert into Mass User Insert Array
            $mui[] = $user;

            // Create New Profile Array
            $profile = array();
            $profile['id'] = $i;
            $profile['email'] = 'curl' . $i . '@integrnet.com';
            $profile['daemon_server_id'] = 1;
            $profile['DOGE_available_balance'] = 500000;
            $profile['LTC_available_balance'] = 100;
            $profile['created_at'] = $date;
            $profile['updated_at'] = $date;

            // Insert into Mass Profile Insert Array
            $mpi[] = $profile;
        }

        // Mass Insert of users
        DB::table('users')->insert($mui);

        // Mass Insert of profiles
        DB::table('profiles')->insert($mpi);
    }
}

class MarketTableSeeder extends Seeder {

    public function run() {
        $seed_data = array('pair' => 'OZC/BTC',
            'coin' => 'OZC',
            'base' => 'BTC',
            'fee_rate' => 0.0014
        );
        Market::create($seed_data);
    }
}

class DaemonServerTableSeeder extends Seeder {

    public function run() {
        $seed_data = array('ip' => '192.99.13.186');
        DaemonServer::create($seed_data);
    }
}

