<?php

class PersistentOrder extends Eloquent {

    protected $table = 'persistent_orders';

    // There exists a 1 to 1 relationship between PersistentOrder{Entity} and Order{Entity}.
    public function order()
    {
        return $this->belongsTo('Order');
    }

    // There exists a N to 1 relationship between PersistentOrder{Entity} and User{Entity}.
    public function user()
    {
        return $this->belongsTo('User');
    }

    // There exists a M to N relationship between TransientOrder{Entity} and PersistentOrder{Entity}.
    public function transientorders()
    {
        return $this->belongsToMany('TransientOrder')->withTimestamps();
    }

}