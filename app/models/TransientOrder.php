<?php

class TransientOrder extends Eloquent {

    protected $table = 'transient_orders';

    // There exists a 1 to 1 relationship between Order{Entity} and TransientOrder{Entity}.
    public function order()
    {
        return $this->belongsTo('Order');
    }

    // There exists a N to 1 relationship between TransientOrder{Entity} and Market{Entity}.
    public function market()
    {
        return $this->belongsTo('Market');
    }

    // There exists a N to 1 relationship between TransientOrder{Entity} and User{Entity}.
    public function user()
    {
        return $this->belongsTo('User');
    }

    // There exists a M to N relationship between TransientOrder{Entity} and PersistentOrder{Entity}.
    public function persistentorders()
    {
        return $this->belongsToMany('PersistentOrder')->withTimestamps();
    }

}