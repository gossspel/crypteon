<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMOONFieldsToProfilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('profiles', function(Blueprint $table)
        {
            $table->string('MOON_deposit_addr', 100)->nullable();
            $table->decimal('MOON_total_received', 20, 8)->default(0); // used for checking and inserting deposit records
            $table->decimal('MOON_available_balance', 20, 8)->default(0); // combine with MOON_order_balance, you will get MOON_current_balance
            $table->decimal('MOON_order_balance', 20, 8)->default(0);
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('profiles', function(Blueprint $table)
        {
            $table->dropColumn('MOON_deposit_addr');
            $table->dropColumn('MOON_total_received');
            $table->dropColumn('MOON_available_balance');
            $table->dropColumn('MOON_order_balance');
        });
	}

}
