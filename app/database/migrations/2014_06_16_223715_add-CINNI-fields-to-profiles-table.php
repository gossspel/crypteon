<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCINNIFieldsToProfilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('profiles', function(Blueprint $table)
        {
            $table->string('CINNI_deposit_addr', 100)->nullable();
            $table->decimal('CINNI_total_received', 20, 8)->default(0); // used for checking and inserting deposit records
            $table->decimal('CINNI_available_balance', 20, 8)->default(0); // combine with CINNI_order_balance, you will get CINNI_current_balance
            $table->decimal('CINNI_order_balance', 20, 8)->default(0);
            $table->string('CINNI_activity', 20)->default('inactive');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('profiles', function(Blueprint $table)
        {
            $table->dropColumn('CINNI_deposit_addr');
            $table->dropColumn('CINNI_total_received');
            $table->dropColumn('CINNI_available_balance');
            $table->dropColumn('CINNI_order_balance');
            $table->dropColumn('CINNI_activity');
        });
	}

}
