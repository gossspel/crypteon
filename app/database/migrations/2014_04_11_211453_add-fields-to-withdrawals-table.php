<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToWithdrawalsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('withdrawals', function(Blueprint $table)
        {
            $table->string('priority', 40)->default('medium');
            $table->integer('raw_transaction_id')->nullable();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('withdrawals', function(Blueprint $table)
        {
            $table->dropColumn('priority');
            $table->dropColumn('raw_transaction_id');
        });
	}

}
