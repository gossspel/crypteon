<?php

class Order extends Eloquent {

    public static $sell_order_rules = array(
        'market_id'=>'required|numeric',
        'sell_rate'=>'required|numeric|minrate',
        'original_sell_amount'=>'required|numeric'
    );

    public static $buy_order_rules = array(
        'market_id'=>'required|numeric',
        'buy_rate'=>'required|numeric|minrate',
        'original_buy_amount'=>'required|numeric'
    );

    public static $cancel_order_rules = array(
        'id' => 'required|numeric'
    );

    protected $table = 'orders';

    // There exists a N to 1 relationship between Order{Entity} and User{Entity}.
    public function user()
    {
        return $this->belongsTo('User');
    }

    // There exists a N to 1 relationship between Order{Entity} and Market{Entity}.
    public function market()
    {
        return $this->belongsTo('Market');
    }

    // There exists a 1 to N relationship between Order{Entity} and InternalTransaction{Entity}.
    public function internaltransactions()
    {
        return $this->hasMany('InternalTransaction');
    }

    // There exists a 1 to N relationship between Order{Entity} and UserBalanceTransaction{Entity}.
    public function userBalanceTransactions()
    {
        return $this->hasMany('UserBalanceTransaction');
    }

    // There exists a 1 to 1 relationship between Order{Entity} and TransientOrder{Entity}.
    public function transientorder()
    {
        return $this->hasOne('TransientOrder');
    }

    // There exists a 1 to 1 relationship between Order{Entity} and PersistentOrder{Entity}.
    public function persistentorder()
    {
        return $this->hasOne('PersistentOrder');
    }
}