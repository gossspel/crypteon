<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFeeRateToMarketsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('markets', function(Blueprint $table)
        {
            $table->decimal('fee_rate',11,8);
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('markets', function(Blueprint $table)
        {
            $table->dropColumn('fee_rate');
        });
	}

}
