<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMissingFieldToInternalTransactionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('internal_transactions', function(Blueprint $table)
		{
            $table->integer('withdrawal_id')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('internal_transactions', function(Blueprint $table)
		{
		    $table->dropColumn('withdrawal_id');
		});
	}

}