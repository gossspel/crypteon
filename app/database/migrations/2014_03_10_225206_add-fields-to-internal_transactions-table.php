<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToInternalTransactionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('internal_transactions', function(Blueprint $table)
		{
            $table->integer('user_id');
            $table->integer('daemon_server_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('internal_transactions', function(Blueprint $table)
		{
            $table->dropColumn('user_id');
            $table->dropColumn('daemon_server_id');
		});
	}

}