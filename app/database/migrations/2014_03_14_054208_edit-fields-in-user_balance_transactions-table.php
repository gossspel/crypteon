<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditFieldsInUserBalanceTransactionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('user_balance_transactions', function(Blueprint $table)
		{
		    $table->dropColumn('balance_before_transaction'); // Infeasible for multi update
            $table->dropColumn('balance_after_transaction'); // Infreasible for multi update
            $table->string('transaction_action', 30); // "increase_balance", "decrease_balance"
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('user_balance_transactions', function(Blueprint $table)
		{
		    $table->dropColumn('transaction_action');
            $table->decimal('balance_before_transaction', 20, 8);
            $table->decimal('balance_after_transaction', 20, 8);
		});
	}

}