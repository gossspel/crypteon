<?php

class Deposit extends Eloquent {

    protected $table = 'deposits';

    // There exists a 1 to 1 relationship between UserBalanceTransaction{Entity} and Desposit{Entity}
    public function userBalanceTransaction()
    {
        return $this->hasOne('UserBalanceTransaction');
    }

    // There exists a 1 to N relationship between DaemonServer{Entity} and Deposit{Entity}.
    public function daemonServer()
    {
        return $this->belongsTo('DaemonServer');
    }

    // There exists a 1 to N relationship between User{Entity} and Deposit{Entity}.
    public function user()
    {
        return $this->belongsTo('User');
    }

}