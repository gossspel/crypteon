<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteFieldsToUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
    public function up()
    {
        Schema::table('users', function(Blueprint $table)
        {
            $table->dropColumn('ltc_deposit_addr');
            $table->dropColumn('ltc_balance');
            $table->dropColumn('ltc_available_balance');
            $table->dropColumn('ltc_order_balance');
            $table->dropColumn('doge_deposit_addr');
            $table->dropColumn('doge_balance');
            $table->dropColumn('doge_available_balance');
            $table->dropColumn('doge_order_balance');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function(Blueprint $table)
        {
            $table->string('ltc_deposit_addr', 100);
            $table->decimal('ltc_balance', 20, 8);
            $table->decimal('ltc_available_balance', 20, 8);
            $table->decimal('ltc_order_balance', 20, 8);
            $table->string('doge_deposit_addr', 100);
            $table->decimal('doge_balance', 20, 8);
            $table->decimal('doge_available_balance', 20, 8);
            $table->decimal('doge_order_balance', 20, 8);
        });
    }

}