<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<p>Welcome {{ $first_name . ' ' . $last_name }}!</p>

		<p>
            Thank you for registering on Crypteon! You are one step away from trading on Crypteon!
		</p>
        <p>
            Activate your account by clicking on the link below:
        </p>
        <p>
        {{ $link }}
        </p>
        <p>
            <div>Cheers,</div>
            <div>Team Crypteon</div>
        </p>
	</body>
</html>