<?php

class WithdrawalsController extends BaseController {

    protected $layout = "layouts.test";

    public function __construct() {
        $this->beforeFilter('csrf', array('on' => 'post'));
        $this->beforeFilter('basic.once', array('only' => 'getProcess'));
        $this->beforeFilter('auth', array('only' => 'postCreate'));
    }

    public function postCreate() {
        // Check if server is in Maintenance mode
        $redis = Redis::connection();

        if ($redis->get('server_mode') == 'maintenance') {
            return View::make('maintenance');
        }

        // Check if user is logged in
        if (Auth::check() == false) {
            return Redirect::action('UsersController@getWallet')
                ->with('message', 'You have been logged out, please login again to request withdrawals.')
                ->with('message-level', 'alert-info');
        }

        // Check if User is activated
        $user = Auth::user();

        if ($user->status != 'active') {
            $link = HTML::link('users/activation-email', 'Resend activation email.');
            return Redirect::action('UsersController@getWallet')
                ->with('message', 'Please activate your account to have full access on Crypteon. ' . $link)
                ->with('message-level', 'alert-danger');
        }

        // Lock withdrawal in progress
        $auth_user_id = $user->id;
        $wipu_key = 'lock_order_or_withdrawal_user_' . $auth_user_id;
        $wipu_key_count = $redis->incr($wipu_key);
        $redis->expire($wipu_key, 10);

        if ($wipu_key_count > 1) {
            return Redirect::action('UsersController@getWallet')
                ->with('message', 'For the sake of security and data integrity, order/withdrawal request is limited to 1 per 10 seconds. Please try again later.')
                ->with('message-level', 'alert-danger');
        }

        // Check if coin is valid
        $coin = Input::get('coin');
        $available_coins = $redis->lrange('available_coins', 0, -1);

        if (!in_array($coin, $available_coins)) {
            return Redirect::action('UsersController@getWallet')
                ->with('message', 'The parameter is invalid, please try to withdraw again without altering the parameter')
                ->with('message-level', 'alert-danger');
        }

        // Check form validation
        $messages = array(
            'withdrawal_amount.required' => 'The Withdrawal Amount is required.',
            'withdrawal_amount.numeric' => 'The Withdrawal Amount must be a number',
            'to_address.required' => 'The ' . $coin . ' Receiving Address is required.',
            'to_address.alpha_num' => 'The ' . $coin . ' Receiving Address may only contain letters and numbers.',
            'password.required' => 'The Account Password is required.'
        );

        $validator = Validator::make(Input::all(), Withdrawal::$process_rules, $messages);

        if (!($validator->passes())) {
            return Redirect::action('UsersController@getWithdraw', array($coin))
                ->with('message', 'The following errors occurred:')
                ->with('message-level', 'alert-danger')
                ->withErrors($validator)
                ->withInput(Input::except(array('password')));
        }

        // Variable Declaration
        $raw_withdrawal_amount = Input::get('withdrawal_amount');
        $tx_fee = $redis->hget('coin_tx_fee', $coin);
        $withdrawal_amount = bcadd($raw_withdrawal_amount, $tx_fee, 8);
        $to_address = Input::get('to_address');
        $string_coin_available_balance = $coin.'_available_balance';

        $auth_daemon_server_id = $user->daemon_server_id;
        $auth_email = $user->email;

        // Check if withdrawal amount is less than available balance
        $profile = DB::table('profiles')
            ->select($string_coin_available_balance)
            ->where('id', $auth_user_id)
            ->first();

        $balance = $profile->{$string_coin_available_balance};

        if ($withdrawal_amount > $profile->{$string_coin_available_balance}) {
            return Redirect::action('UsersController@getWithdraw', array($coin))
                ->with('message', 'Insufficient balance, please enter a withdrawal amount less than or equal to your ' . $coin . ' available balance')
                ->with('message-level', 'alert-danger');
        }

        // Insert a new record in withdrawals table
        $date = date('Y-m-d H:i:s');
        $new_withdrawal = new Withdrawal;
        $new_withdrawal->coin = $coin;
        $new_withdrawal->from_address = 'default_'.$coin;
        $new_withdrawal->to_address = $to_address;
        $new_withdrawal->withdrawal_amount = $raw_withdrawal_amount;
        $new_withdrawal->tx_fee = $tx_fee;
        $new_withdrawal->status = 'pending';
        $new_withdrawal->user_id = $auth_user_id;
        $new_withdrawal->daemon_server_id = $auth_daemon_server_id;
        $new_withdrawal->created_at = $date;
        $new_withdrawal->updated_at = $date;
        $new_withdrawal->save();

        // Insert a new record in user_balance_transactions
        $date = date('Y-m-d H:i:s');
        $new_user_balance_transaction = new UserBalanceTransaction;
        $new_user_balance_transaction->instance_type = $string_coin_available_balance;
        $new_user_balance_transaction->transaction_amount = $withdrawal_amount;
        $new_user_balance_transaction->type = 'withdrawal';
        $new_user_balance_transaction->withdrawal_id = $new_withdrawal->id;
        $new_user_balance_transaction->user_id = $auth_user_id;
        $new_user_balance_transaction->transaction_action = 'decrease_balance';
        $new_user_balance_transaction->created_at = $date;
        $new_user_balance_transaction->updated_at = $date;
        $new_user_balance_transaction->save();

        // Update User Profile with binding for sanitize
        DB::statement(DB::raw(
            "UPDATE profiles SET " .
            $string_coin_available_balance . " = " . $string_coin_available_balance . " - :withdrawal_amount" .
            ", updated_at = now() WHERE id = " . $auth_user_id), array('withdrawal_amount' => $withdrawal_amount));

        // Insert a new record in internal_transactions
        $date = date('Y-m-d H:i:s');
        $new_internal_transaction = new InternalTransaction;
        $new_internal_transaction->amount = $withdrawal_amount;
        $new_internal_transaction->coin = $coin;
        $new_internal_transaction->from_account = $auth_email;
        $new_internal_transaction->to_account = 'default_'.$coin;
        $new_internal_transaction->status = 'pending';
        $new_internal_transaction->type = 'withdrawal';
        $new_internal_transaction->user_id = $auth_user_id;
        $new_internal_transaction->daemon_server_id = $auth_daemon_server_id;
        $new_internal_transaction->withdrawal_id = $new_withdrawal->id;
        $new_internal_transaction->created_at = $date;
        $new_internal_transaction->updated_at = $date;
        $new_internal_transaction->save();

        // Redirect
        return Redirect::action('UsersController@getWithdrawalHistory')
            ->with('message', $coin. ' Withdrawal has been submitted!')
            ->with('message-level', 'alert-success');

        // TODO: Create controller logic and view for UsersController::getWithdrawal History with at least sortable links
        // TODO: Test withdrawals for the newly implemented tx_fee system

    }

    // Process Withdrawals and Create RawTransactions
    public function getProcess($coin = null, $coin_return_addr = null, $min_input = null, $tx_fee_per_kb = null, $daemon_server_id = null) {
        // Check if Admin
        $user = Auth::user();
        if ($user->group != 'admin') {
            return Redirect::to('markets/secure-view')
                ->with('message', "Oops, we couldn't find what you're looking for.")
                ->with('message-level', 'alert-info');
        }

        // Check if server is in Maintenance mode
        $redis = Redis::connection();

        if ($redis->get('server_mode') == 'maintenance') {
            return 'Server is on maintenance mode. Withdrawal Process is postponed.';
        }

        // Check if CRON Process is locked
        $cron_key = 'CRON_withdrawals_' . $coin;

        if ($redis->exists($cron_key)) {
            $custom_error = 'withdrawal_process_aborted: locked CRON process in ' . $coin . ' Daemon at Daemon Server '. $daemon_server_id;

            // Email to report error
            $mail_data = array('error_message' => $custom_error);
            Mail::later(10, 'emails.admin.error', $mail_data, function($message)
            {
                $message->to('crypteonnet@gmail.com', 'Crypteon')->subject('Error on server');
            });

            Log::error($custom_error);
            return $custom_error;
        } else {
            // Lock CRON Process
            $lock_time = time();
            $redis->setex($cron_key, 600, $lock_time);
        }

        // Extend Maximum Execution Time
        set_time_limit(900);

        // Getting Unvalidated Withdrawals array
        $withdrawal_select = array('id',
            'to_address',
            'withdrawal_amount',
            'priority',
            'user_id');

        $raw_high_withdrawals = DB::table('withdrawals')
            ->select($withdrawal_select)
            ->where('daemon_server_id', $daemon_server_id)
            ->where('coin', $coin)
            ->where('status', 'failed, retrying')
            ->where('priority', 'high')
            ->orderBy('withdrawal_amount', 'asc')
            ->get(); // The return result is an array of php standard class (objects).

        $raw_medium_withdrawals = DB::table('withdrawals')
            ->select($withdrawal_select)
            ->where('daemon_server_id', $daemon_server_id)
            ->where('coin', $coin)
            ->where('status', 'pending')
            ->where('priority', 'medium')
            ->orderBy('withdrawal_amount', 'asc')
            ->get(); // The return result is an array of php standard class (objects).

        // Check if we have Withdrawals to process and define the Raw Withdrawals
        if (!empty($raw_high_withdrawals) && !empty($raw_medium_withdrawals)) {
            $raw_withdrawals = array_merge($raw_high_withdrawals, $raw_medium_withdrawals); // So that high priority withdrawals will most likely be processed
        } elseif (!empty($raw_high_withdrawals) && empty($raw_medium_withdrawals)) {
            $raw_withdrawals = $raw_high_withdrawals;
        } elseif (empty($raw_high_withdrawals) && !empty($raw_medium_withdrawals)) {
            $raw_withdrawals = $raw_medium_withdrawals;
        } else {
            // Unlock CRON Process
            $redis->del($cron_key);

            return 'No pending Withdrawals on ' . $coin . ' at Daemon Server ' . $daemon_server_id . '.';
        }

        // Getting Coin Daemon
        $coin_json_rpc = $coin . '_JSON_RPC_DS_' . $daemon_server_id;
        $json_rpc_url = 'http://' . $_ENV[$coin_json_rpc] . '/';
        $coind = new jsonRPCClient($json_rpc_url);

        // Getting Validated Withdrawals array
        $withdrawals = array();

        foreach ($raw_withdrawals as $raw_withdrawal) {
            $validation_array = $coind->validateaddress($raw_withdrawal->to_address);
            // TODO - Add address validation logic to Withdrawal create form, don't worry about daemon ddos yet
            if ($validation_array['isvalid'] == true) {
                $withdrawals[] = $raw_withdrawal;
            }
        }

        // Getting Listunspent array from Coin Daemon
        $raw_listunspent = $coind->listunspent(6); // Only show unspent input that has atleast 6 confirmation

        // Sorting (DESC) Listunspent array
        $cmp = function($a, $b) { // Closure comparison function for using usort
            if ($a["amount"] == $b["amount"]) {
                return 0;
            }
            return ($a["amount"] < $b["amount"]) ? 1 : -1;
        };

        usort($raw_listunspent, $cmp);

        // Getting Final Listunspent array (special sort: smallest, largest, second smallest, second largest, ...)
        $listunspent = array();
        $count = count($raw_listunspent);

        for ($i = 1; $i <= $count; $i++) {
            if ($i % 2 == 1) {
                $listunspent[$i - 1] = array_pop($raw_listunspent);
            } else {
                $listunspent[$i - 1] = array_shift($raw_listunspent);
            }
        }

        // Gettling Listunspent cumulative amount array
        $listunspent_cumulative = array();
        $cumulative_amount = 0;

        foreach ($listunspent as $unspent) {
            $cumulative_amount = bcadd($cumulative_amount, $unspent['amount'], 8);
            $listunspent_cumulative[] = $cumulative_amount;
        }

        // Getting Sum Amount of Validated Withdrawals
        $withdrawals_cumulative = array();
        $withdrawals_medium_id = array();
        $withdrawal_amount = 0;

        foreach ($withdrawals as $withdrawal) {
            $withdrawal_amount = bcadd($withdrawal_amount, $withdrawal->withdrawal_amount, 8);
            $withdrawals_cumulative[] = $withdrawal_amount;
            $withdrawals_medium_id[] = $withdrawal->id;
        }
        
        $withdrawals_id = $withdrawals_medium_id;
        $listunspent_cumulative_total = end($listunspent_cumulative);
        $withdrawals_cumulative_total = end($withdrawals_cumulative);

        // Determine if we can create raw transaction now or if we need to pop off some withdrawals for next round.
        $withdrawals_high_id = array(); // These withdrawals will be updated as "high" priority for next round.
        $continue_processing = true; // This is for checking if we should create the raw transaction or not.
        $no_next_round_withdrawals = true;

        while ($withdrawals_cumulative_total > $listunspent_cumulative_total) {
            // Move the biggest withdrawal to the next round
            $no_next_round_withdrawals = false;
            $next_round_withdrawal = array_pop($withdrawals);
            array_pop($withdrawals_medium_id);

            // Stop the Process if we need to pop off high priority withdrawals because that means we have to wait until more inputs are confirmed
            if ($next_round_withdrawal->priority == 'high') {
                // save warning log and quit,
                $continue_processing = false;
                $warning = 'withdrawal_process_aborted: Daemon Server ' . $daemon_server_id . ' ' . $coin . ' does not have enough unspent inputs.';
                Log::warning($warning);
                break;
            }

            // Set the popped off withdrawal id to the update array
            $withdrawals_high_id[] = $next_round_withdrawal->id;

            // Update the Withdrawal Cumulative Total for next iteration
            array_pop($withdrawals_cumulative);
            $withdrawals_cumulative_total = end($withdrawals_cumulative);
        }

        if ($continue_processing == false) {
            // Unlock CRON Process
            $redis->del($cron_key);

            // Return the message Withdrawal process aborted and stop the process
            return $warning;
        } else {
            // Loop through the listunspent_cumulative total with the withdrawal_cumulative_total to see how many unspent inputs we need.
            $input_count = 0;

            foreach ($listunspent_cumulative as $unspent_cumulative) {
                $input_count += 1;

                if ($unspent_cumulative >= $withdrawals_cumulative_total) {
                    // End the loop, we only need the first $input_count inputs
                    $sum_of_unspent_inputs = $unspent_cumulative;
                    break;
                }

            }

            // Make Raw Transaction using try catch
            $raw_tx_first_param = array();
            $raw_tx_second_param = array();

            // Calculate Transaction fee and Return Address Amount
            $withdrawals_count = count($withdrawals) + 1; // plus one for return addr
            $raw_tx_bytes = $input_count * 149 + $withdrawals_count * 34 + 10;
            $raw_tx_kbs = bcdiv($raw_tx_bytes, '1000', 8);
            $tx_fee = bcmul($raw_tx_kbs, $tx_fee_per_kb, 8);
            $total_withdrawals_amount_with_tx_fee = bcadd($withdrawals_cumulative_total, $tx_fee, 8);
            $return_addr_amount = bcsub($sum_of_unspent_inputs, $total_withdrawals_amount_with_tx_fee, 8);

            // Getting first parameter for CoinDaemon::createrawtransaction
            for ($i = 1; $i <= $input_count; $i++) {
                $tx_instance = array(
                    "txid" => $listunspent[$i - 1]["txid"],
                    "vout" => $listunspent[$i - 1]["vout"]
                );

                $raw_tx_first_param[] = $tx_instance;
            }

            // Getting second parameter for CoinDaemon::createrawtransaction
            foreach ($withdrawals as $withdrawal) {
                $raw_tx_second_param[$withdrawal->to_address] = floatval($withdrawal->withdrawal_amount);
            }

            // Insert Return Address Amount into second parameter
            if ($return_addr_amount > $min_input) {
                $raw_tx_second_param[$coin_return_addr] = floatval($return_addr_amount);
            }

            // CoinDaemon::createrawtransaction
            try {
                $createrawtransaction = $coind->createrawtransaction($raw_tx_first_param, $raw_tx_second_param);
            } catch (Exception $e) {
                $error = $e->getMessage();
                $custom_error = 'withdrawal_process_aborted: failed createrawtransaction from ' . $coin . ' Daemon at Daemon Server '. $daemon_server_id;
                Log::error($custom_error);
                Log::error($error);

                // Unlock CRON Process
                $redis->del($cron_key);

                return $custom_error;
            }

            // CoinDaemon::signrawtransaction
            try {
                $signrawtransaction = $coind->signrawtransaction($createrawtransaction);
            } catch (Exception $e) {
                $error = $e->getMessage();
                $custom_error = 'withdrawal_process_aborted: failed signrawtransaction from ' . $coin . ' Daemon at Daemon Server '. $daemon_server_id;
                Log::error($custom_error);
                Log::error($error);

                // Unlock CRON Process
                $redis->del($cron_key);

                return $custom_error;
            }

            // CoinDaemon::sendrawtransaction
            if ($signrawtransaction["complete"] == true) {
                try {
                    $sendrawtransaction = $coind->sendrawtransaction($signrawtransaction['hex']);
                } catch (Exception $e) {
                    $error = $e->getMessage();
                    $custom_error = 'withdrawal_process_aborted: failed sendrawtransaction from ' . $coin . ' Daemon at Daemon Server '. $daemon_server_id;
                    Log::error($custom_error);
                    Log::error($error);

                    // Unlock CRON Process
                    $redis->del($cron_key);

                    return $custom_error;
                }
            } else {
                $custom_error = 'withdrawal_process_aborted: failed to complete signrawtransaction from ' . $coin . ' Daemon at Daemon Server '. $daemon_server_id;
                Log::error($custom_error);

                // Unlock CRON Process
                $redis->del($cron_key);

                return $custom_error;
            }

            // Insert New Raw Transaction Record
            $raw_tx = new RawTransaction;
            $raw_tx->coin = $coin;
            $raw_tx->txid = $sendrawtransaction;
            $raw_tx->status = "pending";
            $raw_tx->daemon_server_id = $daemon_server_id;
            $raw_tx->save();
            $tx_id = $raw_tx->id;

            // DONETODO: Make another CRON Process to confirm Raw Transaction after 3 blocks, and then update those related withdrawals as "completed".
            // DONETODO: tx fee cal + return addr VERY IMPORTANT!!!!!
            // DONETODO: Even more important, get rid of all floating point in all calculation, use BCMATH, which will work nicely with all form inputs, since they are strings
            // DONETODO: BCMATH done in WithdrawalsController, OrdersController, DepositsController
            // DONETODO: clean up UsersController test functions and logics, removed canceled part of OrderCreate

            // Update Withdrawals that are marked as high priority
            $high_id = implode(",", $withdrawals_high_id);
            $medium_id = implode(",", $withdrawals_medium_id);
            $all_id = implode(",", $withdrawals_id);

            if ($no_next_round_withdrawals == true) {
                // No withdrawals are popped off, all withdrawals will be updated as "in_progess" in status
                DB::statement(DB::raw(
                    "UPDATE withdrawals SET status = 'in progress', raw_transaction_id = " . $tx_id . ", " .
                    "updated_at = now() WHERE id IN (" . $all_id . ")"));
            } else {
                // Some withdrawals were popped off, they will be updated as "high" in priority
                DB::statement(DB::raw(
                    "UPDATE withdrawals SET status = CASE " .
                    "WHEN id IN (" . $high_id . ") THEN 'failed, retrying' " .
                    "WHEN id IN (" . $medium_id . ") THEN 'in progress' END, priority = CASE " .
                    "WHEN id IN (" . $high_id . ") THEN 'high' " .
                    "WHEN id IN (" . $medium_id . ") THEN 'medium' END, raw_transaction_id = CASE " .
                    "WHEN id IN (" . $high_id . ") THEN NULL " .
                    "WHEN id IN (" . $medium_id . ") THEN " . $tx_id . " END, " .
                    "updated_at = now() WHERE id IN (" . $all_id . ")"));
            }

            // Unlock CRON Process
            $redis->del($cron_key);

            // Return Finish Message
            return 'Withdrawal Process completed on ' . $coin . ' at Daemon Server ' . $daemon_server_id . '. Txid: ' . $sendrawtransaction;

        }

    }

}