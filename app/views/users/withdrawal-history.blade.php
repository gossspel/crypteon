<div class="row">
    <div class="col-md-12 basic-mainframe">

        <!-- Include Alert Message here, better than showing it in the layout to more easily control css -->
        @include('alert')

        <!-- Page Title -->
        <div class="div-table-title">
            Withdrawal History
        </div>
        <div class="panel panel-default div-table div-table-no-max-height">
            <table class="table table-striped table-hover table-bordered">
                @if (!empty($withdrawals))
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Coin</th>
                            <th class="hidden-xs">Status</th>
                            <th class="visible-lg">Amount</th>
                            <th class="visible-lg">Tx Fee</th>
                            <th>Detail</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($withdrawals as $withdrawal)
                            <tr>
                                <td>{{ $withdrawal->created_at }}</td>
                                <td>{{ $withdrawal->coin }}</td>
                                <td class="hidden-xs">{{ $withdrawal->status }}</td>
                                <td class="visible-lg">{{ $withdrawal->withdrawal_amount }}</td>
                                <td class="visible-lg">{{ $withdrawal->tx_fee }}</td>
                                <td>
                                    <!-- Button trigger modal -->
                                    <button class="btn btn-info" data-toggle="modal" data-target="#myModal-withdrawal-{{ $withdrawal->id }}">
                                        Show
                                    </button>

                                    <!-- Modal -->
                                    <div class="modal detail-modal fade" id="myModal-withdrawal-{{ $withdrawal->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel-withdrawal-{{ $withdrawal->id }}" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h4 class="modal-title" id="myModalLabel-withdrawal-{{ $withdrawal->id }}">Withdrawal Detail</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="detail-div-row">
                                                        <div>Date:</div>
                                                        <div class="up">{{ $withdrawal->created_at }}</div>
                                                    </div>
                                                    <div class="detail-div-row">
                                                        <div>Coin:</div>
                                                        <div class="up">{{ $withdrawal->coin }}</div>
                                                    </div>
                                                    <div class="detail-div-row">
                                                        <div>Status:</div>
                                                        <div class="up">{{ $withdrawal->status }}</div>
                                                    </div>
                                                    <div class="detail-div-row">
                                                        <div>Sent To:</div>
                                                        <div class="up too-long-break">{{ $withdrawal->to_address }}</div>
                                                    </div>
                                                    <div class="detail-div-row">
                                                        <div>Amount:</div>
                                                        <div class="up">{{ $withdrawal->withdrawal_amount . ' ' . $withdrawal->coin }}</div>
                                                    </div>
                                                    <div class="detail-div-row">
                                                        <div>Tx Fee:</div>
                                                        <div class="up too-long-break">{{ $withdrawal->tx_fee . ' ' . $withdrawal->coin }}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                @else
                    <tr>
                        <td>You don't have any withdrawals.</td>
                    </tr>
                @endif
            </table>
        </div>
    </div>
</div>