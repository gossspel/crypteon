<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditFieldsToTransientOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('transient_orders', function(Blueprint $table)
		{
            $table->dropColumn('buy_amount');
            $table->dropColumn('sell_amount');
            $table->dropColumn('filled_amount');
            $table->dropColumn('status');
            $table->decimal('bought_amount', 20, 8);
            $table->decimal('sold_amount', 20, 8);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('transient_orders', function(Blueprint $table)
		{
            $table->decimal('sell_amount', 20, 8);
            $table->decimal('buy_amount', 20, 8);
            $table->decimal('filled_amount', 20, 8);
            $table->string('status', 20);
		});
	}

}