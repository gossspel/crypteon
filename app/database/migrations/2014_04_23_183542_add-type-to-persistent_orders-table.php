<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeToPersistentOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('persistent_orders', function(Blueprint $table)
        {
            $table->string('mode', 20); // Sell or Buy
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('persistent_orders', function(Blueprint $table)
        {
            $table->dropColumn('mode');
        });
	}

}
