<?php

class InternalTransaction extends Eloquent {

    protected $table = 'internal_transactions';

    // There exists a N to 1 relationship between InternalTransaction{Entity} and Order{Entity}.
    public function order()
    {
        return $this->belongsTo('Order');
    }

    // There exists a N to 1 relationship between InternalTransaction{Entity} and DaemonServer{Entity}.
    public function daemonServer()
    {
        return $this->belongsTo('DaemonServer');
    }

    // There exists a N to 1 relationship between InternalTransaction{Entity} and {Entity}.
    public function user()
    {
        return $this->belongsTo('User');
    }

    // There exists a 1 to 1 relationship between InternalTransaction{Entity} and Withdrawal{Entity}.
    public function withdrawal()
    {
        return $this->belongsTo('Withdrawal');
    }



}