<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteSomeFieldsInDepositsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('deposits', function(Blueprint $table)
        {
            $table->dropColumn('total_received_before');
            $table->dropColumn('total_received_after');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('deposits', function(Blueprint $table)
        {
            $table->decimal('total_received_before', 20, 8);
            $table->decimal('total_received_after', 20, 8);
        });
	}

}
