<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'MarketsController@getView');
Route::controller('users', 'UsersController');
Route::controller('orders', 'OrdersController');
Route::controller('markets', 'MarketsController');
Route::controller('withdrawals', 'WithdrawalsController');
Route::controller('deposits', 'DepositsController');
Route::controller('internal-transactions', 'InternalTransactionsController');
Route::controller('invitations', 'InvitationsController');
Route::controller('raw-transactions', 'RawTransactionsController');
Route::controller('password', 'RemindersController');
Route::group(array('prefix' => 'api/process/v1'), function()
{
    Route::controller('api-markets', 'ApiMarketsController');
});