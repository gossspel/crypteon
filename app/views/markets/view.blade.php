<div class="row">
    <div class="col-lg-12 basic-mainframe alert-holder">
        <!-- Include Alert Message here, better than showing it in the layout to more easily control css -->
        @include('row-alert')
        <div class="alert alert-success alert-dismissable row-alert" id="alert-container">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            Crypteon is operating in private beta mode with 0.00% trade fee until all legal matters of business establishment are taken care of.
            {{ HTML::link('https://bitcointalk.org/index.php?topic=605486.0', 'Check out our Bitcointalk thread', array('target'=>'_blank')) }}
        </div>
    </div>
    <div class="col-lg-12">
        <div class="main-title">
            {{ $market_array['pair'] }}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-8">
        <div class="crypteon-chart" id="crypteon-chart-div"></div>
    </div>
    <div class="col-lg-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-tasks fa-fw"></i> Market Data
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="list-group">
                    <div class="list-group-item">
                        <span class="market-data market-data-label">
                            Last Rate:
                        </span>
                        <span class="market-data market-data-value">
                            {{ $market_array['last_rate'] . ' ' . $market_array['base'] }}
                        </span>
                    </div>
                    <div class="list-group-item">
                        <span class="market-data market-data-label">
                            24 HR High:
                        </span>
                        <span class="market-data market-data-value">
                            {{ $market_array['rate_daily_high'] . ' ' . $market_array['base'] }}
                        </span>
                    </div>
                    <div class="list-group-item">
                        <span class="market-data market-data-label">
                            24 HR Low:
                        </span>
                        <span class="market-data market-data-value">
                            {{ $market_array['rate_daily_low'] . ' ' . $market_array['base'] }}
                        </span>
                    </div>
                    <div class="list-group-item">
                        <span class="market-data market-data-label">
                            24 HR Volume:
                        </span>
                        <span class="market-data market-data-value">
                            {{ $market_array['volume_daily'] . ' ' . $market_array['base'] }}
                        </span>
                    </div>
                    <div class="list-group-item">
                        <span class="market-data market-data-label">
                            Coin Info:
                        </span>
                        <span class="market-data market-data-value">
                            {{ HTML::link($market_array['bitcointalk_link'], 'Bitcointalk Thread', array('target'=>'_blank')) }}
                        </span>
                    </div>
                </div>
            </div>
            <!-- /.panel-body -->
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs nav-justified">
            <li class="active"><a href="#open" data-toggle="tab">Your Open {{ $market_array['coin'] }} Orders</a></li>
            <li><a href="#completed" data-toggle="tab">Your Completed {{ $market_array['coin'] }} Orders</a></li>
            <li><a href="#canceled" data-toggle="tab">Your Canceled {{ $market_array['coin'] }} Orders</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div id="open" class="tab-pane fade in active">
                <div class="panel panel-default div-table">
                    <table class="table table-striped table-hover table-bordered">
                        <tr>
                            <td>Please {{ $login_link }} or {{ $register_link }} to view your Open {{ $market_array['coin'] }} Orders</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div id="canceled" class="tab-pane fade">
                <div class="panel panel-default div-table">
                    <table class="table table-striped table-hover table-bordered">
                        <tr>
                            <td>Please {{ $login_link }} or {{ $register_link }} to view your Completed {{ $market_array['coin'] }} Orders</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div id="completed" class="tab-pane fade">
                <div class="panel panel-default div-table">
                    <table class="table table-striped table-hover table-bordered">
                        <tr>
                            <td>Please {{ $login_link }} or {{ $register_link }} to view your Canceled {{ $market_array['coin'] }} Orders</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-8">
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading div-form-title">
                        Buy {{ $market_array['coin'] }}
                    </div>
                    <div class="panel-body">
                        <form class="form-order">
                            <div class="form-group">
                                {{ Form::label('original_buy_amount', 'Buy Amount') }}
                                {{ Form::text('original_buy_amount', $market_array['min_amount'], array('class'=>'form-control', 'id'=>'b-amount', 'placeholder'=>'Amount')) }}
                            </div>
                            @if (!($errors->isEmpty()) && !empty($errors->first('original_buy_amount')))
                            <div class="popover bottom">
                                <div class="arrow"></div>
                                <h3 class="popover-title">Buy Amount Error</h3>
                                <div class="popover-content">
                                    <p>{{ $errors->first('original_buy_amount') }}</p>
                                </div>
                            </div>
                            @endif
                            <div class="form-group">
                                {{ Form::label('buy_rate', 'Price Per '. $market_array['coin'] . ' (in ' . $market_array['base'] . ')') }}
                                {{ Form::text('buy_rate', $market_array['last_rate'], array('class'=>'form-control', 'id'=>'b-rate', 'placeholder'=>'Price Per ' . $market_array['coin'])) }}
                            </div>
                            @if (!($errors->isEmpty()) && !empty($errors->first('buy_rate')))
                            <div class="popover bottom">
                                <div class="arrow"></div>
                                <h3 class="popover-title">Rate Error</h3>
                                <div class="popover-content">
                                    <p>{{ $errors->first('buy_rate') }}</p>
                                </div>
                            </div>
                            @endif
                            <div class="well">
                                <ul class="ul-form-panel">
                                    <li class="li-form-panel">
                                        <span>Change in {{ $market_array['coin'] }} Balance</span>
                                        <span class="span-form-panel up" id="b-coin">+ 0.00000000 {{ $market_array['coin'] }}</span>
                                    </li>
                                    <li class="li-form-panel">
                                        <span>Change in {{ $market_array['base'] }} Balance</span>
                                        <span class="span-form-panel" id="b-base">- 0.00000000 {{ $market_array['base'] }}</span>
                                    </li>
                                    <li class="li-form-panel">
                                        <span>Fee ({{ $market_array['fee_rate_percent'] }}%):</span>
                                        <span class="span-form-panel" id="b-fee">- 0.00000000 {{ $market_array['base'] }}</span>
                                    </li>
                                    <li class="li-form-panel">
                                        <span>Net Change in {{ $market_array['base'] }} Balance</span>
                                        <span class="span-form-panel down" id="b-net-base">- 0.00000000 {{ $market_array['base'] }}</span>
                                    </li>
                                </ul>
                            </div>
                            {{ Form::hidden('market_id', $market_array['id'], array('class'=>'form-control')) }}
                            {{ Form::hidden('mode', 'b', array('class'=>'form-control')) }}
                            {{ Form::button('Submit', array('class'=>'btn btn-success btn-block submit-alert'))}}
                        </form>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading div-form-title">ORDERS selling {{ $market_array['coin'] }}</div>
                    <div class="div-table">
                        <table class="table order-table table-striped table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Rate</th>
                                <th>{{ $market_array['coin'] }}</th>
                                <th>{{ $market_array['base'] }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($sell_orders as $sell_order)
                            <tr class="sell_order">
                                <td class="sell_order_rate">{{ $sell_order->rate }}</td>
                                <td class="sell_order_sum_sell">{{ $sell_order->sum_sell }}</td>
                                <td>{{ $sell_order->sum_buy }}</td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading div-form-title">
                        Sell {{ $market_array['coin'] }}
                    </div>
                    <div class="panel-body">
                        <form class="form-order">
                            <div class="form-group">
                                {{ Form::label('original_sell_amount', 'Sell Amount') }}
                                {{ Form::text('original_sell_amount', $market_array['min_amount'], array('class'=>'form-control', 'id'=>'s-amount', 'placeholder'=>'Amount')) }}
                            </div>
                            @if (!($errors->isEmpty()) && !empty($errors->first('original_sell_amount')))
                            <div class="popover bottom">
                                <div class="arrow"></div>
                                <h3 class="popover-title">Sell Amount Error</h3>
                                <div class="popover-content">
                                    <p>{{ $errors->first('original_sell_amount') }}</p>
                                </div>
                            </div>
                            @endif
                            <div class="form-group">
                                {{ Form::label('sell_rate', 'Price Per '. $market_array['coin'] . ' (in ' . $market_array['base'] . ')') }}
                                {{ Form::text('sell_rate', $market_array['last_rate'], array('class'=>'form-control', 'id'=>'s-rate', 'placeholder'=>'Price Per ' . $market_array['coin'])) }}
                            </div>
                            @if (!($errors->isEmpty()) && !empty($errors->first('sell_rate')))
                            <div class="popover bottom">
                                <div class="arrow"></div>
                                <h3 class="popover-title">Rate Error</h3>
                                <div class="popover-content">
                                    <p>{{ $errors->first('sell_rate') }}</p>
                                </div>
                            </div>
                            @endif
                            <div class="well">
                                <ul class="ul-form-panel">
                                    <li class="li-form-panel">
                                        <span>Change in {{ $market_array['coin'] }} Balance</span>
                                        <span class="span-form-panel down" id="s-coin">- 0.00000000 {{ $market_array['coin'] }}</span>
                                    </li>
                                    <li class="li-form-panel">
                                        <span>Change in {{ $market_array['base'] }} Balance</span>
                                        <span class="span-form-panel" id="s-base">+ 0.00000000 {{ $market_array['base'] }}</span>
                                    </li>
                                    <li class="li-form-panel">
                                        <span>Fee ({{ $market_array['fee_rate_percent'] }}%):</span>
                                        <span class="span-form-panel" id="s-fee">- 0.00000000 {{ $market_array['base'] }}</span>
                                    </li>
                                    <li class="li-form-panel">
                                        <span>Net Change in {{ $market_array['base'] }} Balance</span>
                                        <span class="span-form-panel up" id="s-net-base">+ 0.00000000 {{ $market_array['base'] }}</span>
                                    </li>
                                </ul>
                            </div>
                            {{ Form::hidden('market_id', $market_array['id'], array('class'=>'form-control')) }}
                            {{ Form::hidden('mode', 's', array('class'=>'form-control')) }}
                            {{ Form::button('Submit', array('class'=>'btn btn-success btn-block submit-alert'))}}
                        </form>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading div-form-title">ORDERS buying {{ $market_array['coin'] }}</div>
                    <div class="div-table">
                        <table class="table order-table table-striped table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Rate</th>
                                <th>{{ $market_array['coin'] }}</th>
                                <th>{{ $market_array['base'] }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($buy_orders as $buy_order)
                            <tr class="buy_order">
                                <td class="buy_order_rate">{{ $buy_order->rate }}</td>
                                <td class="buy_order_sum_buy">{{ $buy_order->sum_buy }}</td>
                                <td>{{ $buy_order->sum_sell }}</td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading div-form-title">Recent Market History</div>
                    <div class="div-table">
                        <table class="table table-striped table-hover table-bordered">
                            <thead>
                            <tr>
                                <th class="hidden-xs hidden-sm">Date</th>
                                <th>Type</th>
                                <th>Rate</th>
                                <th class="hidden-xs hidden-sm">{{ $market_array['coin'] }}</th>
                                <th class="hidden-xs hidden-sm">{{ $market_array['base'] }}</th>
                                <th class="hidden-lg hidden-md">Detail</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($finished_orders as $finished_order)
                            <tr>
                                <td class="hidden-xs hidden-sm">{{ $finished_order->created_at }}</td>
                                <td>{{ $finished_order->mode }}</td>
                                <td>{{ $finished_order->rate }}</td>
                                @if ($finished_order->mode == 'Buy')
                                <td class="hidden-xs hidden-sm">{{ $finished_order->bought_amount }}</td>
                                <td class="hidden-xs hidden-sm">{{ $finished_order->sold_amount }}</td>
                                @else
                                <td class="hidden-xs hidden-sm">{{ $finished_order->sold_amount }}</td>
                                <td class="hidden-xs hidden-sm">{{ $finished_order->bought_amount }}</td>
                                @endif
                                <td class="hidden-lg hidden-md">
                                    <button class="btn btn-info" data-toggle="modal" data-target="#myModal-finished-order-{{ $finished_order->id }}">
                                        Show
                                    </button>
                                    <!-- Modal -->
                                    <div class="modal detail-modal fade" id="myModal-finished-order-{{ $finished_order->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel-finished-order-{{ $finished_order->id }}" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h4 class="modal-title" id="myModalLabel-finished-order-{{ $finished_order->id }}">Order History Detail</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="detail-div-row">
                                                        <div>Date:</div>
                                                        <div class="up">{{ $finished_order->created_at }}</div>
                                                    </div>
                                                    <div class="detail-div-row">
                                                        <div>Type:</div>
                                                        <div class="up">{{ $finished_order->mode }}</div>
                                                    </div>
                                                    <div class="detail-div-row">
                                                        <div>Rate:</div>
                                                        <div class="up">{{ $finished_order->rate }}</div>
                                                    </div>
                                                    @if ($finished_order->mode == 'Buy')
                                                    <div class="detail-div-row">
                                                        <div>Bought:</div>
                                                        <div class="up">{{ $finished_order->bought_amount . ' ' . $market_array['coin'] }}</div>
                                                    </div>
                                                    <div class="detail-div-row">
                                                        <div>Sold:</div>
                                                        <div class="up">{{ $finished_order->sold_amount . ' ' . $market_array['base'] }}</div>
                                                    </div>
                                                    @else
                                                    <div class="detail-div-row">
                                                        <div>Sold:</div>
                                                        <div class="up">{{ $finished_order->sold_amount . ' ' . $market_array['coin'] }}</div>
                                                    </div>
                                                    <div class="detail-div-row">
                                                        <div>Bought:</div>
                                                        <div class="up">{{ $finished_order->bought_amount . ' ' . $market_array['base'] }}</div>
                                                    </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        @include('twitter-widget')
    </div>
</div>


@section('plscripts')
    {{ HTML::script('https://code.highcharts.com/stock/highstock.js') }}
    {{ HTML::script('https://code.highcharts.com/stock/modules/exporting.js') }}
    <script>
        $( document ).ready(function() {
            // Candlestick Example
            $.getJSON("{{ url('api/process/v1/api-markets/raw-stat', array($market_array['id']), true) }}", function(data) {

                // split the data set into ohlc and volume
                var ohlc = [],
                        volume = [],
                        dataLength = data.length;

                for (i = 0; i < dataLength; i++) {
                    ohlc.push([
                        data[i][0], // the date
                        data[i][1], // open
                        data[i][2], // high
                        data[i][3], // low
                        data[i][4] // close
                    ]);

                    volume.push([
                        data[i][0], // the date
                        data[i][5] // the volume
                    ])
                }

                // set the allowed units for data grouping
                var groupingUnits = [[
                    'minute',                         // unit name
                    [10, 30]                             // allowed multiples
                ], [
                    'hour',
                    [1, 6]
                ], [
                    'day',
                    [1]
                ]];

                // create the chart
                $('#crypteon-chart-div').highcharts('StockChart', {
                    credits: {
                        enabled: false
                    },
                    exporting: {
                        enabled: false
                    },
                    rangeSelector: {
                        buttons: [{
                            type: 'minute',
                            count: 360,
                            text: '6h'
                        }, {
                            type: 'day',
                            count: 1,
                            text: '24h'
                        }, {
                            type: 'day',
                            count: 3,
                            text: '3d'
                        }, {
                            type: 'week',
                            count: 1,
                            text: '1w'
                        }, {
                            type: 'all',
                            text: 'Max'
                        }],
                        inputEnabled: false,
                        selected: 3
                    },
                    navigator: {
                        enabled: false
                    },
                    plotOptions: {
                        candlestick: {
                            color: '#D2322D',
                            upColor: '#47A447'
                        }
                    },
                    scrollbar: {
                        enabled: false
                    },
                    title: {
                        text: "{{ $market_array['pair'] }}"
                    },
                    tooltip: {
                        valueDecimals: 8,
                        valueSuffix: " {{ $market_array['base'] }}"
                    },
                    yAxis: [{
                        labels: {
                            align: 'right',
                            formatter: function() {
                                return this.value.toFixed(8) + " {{ $market_array['base'] }}";
                            },
                            x: -3
                        },
                        min: 0,
                        title: {
                            text: "Price Per {{ $market_array['coin'] }}"
                        },
                        height: '60%',
                        lineWidth: 2
                    }, {
                        labels: {
                            align: 'right',
                            formatter: function() {
                                return this.value.toFixed(4) + " {{ $market_array['base'] }}";
                            },
                            x: -3
                        },
                        min: 0,
                        title: {
                            text: 'Volume'
                        },
                        top: '65%',
                        height: '35%',
                        offset: 0,
                        lineWidth: 2
                    }],
                    series: [{
                        type: 'candlestick',
                        name: "{{ $market_array['pair'] }}",
                        data: ohlc,
                        dataGrouping: {
                            units: groupingUnits
                        }
                    }, {
                        type: 'column',
                        name: 'Volume',
                        data: volume,
                        yAxis: 1,
                        dataGrouping: {
                            units: groupingUnits
                        }
                    }]
                });
            });

            // Initial Calculation
            var raw_s_coin_init = $("#s-amount").val();
            var raw_s_rate_init = $("#s-rate").val();
            var raw_s_base_init = parseFloat(raw_s_coin_init * raw_s_rate_init);
            var raw_s_fee_init = raw_s_base_init * "{{ $market_array['fee_rate'] }}";
            var cooked_s_net_base_init = (raw_s_base_init - raw_s_fee_init).toFixed(8);
            var cooked_s_base_init = raw_s_base_init.toFixed(8);
            var cooked_s_fee_init = raw_s_fee_init.toFixed(8);

            $("#s-coin").text("- " + raw_s_coin_init + " {{ $market_array['coin'] }}");
            $("#s-base").text("+ " + cooked_s_base_init + " {{ $market_array['base'] }}");
            $("#s-fee").text("- " + cooked_s_fee_init + " {{ $market_array['base'] }}");
            $("#s-net-base").text("+ " + cooked_s_net_base_init + " {{ $market_array['base'] }}");

            var raw_b_coin_init = $("#b-amount").val();
            var raw_b_rate_init = $("#b-rate").val();
            var raw_b_base_init = parseFloat(raw_b_coin_init * raw_b_rate_init);
            var raw_b_fee_init = raw_b_base_init * "{{ $market_array['fee_rate'] }}";
            var cooked_b_net_base_init = (raw_b_base_init + raw_b_fee_init).toFixed(8);
            var cooked_b_base_init = raw_b_base_init.toFixed(8);
            var cooked_b_fee_init = raw_b_fee_init.toFixed(8);

            $("#b-coin").text("+ " + raw_b_coin_init + " {{ $market_array['coin'] }}");
            $("#b-base").text("- " + cooked_b_base_init + " {{ $market_array['base'] }}");
            $("#b-fee").text("- " + cooked_b_fee_init + " {{ $market_array['base'] }}");
            $("#b-net-base").text("- " + cooked_b_net_base_init + " {{ $market_array['base'] }}");

            // Live Calculation
            $("#s-amount").keyup(function() {
                var raw_s_coin_string = $(this).val();
                var raw_s_coin = parseFloat(raw_s_coin_string);

                if ((jQuery.type(raw_s_coin) !== 'number') || (isNaN(raw_s_coin))) {
                    var raw_s_coin = 0;
                }

                var raw_s_rate_string = $("#s-rate").val();
                var raw_s_rate = parseFloat(raw_s_rate_string);

                if ((jQuery.type(raw_s_rate) !== 'number') || (isNaN(raw_s_rate))) {
                    var raw_s_rate = 0;
                }

                var raw_s_base = raw_s_coin * raw_s_rate;
                var raw_s_fee = raw_s_base * "{{ $market_array['fee_rate'] }}";
                var cooked_s_net_base = (raw_s_base - raw_s_fee).toFixed(8);
                var cooked_s_fee = raw_s_fee.toFixed(8);
                var cooked_s_base = raw_s_base.toFixed(8);
                var cooked_s_coin = raw_s_coin.toFixed(8);

                $("#s-coin").text("- " + cooked_s_coin + " {{ $market_array['coin'] }}");
                $("#s-base").text("+ " + cooked_s_base + " {{ $market_array['base'] }}");
                $("#s-fee").text("- " + cooked_s_fee + " {{ $market_array['base'] }}");
                $("#s-net-base").text("+ " + cooked_s_net_base + " {{ $market_array['base'] }}");
            });
            $("#s-rate").keyup(function() {
                var raw_s_coin_string = $("#s-amount").val();
                var raw_s_coin = parseFloat(raw_s_coin_string);

                if ((jQuery.type(raw_s_coin) !== 'number') || (isNaN(raw_s_coin))) {
                    var raw_s_coin = 0;
                }

                var raw_s_rate_string = $(this).val();
                var raw_s_rate = parseFloat(raw_s_rate_string);

                if ((jQuery.type(raw_s_rate) !== 'number') || (isNaN(raw_s_rate))) {
                    var raw_s_rate = 0;
                }

                var raw_s_base = raw_s_coin * raw_s_rate;
                var raw_s_fee = raw_s_base * "{{ $market_array['fee_rate'] }}";
                var cooked_s_net_base = (raw_s_base - raw_s_fee).toFixed(8);
                var cooked_s_fee = raw_s_fee.toFixed(8);
                var cooked_s_base = raw_s_base.toFixed(8);
                var cooked_s_coin = raw_s_coin.toFixed(8);

                $("#s-coin").text("- " + cooked_s_coin + " {{ $market_array['coin'] }}");
                $("#s-base").text("+ " + cooked_s_base + " {{ $market_array['base'] }}");
                $("#s-fee").text("- " + cooked_s_fee + " {{ $market_array['base'] }}");
                $("#s-net-base").text("+ " + cooked_s_net_base + " {{ $market_array['base'] }}");
            });
            $("#s-amount").focusout(function() {
                var raw_s = $(this).val();
                var raw_s = parseFloat(raw_s);
                if ((jQuery.type(raw_s) !== 'number') || (isNaN(raw_s))) {
                    var raw_s = 0;
                }
                var cooked_s = raw_s.toFixed(8);
                $(this).val(cooked_s);
            });
            $("#s-rate").focusout(function() {
                var raw_s = $(this).val();
                var raw_s = parseFloat(raw_s);
                if ((jQuery.type(raw_s) !== 'number') || (isNaN(raw_s))) {
                    var raw_s = 0;
                }
                var cooked_s = raw_s.toFixed(8);
                $(this).val(cooked_s);
            });
            $("#b-amount").keyup(function() {
                var raw_b_coin_string = $(this).val();
                var raw_b_coin = parseFloat(raw_b_coin_string);

                if ((jQuery.type(raw_b_coin) !== 'number') || (isNaN(raw_b_coin))) {
                    var raw_b_coin = 0;
                }

                var raw_b_rate_string = $("#b-rate").val();
                var raw_b_rate = parseFloat(raw_b_rate_string);

                if ((jQuery.type(raw_b_rate) !== 'number') || (isNaN(raw_b_rate))) {
                    var raw_b_rate = 0;
                }

                var raw_b_base = raw_b_coin * raw_b_rate;
                var raw_b_fee = raw_b_base * "{{ $market_array['fee_rate'] }}";
                var cooked_b_net_base = (raw_b_base + raw_b_fee).toFixed(8);
                var cooked_b_fee = raw_b_fee.toFixed(8);
                var cooked_b_base = raw_b_base.toFixed(8);
                var cooked_b_coin = raw_b_coin.toFixed(8);

                $("#b-coin").text("+ " + cooked_b_coin + " {{ $market_array['coin'] }}");
                $("#b-base").text("- " + cooked_b_base + " {{ $market_array['base'] }}");
                $("#b-fee").text("- " + cooked_b_fee + " {{ $market_array['base'] }}");
                $("#b-net-base").text("- " + cooked_b_net_base + " {{ $market_array['base'] }}");
            });
            $("#b-rate").keyup(function() {
                var raw_b_coin_string = $("#b-amount").val();
                var raw_b_coin = parseFloat(raw_b_coin_string);

                if ((jQuery.type(raw_b_coin) !== 'number') || (isNaN(raw_b_coin))) {
                    var raw_b_coin = 0;
                }

                var raw_b_rate_string = $(this).val();
                var raw_b_rate = parseFloat(raw_b_rate_string);

                if ((jQuery.type(raw_b_rate) !== 'number') || (isNaN(raw_b_rate))) {
                    var raw_b_rate = 0;
                }

                var raw_b_base = raw_b_coin * raw_b_rate;
                var raw_b_fee = raw_b_base * "{{ $market_array['fee_rate'] }}";
                var cooked_b_net_base = (raw_b_base + raw_b_fee).toFixed(8);
                var cooked_b_fee = raw_b_fee.toFixed(8);
                var cooked_b_base = raw_b_base.toFixed(8);
                var cooked_b_coin = raw_b_coin.toFixed(8);

                $("#b-coin").text("+ " + cooked_b_coin + " {{ $market_array['coin'] }}");
                $("#b-base").text("- " + cooked_b_base + " {{ $market_array['base'] }}");
                $("#b-fee").text("- " + cooked_b_fee + " {{ $market_array['base'] }}");
                $("#b-net-base").text("- " + cooked_b_net_base + " {{ $market_array['base'] }}");
            });
            $("#b-amount").focusout(function() {
                var raw_b = $(this).val();
                var raw_b = parseFloat(raw_b);
                if ((jQuery.type(raw_b) !== 'number') || (isNaN(raw_b))) {
                    var raw_b = 0;
                }
                var cooked_b = raw_b.toFixed(8);
                $(this).val(cooked_b);
            });
            $("#b-rate").focusout(function() {
                var raw_b = $(this).val();
                var raw_b = parseFloat(raw_b);
                if ((jQuery.type(raw_b) !== 'number') || (isNaN(raw_b))) {
                    var raw_b = 0;
                }
                var cooked_b = raw_b.toFixed(8);
                $(this).val(cooked_b);
            });

            // Message popup
            $(".submit-alert").on('click', function() {
                var first = '<div class="alert alert-info alert-dismissable row-alert" id="alert-container">';
                var second = '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                var third = 'Please ' + '{{ $login_link }}' + ' or ' + '{{ $register_link }}' + ' to submit orders.';
                var fourth = '</div>';
                var all = first + second + third + fourth;
                $("#alert-container").remove();
                $(".alert-holder").prepend(all);
                window.scrollTo(0, 0);
            });

            $(".sell_order").on('click', function() {
                var sell_rate = $(this).find(".sell_order_rate").html();
                var sell_rate = parseFloat(sell_rate);
                if ((jQuery.type(sell_rate) !== 'number') || (isNaN(sell_rate))) {
                    var sell_rate = 0;
                }

                var sum_sell = $(this).find(".sell_order_sum_sell").html();
                var sum_sell = parseFloat(sum_sell);
                if ((jQuery.type(sum_sell) !== 'number') || (isNaN(sum_sell))) {
                    var sum_sell = 0;
                }

                var raw_b_base = sum_sell * sell_rate;
                var raw_b_fee = raw_b_base * "{{ $market_array['fee_rate'] }}";
                var cooked_b_net_base = (raw_b_base + raw_b_fee).toFixed(8);
                var cooked_b_fee = raw_b_fee.toFixed(8);
                var cooked_b_base = raw_b_base.toFixed(8);
                var cooked_b_coin = sum_sell.toFixed(8);
                var sell_rate = sell_rate.toFixed(8);

                $("#b-rate").val(sell_rate);
                $("#b-amount").val(cooked_b_coin);
                $("#b-coin").text("+ " + cooked_b_coin + " {{ $market_array['coin'] }}");
                $("#b-base").text("- " + cooked_b_base + " {{ $market_array['base'] }}");
                $("#b-fee").text("- " + cooked_b_fee + " {{ $market_array['base'] }}");
                $("#b-net-base").text("- " + cooked_b_net_base + " {{ $market_array['base'] }}");
            });

            $(".buy_order").on('click', function() {
                var buy_rate = $(this).find(".buy_order_rate").html();
                var buy_rate = parseFloat(buy_rate);
                if ((jQuery.type(buy_rate) !== 'number') || (isNaN(buy_rate))) {
                    var buy_rate = 0;
                }

                var sum_buy = $(this).find(".buy_order_sum_buy").html();
                var sum_buy = parseFloat(sum_buy);
                if ((jQuery.type(sum_buy) !== 'number') || (isNaN(sum_buy))) {
                    var sum_buy = 0;
                }

                var raw_s_base = sum_buy * buy_rate;
                var raw_s_fee = raw_s_base * "{{ $market_array['fee_rate'] }}";
                var cooked_s_net_base = (raw_s_base - raw_s_fee).toFixed(8);
                var cooked_s_fee = raw_s_fee.toFixed(8);
                var cooked_s_base = raw_s_base.toFixed(8);
                var cooked_s_coin = sum_buy.toFixed(8);
                var buy_rate = buy_rate.toFixed(8);

                $("#s-rate").val(buy_rate);
                $("#s-amount").val(cooked_s_coin);
                $("#s-coin").text("- " + cooked_s_coin + " {{ $market_array['coin'] }}");
                $("#s-base").text("+ " + cooked_s_base + " {{ $market_array['base'] }}");
                $("#s-fee").text("- " + cooked_s_fee + " {{ $market_array['base'] }}");
                $("#s-net-base").text("+ " + cooked_s_net_base + " {{ $market_array['base'] }}");
            });
        });
    </script>
@stop