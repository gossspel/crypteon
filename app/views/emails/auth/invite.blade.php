<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<p>Hi there!</p>

		<p>
            Thank you for your interest. Here is your intiviation code: {{ $code }}
		</p>
        <p>
            The invitation code is required by the registration link below:
        </p>
        <p>
        {{ $link }}
        </p>
        <p>
            <div>Cheers,</div>
            <div>Team Crypteon</div>
        </p>
	</body>
</html>