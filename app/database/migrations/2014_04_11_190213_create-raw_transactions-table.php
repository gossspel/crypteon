<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRawTransactionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('raw_transactions', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('coin', 20); // which coin daemon
            $table->string('txid', 255); // transaction id returned from sendrawtransaction
            $table->string('status', 40); // pending as default, after 6 confirmation change to confirmed
            $table->integer('daemon_server_id');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('raw_transactions');
	}

}
