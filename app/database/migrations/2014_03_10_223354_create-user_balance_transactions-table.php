<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserBalanceTransactionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_balance_transactions', function(Blueprint $table)
		{
            $table->increments('id');
            $table->string('instance_type', 40); // what kind of balance, ie: ltc_order_balance, ltc_available_balance
            $table->decimal('balance_before_transaction', 20, 8);
            $table->decimal('balance_after_transaction', 20, 8);
            $table->decimal('transaction_amount', 20, 8);
            $table->string('type', 40); // "order_related", "withdrawal_related", "deposit_related"
            $table->integer('order_id')->nullable();
            $table->integer('withdrawal_id')->nullable();
            $table->integer('deposit_id')->nullable();
            $table->integer('user_id');
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_balance_transactions');
	}

}
