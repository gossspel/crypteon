<?php

class Profile extends Eloquent {

    protected $table = 'profiles';

    // There exists a 1 to 1 relationship between Profile{Entity} and User{Entity}
    public function user()
    {
        return $this->belongsTo('User', 'id');
    }

    // There exists a N to 1 relationship between Profile{Entity} and DaemonServer{Entity}.
    public function daemonServer()
    {
        return $this->belongsTo('DaemonServer');
    }
}