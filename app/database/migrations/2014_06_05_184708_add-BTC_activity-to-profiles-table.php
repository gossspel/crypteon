<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBTCActivityToProfilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('profiles', function(Blueprint $table)
        {
            $table->string('BTC_activity', 20)->default('inactive');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('profiles', function(Blueprint $table)
        {
            $table->dropColumn('BTC_activity');
        });
	}

}
