<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('orders', function(Blueprint $table)
        {
            $table->string('fee_coin',20); // Only BTC, LTC or DOGE
            $table->decimal('original_fee',20,8);
            $table->decimal('collected_fee',20,8);
            $table->decimal('pending_fee',20,8);
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('orders', function(Blueprint $table)
        {
            $table->dropColumn('fee_coin');
            $table->dropColumn('original_fee');
            $table->dropColumn('collected_fee');
            $table->dropColumn('pending_fee');
        });
	}

}
