<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTxFeeToWithdrawalsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('withdrawals', function(Blueprint $table)
        {
            $table->decimal('tx_fee', 20, 8);
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('withdrawals', function(Blueprint $table)
        {
            $table->dropColumn('tx_fee');
        });
	}

}
