<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAvailableBuyAmountToPersistentOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('persistent_orders', function(Blueprint $table)
		{
            $table->decimal('available_buy_amount',20,8);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('persistent_orders', function(Blueprint $table)
		{
            $table->dropColumn('available_buy_amount');
		});
	}

}