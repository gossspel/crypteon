<?php

class InvitationsController extends BaseController {

    protected $layout = "layouts.test";

    public function __construct() {
        $this->beforeFilter('csrf', array('on' => 'post'));
        $this->beforeFilter('auth');
    }

    public function getCreate() {
        // Check if Admin
        $user = Auth::user();
        if ($user->group != 'admin') {
            return Redirect::to('markets/secure-view')
                ->with('message', "Oops, we couldn't find what you're looking for.")
                ->with('message-level', 'alert-info');
        }

        $this->layout->content = View::make('invitations.create');
    }

    public function postCreate() {
        // Check if Admin
        $user = Auth::user();
        if ($user->group != 'admin') {
            return Redirect::to('markets/secure-view')
                ->with('message', "Oops, we couldn't find what you're looking for.")
                ->with('message-level', 'alert-info');
        }

        $validator = Validator::make(Input::all(), Invitation::$rules);

        if ($validator->passes()) {
            // Create Invitation key
            $email = Input::get('email');
            $secret = GoogleAuthenticator::userRandomKey();

            // Insert to invitations table
            $invitation = new Invitation;
            $invitation->invitation_code = $secret;
            $invitation->email = $email;
            $invitation->save();

            // Send Activation Email
            $reg_link = URL::to('users/register');

            $user_data = array('link' => $reg_link, 'code' => $secret);

            Mail::later(10, 'emails.auth.invite', $user_data, function($message) use ($email)
            {
                $message->to($email)->subject('Crypteon Invitation');
            });

            return Redirect::to('markets/secure-view')
                ->with('message', 'Invitation sent to ' . $email . '!')
                ->with('message-level', 'alert-success');
        } else {
            // validation has failed, display error messages
            return Redirect::to('invitations/create')
                ->with('message', 'This email has already received an invitation before.')
                ->with('message-level', 'alert-danger');
        }
    }
}