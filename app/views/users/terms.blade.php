<div class="row">
    <div class="col-lg-12 basic-mainframe">

        <!-- Include Alert Message here, better than showing it in the layout to more easily control css -->
        @include('alert')

        <!-- Page Title -->
        <div class="div-table-title">
            Terms
        </div>
        <div class="panel panel-default">
            <div class="panel-heading div-about-title">
                (Terms last updated on 5/21/2014)
            </div>
            <div class="panel-body">
                <p class="bold-font">1.) General Term</p>
                <p>
                    These Terms of Use form a binding contract between you and Crypteon (crypteon.net). 
                    By accessing or using the Website (crypteon.net), you signify that you have read all of the terms 
                    and conditions in, and linked to, this Agreement, and you agree to be bound by this Agreement. 
                    If you do not agree, you have no right to use this Website.
                </p>
                <p>
                    Crypteon.net ("Crypteon") provides this site as a service to beta testers using Crypteon beta
                    services. Crypteon is non-commercial and is neither invested nor supported by other companies,
                    businesses or corporations.
                </p>
                <p>
                    This Agreement may be modified by Crypteon in its sole discretion from time to time and such 
                    modifications shall automatically become part of this Agreement and shall be effective once posted 
                    by Crypteon on the Website (as indicated by the posted update date). 
                    Your use of the Website will be subject to any such modifications.
                </p>                
                <p class="bold-font">2.) Privacy Policy</p>
                <p>
                    Crypteon requires a member to provide a valid email address. You may connect to Crypteon with an
                    external application, in which case Crypteon may receive some information from that other service.
                    We use browser cookies, session, cache, and log files to help understand how users interact with
                    our site and services, in order to provide a better experience. We use this information to provide
                    and improve our services, and to communicate with you. Crypteon will not sell or disclose your
                    personal information to third parties without your explicit consent. Information stored on Crypteon
                    cannot be used, rented, distributed, or sold for commercial purposes.
                </p>
                <p class="bold-font">3.) Website Access</p>
                <p>
                    Crypteon reserves the right at all times (but will not have an obligation) to remove or refuse to
                    post or distribute any Third Party Content, and to restrict, suspend or terminate the participation
                    of any User from the Website at any time, with or without prior notice.
                </p>
                <p class="bold-font">4.) Website Modification</p>
                <p>
                    Crypteon reserves the right at all time (but will not have an obligation) to update, modify,
                    discontinue any content on the website at any time, with or without prior notice.
                </p>
                <p class="bold-font">5.) Disclaimer</p>
                <p>
                    YOUR ACCESS TO AND USE OF CRYPTEON IS ENTIRELY AT YOUR OWN RISK. EVERYTHING ON CRYPTEON IS PROVIDED
                    ON AN "AS IS" OR "AS AVAILABLE" BASIS WITHOUT ANY WARRANTIES OF ANY KIND. NEITHER CRYPTEON.NET NOR
                    ITS AFFILIATES OR EMPLOYEES IS RESPONSIBLE FOR THE CONDUCT, OF ANY USER OF THE WEBSITE.
                </p>
                <p class="bold-font">6.) Limitation of Liabilities </p>
                <p>
                    You understand that any and all decisions made by you with respect to the Website (crypteon.net)
                    are yours alone. Crypteon cannot and does not verify the accuracy of information from other Users.
                    Crypteon shall not be responsible, or have any duty or obligation to, or liability for: (a) decisions
                    or interactions resulting (directly or indirectly) from your use of the Website; or (b) any damages,
                    costs, losses or expenses a User incurs as a result (directly or indirectly) of your use of the Website.
                    In addition, in no event will Crypteon be liable to you or any third person for any damages, costs,
                    losses or expenses, including any lost capital, lost profits or special, incidental, consequential or
                    punitive damages arising from your use of the Website, even if Crypteon has been advised of the
                    possibility of such damages, costs, losses or expenses.
                </p>
                <p class="bold-font">7.) Indemnity</p>
                <p>
                    You agree to indemnify and hold Crypteon harmless from any loss, liability, claim, or demand, including
                    reasonable attorney's fees, made or incurred by any third party due to or arising (directly or indirectly)
                    out of your use of the Website or arising from your breach of this Agreement.
                </p>
                <p class="bold-font">8.) No Gurantee</p>
                <p>
                    Although Crypteon will make the best efforts to provide the best continuous and uninterrupted service,
                    this is not guaranteed. Crypteon does not guarantee continuous, uninterrupted access to the Site,
                    and operation of the Site may be interfered with by numerous factors outside Crypteon's control.
                </p>
            </div>
        </div>
    </div>
</div>