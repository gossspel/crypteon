<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersistentOrderTransientOrderTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('persistent_order_transient_order', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('persistent_order_id');
            $table->integer('transient_order_id');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('persistent_order_transient_order');
	}

}
