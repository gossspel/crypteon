<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWithdrawalsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('withdrawals', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('coin', 20); // which coin daemon
            $table->string('from_address', 100); // should always be default daemon send address
            $table->string('to_address', 100); // the address that account user specified
            $table->decimal('withdrawal_amount', 20, 8);
            $table->string('status', 20); // pending/success
            $table->integer('user_id');
            $table->integer('daemon_server_id');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('withdrawals');
	}

}
