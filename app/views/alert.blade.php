@if (Session::has('message'))
    <div class="alert {{ Session::get('message-level') }} alert-dismissable" id="alert-container">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        {{ Session::get('message') }}
    </div>
@endif