<div class="row">
    <div class="col-md-12 basic-mainframe">

        <!-- Include Alert Message here, better than showing it in the layout to more easily control css -->
        @include('alert')

        <!-- Page Title -->
        <div class="div-table-title">
            Deposit History
        </div>
        <div class="panel panel-default div-table div-table-no-max-height">
            <table class="table table-striped table-hover table-bordered">
                @if (!empty($deposits))
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Coin</th>
                            <th class="visible-md visible-lg">Status</th>
                            <th class="hidden-xs">Amount</th>
                            <th>Detail</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($deposits as $deposit)
                            <tr>
                                <td>{{ $deposit->created_at }}</td>
                                <td>{{ $deposit->coin }}</td>
                                <td class="visible-md visible-lg">{{ $deposit->status }}</td>
                                <td class="hidden-xs">{{ $deposit->deposit_amount }}</td>
                                <td>
                                    <!-- Button trigger modal -->
                                    <button class="btn btn-info" data-toggle="modal" data-target="#myModal-deposit-{{ $deposit->id }}">
                                        Show
                                    </button>

                                    <!-- Modal -->
                                    <div class="modal detail-modal fade" id="myModal-deposit-{{ $deposit->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel-deposit-{{ $deposit->id }}" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h4 class="modal-title" id="myModalLabel-deposit-{{ $deposit->id }}">Deposit Detail</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="detail-div-row">
                                                        <div>Date:</div>
                                                        <div class="up">{{ $deposit->created_at }}</div>
                                                    </div>
                                                    <div class="detail-div-row">
                                                        <div>Coin:</div>
                                                        <div class="up">{{ $deposit->coin }}</div>
                                                    </div>
                                                    <div class="detail-div-row">
                                                        <div>Status:</div>
                                                        <div class="up">{{ $deposit->status }}</div>
                                                    </div>
                                                    <div class="detail-div-row">
                                                        <div>Amount:</div>
                                                        <div class="up">{{ $deposit->deposit_amount . ' ' . $deposit->coin }}</div>
                                                    </div>
                                                    <div class="detail-div-row">
                                                        <div>Txid:</div>
                                                        <div class="up too-long-break">{{ $deposit->txid }}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                @else
                    <tr>
                        <td>You don't have any deposits.</td>
                    </tr>
                @endif
            </table>
        </div>
    </div>
</div>