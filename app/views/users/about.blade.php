<div class="row">
    <div class="col-lg-12 basic-mainframe">

        <!-- Include Alert Message here, better than showing it in the layout to more easily control css -->
        @include('alert')

        <!-- Page Title -->
        <div class="div-table-title">
            About Us
        </div>
        <div class="panel panel-default">
            <div class="panel-heading div-about-title">
                What is Crypteon?
            </div>
            <div class="panel-body">
                <p>
                    Crypteon is a crypto to crypto exchange that is built for secure and smooth experience on mobile devices.
                    (It will be equally secure and smooth on desktops and laptops, we promise).
                </p>
                <div>Current functionality:</div>
                <p>
                    <ul>
                        <li>
                            Registration
                        </li>
                        <li>
                            Trading Process
                        </li>
                        <li>
                            Withdrawal Process
                        </li>
                        <li>
                            Deposit Process
                        </li>
                        <li>
                            Responsive UI
                        </li>
                        <li>
                            Sidebar Statistics
                        </li>
                        <li>
                            Two Factor Authentication
                        </li>
                        <li>
                            Ticket Supporting System
                        </li>
                    </ul>
                </p>
                <div>Upcoming functionality:</div>
                <p>
                    <ul>
                        <li>
                            More supported Coins and Markets
                        </li>
                        <li>
                            Trading and Withdrawal API
                        </li>
                        <li>
                            Market Data Graph
                        </li>
                        <li>
                            Embedded Twiiter Timelines
                        </li>
                        <li>
                            Realtime Chatbox
                        </li>
                    </ul>
                </p>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading div-about-title">
                Who we are?
            </div>
            <div class="panel-body">
                A group of software engineers in SF bay area who care about the crypto currency movement,
                mine and trade crypto currencies, like many of you. Crypteon is a startup project that we have been
                planning and building since early March.
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading div-about-title">
                Problems we want to solve:
            </div>
            <div class="panel-body">
                <span class="bold-font">1.) The pain to trade crypto currencies in mobile devices</span>
                <p>
                    The crypto currency exchange market is very volatile, sometimes being able to create/cancel an order
                    just in time would mean the difference of gaining/losing a few hundred dollars.
                    Nowadays, everyone carries a smart phone or an ipad, these mobile devices are excellent means to
                    trade instantaneously on crypto exchanges. However, most if not all crypto exchanges neglect to do an
                    excellent job on supporting mobile devices. One big reason why we decide to build Crypteon is that it
                    has always been painful for us to trade crypteo currencies in our ios and android based mobile devices.
                    We aim to make this a smooth and enjoyable experience by making a fluid and responsive UI in Crypteon.
                </p>
                <span class="bold-font">2.) Crypto exchange security</span>
                <p>
                    A few of the big exchanges got hacked before. Most, if not all hacks are performed via concurrent
                    requests to bypass validation and balance checking. With this caution in mind, orders and withdrawals
                    in Crypteon are processed via atomic queues.
                </p>
                <div>In addition, Crypteon has the following features to enhance security:</div>
                <p>
                <ul>
                    <li>
                        Hardware DDOS protection
                    </li>
                    <li>
                        Key-Based SSH for remote server access
                    </li>
                    <li>
                        Full site HTTPS to protect against data eavesdropping and tampering
                    </li>
                    <li>
                        Remote daemon servers (The web server and daemon servers are physically separated!)
                    </li>
                    <li>
                        Hot and cold wallet
                    </li>
                    <li>
                        Customized firewall
                    </li>
                    <li>
                        2 Factor Authentication
                    </li>
                    <li>
                        Prevention against MySQL injection and XSS attack
                    </li>
                    <li>
                        Data validation, sanitization and encryption
                    </li>
                </ul>
                </p>
                <span class="bold-font">3.) Crypto exchange performance</span>
                <div>A few of the big exchanges are painfully slow at times. There are three main causes:</div>
                <p>
                    <ul>
                        <li>
                            Bitcoin/other altcoin daemons do not scale up to thousands of accounts with tens of thousands of transactions.
                        </li>
                        <li>
                            Database bottleneck, especially i/o
                        </li>
                        <li>
                            Process-based http server under heavier loads consume a lot of RAM which significantly degrades performance.
                        </li>
                    </ul>
                </p>
                <div>To address these problems:</div>
                <p>
                    <ul>
                        <li>
                            Crypteon is built using a scalable coin daemon architecture,
                            with more secondary nodes to be added to interact with the master node to scale and expand.
                        </li>
                        <li>
                            Master node has a RAID 10 SSD setup and utilize database replication on secondary nodes
                            to maximize IO performance and data integrity
                        </li>
                        <li>
                            Database queries are written to take advantages of multiple inserts and updates.
                        </li>
                        <li>
                            Instead of using processed-based http server, Crypteon uses an optimized asynchronous http
                            server and various caching mechanisms to support heavy traffic.
                        </li>
                    </ul>
                </p>
            </div>
        </div>
    </div>
</div>