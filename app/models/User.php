<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

    public static $rules = array(
        'firstname'=>'required|alpha_num|min:2',
        'lastname'=>'required|alpha|min:2',
        'email'=>'required|email|unique:users',
        'password'=>'required|alpha_num|between:6,12|confirmed',
        'password_confirmation'=>'required|alpha_num|between:6,12'
    );

    public static $reset_rules = array(
        'current_password'=>'required|passcheck',
        'new_password'=>'required|alpha_num|between:6,12|confirmed',
        'new_password_confirmation'=>'required|alpha_num|between:6,12'
    );

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

    // There exists a 1 to 1 relationship between User{Entity} and Profile{Entity}
    public function profile()
    {
        return $this->hasOne('Profile', 'id');
    }

    // There exists a 1 to N relationship between User{Entity} and Deposit{Entity}.
    public function deposits()
    {
        return $this->hasMany('Deposit');
    }

    // There exists a 1 to N relationship between User{Entity} and Withdrawal{Entity}.
    public function withdrawals()
    {
        return $this->hasMany('Withdrawal');
    }

    // There exists a 1 to N relationship between User{Entity} and InternalTransaction{Entity}.
    public function internalTransactions()
    {
        return $this->hasMany('InternalTransaction');
    }

    // There exists a 1 to N relationship between User{Entity} and UserBalanceTransaction{Entity}.
    public function userBalanceTransactions()
    {
        return $this->hasMany('UserBalanceTransaction');
    }

    // There exists a 1 to N relationship between User{Entity} and Order{Entity}.
    public function orders()
    {
        return $this->hasMany('Order');
    }

    // There exists a 1 to N relationship between User{Entity} and PersistentOrder{Entity}.
    public function persistentOrders()
    {
        return $this->hasMany('PersistentOrder');
    }

    // There exists a 1 to N relationship between User{Entity} and TransientOrder{Entity}.
    public function transientOrders()
    {
        return $this->hasMany('TransientOrder');
    }

    // There exists a N to 1 relationship between User{Entity} and DaemonServer{Entity}.
    public function daemonServer()
    {
        return $this->belongsTo('DaemonServer');
    }

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

    public function getAllInfo()
    {
        //
    }

    public function getRememberToken()
    {
        return $this->remember_token;
    }

    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }

    public function getRememberTokenName()
    {
        return 'remember_token';
    }
}