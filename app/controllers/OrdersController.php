<?php

class OrdersController extends BaseController {

    protected $layout = "layouts.test";

    public function __construct() {
        $this->beforeFilter('csrf', array('on'=>'post'));
        $this->beforeFilter('auth');
    }

    public function postCancel() {
        // Check if server is in Maintenance mode
        $redis = Redis::connection();

        if ($redis->get('server_mode') == 'maintenance') {
            return View::make('maintenance');
        }

        // Check if User is logged in, if not, redirect to the Market Secure View of the coin.
        if (Auth::check() == false) {
            return Redirect::action('UsersController@getOrderHistory')
                ->with('message', 'You have been logged out, please login again to submit your order.')
                ->with('message-level', 'alert-info');
        }

        // Check if User is activated
        $user = Auth::user();

        if ($user->status != 'active') {
            $link = HTML::secureLink('users/activation-email', 'Resend activation email.');
            return Redirect::action('UsersController@getOrderHistory')
                ->with('message', 'Please activate your account to have full access on Crypteon. ' . $link)
                ->with('message-level', 'alert-danger');
        }

        // Allow only one order to be canceled per 5 seconds
        $auth_user_id = $user->id;
        $cou_key = 'cancel_order_user_' . $auth_user_id;
        $cou_key_count = $redis->incr($cou_key);
        $redis->expire($cou_key, 5);

        if ($cou_key_count > 1) {
            return Redirect::action('UsersController@getOrderHistory')
                ->with('message', 'For the sake of security and data integrity, cancellation request is limited to 1 per 5 seconds. Please try again later.')
                ->with('message-level', 'alert-danger');
        }

        $raw_order_id = Input::get('id');
        $order_id = intval($raw_order_id);
        $pending_cancel_order_select = array('id', 'buy_coin', 'sell_coin', 'market_id', 'user_id', 'status', 'rate', 'fee_coin', 'fee_rate', 'current_sell_amount');
        $order = DB::table('orders')->select($pending_cancel_order_select)->where('id', $order_id)->first();
        $scoin_available_balance = $order->sell_coin . '_available_balance';
        $scoin_order_balance = $order->sell_coin . '_order_balance';
        $market_id = $order->market_id;

        // Check if the Order belongs to the User
        if ($user->id != $order->user_id) {
            return Redirect::action('MarketsController@getSecureView', array($order->market_id))
                ->with('message', 'The hidden field value is invalid, please try again without altering the hidden field.')
                ->with('message-level', 'alert-danger');
        }

        // Check if the Order has already been canceled
        if ($order->status == 'canceled') {
            return Redirect::action('MarketsController@getSecureView', array($order->market_id))
                ->with('message', 'The order has already been canceled.')
                ->with('message-level', 'alert-success');
        } elseif ($order->status == 'completed') {
            return Redirect::action('MarketsController@getSecureView', array($order->market_id))
                ->with('message', "The order has already been completed and can't be canceled.")
                ->with('message-level', 'alert-danger');
        }

        // Queue for concurrent processing
        $lock_key = 'sell_'.$order->buy_coin.'_buy_'.$order->sell_coin.'_'.$order->rate;
        $queue_key = 'sell_'.$order->buy_coin.'_buy_'.$order->sell_coin.'_queue_'.$order->rate;

        // Retrieve Queue
        $queue = $redis->lrange($queue_key, 0, -1); // The queue is consist of order_create and order_cancel requests.

        // Make sure the values in the Queue are unique.
        $order_cancel_value = 'x'.$auth_user_id;

        if (!empty($queue)) {
            if (in_array($order_cancel_value, $queue)) {
                return Redirect::action('MarketsController@getSecureView', array($market_id))
                    ->with('message', 'The trade engine is currently too busy with simultaneous orders, please try again later.')
                    ->with('message-level', 'alert-warning');
            }
        }

        // Update Queue
        $redis->rpush($queue_key, $order_cancel_value);

        // Get Information for the next order in queue
        $next_in_queue_array = $redis->lrange($queue_key, 0 , 0);
        $next_in_queue = $next_in_queue_array[0];

        if ($next_in_queue[0] == 'x') { // Key value format - order_create: {$user_id}, order_cancel: {'x'.$user_id}
            $niquid = substr($next_in_queue, 1); //next_in_queue_user_id
            $niqt = 'cancel'; //next_in_queue_type
        } else {
            $niquid = $next_in_queue; //next_in_queue_user_id
            $niqt = 'create'; //next_in_queue_type
        }

        // Keep waiting we are next
        while ($redis->exists($lock_key) || ($niquid != $auth_user_id) || ($niqt != 'cancel')) {
            usleep(5000);

            // Update the information for the next order in queue
            $next_in_queue_array = $redis->lrange($queue_key, 0 , 0);
            $next_in_queue = $next_in_queue_array[0];

            if ($next_in_queue[0] == 'x') { // Key value format - order_create: {$user_id}, order_cancel: {'x'.$user_id}
                $niquid = substr($next_in_queue, 1); //next_in_queue_user_id
                $niqt = 'cancel'; //next_in_queue_type
            } else {
                $niquid = $next_in_queue; //next_in_queue_user_id
                $niqt = 'create'; //next_in_queue_type
            }

        }

        // Lock the atomic order key
        $redis->setex($lock_key, 30, $auth_user_id);

        // Get latest order info
        $pending_cancel_order_select = array('id', 'status');
        $latest_order = DB::table('orders')->select($pending_cancel_order_select)->where('id', $order_id)->first();

        // Check again to see if the Order has already been canceled
        if ($latest_order->status == 'canceled') {
            // Update the Queue
            $redis->lpop($queue_key);

            // Unlock the atomic order key
            $redis->del($lock_key);

            return Redirect::action('MarketsController@getSecureView', array($market_id))
                ->with('message', 'The order has already been canceled')
                ->with('message-level', 'alert-info');
        } elseif ($latest_order->status == 'completed') {
            // Update the Queue
            $redis->lpop($queue_key);

            // Unlock the atomic order key
            $redis->del($lock_key);

            return Redirect::action('MarketsController@getSecureView', array($market_id))
                ->with('message', "Sorry, the order can't be canceled because it has been completed by users in front of you in the order processing queue.")
                ->with('message-level', 'alert-danger');
        }

        // Get latest persistent order info
        $lpo = DB::table('persistent_orders')->select('id')->where('order_id', $order_id)->first(); // Latest Persistent Order

        // Canceling the Order
        $sell_coin = $order->sell_coin;
        $fee_coin = $order->fee_coin;
        $fee_rate = $order->fee_rate;

        // Return the order amount with fee for orders that are applicable
        if ($sell_coin == $fee_coin) {
            $fee = bcmul($order->current_sell_amount, $fee_rate, 8);
            $tx_amount = bcadd($order->current_sell_amount, $fee, 8);
        } else {
            $tx_amount = $order->current_sell_amount;
        }

        $mubt = array(); // Mass User Balance Transaction

        $ubtscab = array(); // User Balance Transaction Sell Coin Available Balance
        $ubtscab['instance_type'] = $scoin_available_balance;
        $ubtscab['transaction_amount'] = $tx_amount;
        $ubtscab['transaction_action'] = 'increase_balance';
        $ubtscab['type'] = 'order_canceled';
        $ubtscab['order_id'] = $order->id;
        $ubtscab['user_id'] = $auth_user_id;

        $ubtscob = array(); // User Balance Transaction Sell Coin Order Balance
        $ubtscob['instance_type'] = $scoin_order_balance;
        $ubtscob['transaction_amount'] = $tx_amount;
        $ubtscob['transaction_action'] = 'decrease_balance';
        $ubtscob['type'] = 'order_canceled';
        $ubtscob['order_id'] = $order->id;
        $ubtscob['user_id'] = $auth_user_id;

        $ubtscab['created_at'] = $ubtscab['updated_at'] = $ubtscob['created_at'] = $ubtscob['updated_at'] = date('Y-m-d H:i:s');

        $mubt[] = $ubtscab;
        $mubt[] = $ubtscob;

        // Last check to see if some other user hijacked the locked operation by chance or malice
        if ($redis->get($lock_key) != $auth_user_id) {
            // save warning log
            Log::warning('redis_lock_broken: simultaneous_order_cancel on key: ' . $lock_key);

            // Update the Queue
            $redis->lpop($queue_key);

            // Unlock the atomic order key
            $redis->del($lock_key);

            // redirect
            return Redirect::action('UsersController@getOrderHistory')
                ->with('message', 'The trade engine is currently too busy with simultaneous orders, please try again later.');
        }

        // Insert User Balance Transactions
        DB::table('user_balance_transactions')->insert($mubt);

        // Update User Profile
        DB::statement(DB::raw(
            "UPDATE profiles SET " .
            $scoin_available_balance . " = " . $scoin_available_balance . " + " . $tx_amount . ", " .
            $scoin_order_balance . " = " . $scoin_order_balance . " - " . $tx_amount .
            ", updated_at = now() WHERE id = " . $auth_user_id
        ));

        // Update Order
        DB::statement(DB::raw(
           "UPDATE orders SET " .
           "status = 'canceled', updated_at = now() " .
           "WHERE id = " . $order->id
        ));

        // Update Persistent Order
        DB::statement(DB::raw(
            "UPDATE persistent_orders SET " .
            "status = 'canceled', available_buy_amount = 0, available_sell_amount = 0, updated_at = now() " .
            "WHERE id = " . $lpo->id
        ));

        // Update the Queue
        $redis->lpop($queue_key);

        // Unlock the atomic order key
        $redis->del($lock_key);

        // Redirect to Order history
        return Redirect::action('UsersController@getOrderHistory', array('canceled', 'desc'))
            ->with('message', 'The order has been canceled')
            ->with('message-level', 'alert-success');
    }

    public function postCreate() {
        // DONETODO: Figure out why completed order is not showing in user_id: 31
        // DONETODO: Implement cache queue system for the locked cache key
        // DONETODO: Remove the cancellation part since the cache queue system will take care of all queued order adding and canceling

        // Check if server is in Maintenance mode
        $redis = Redis::connection();

        if ($redis->get('server_mode') == 'maintenance') {
            return View::make('maintenance');
        }

        // Check if User is logged in, if not, redirect to the Market Secure View of the coin.
        if (Auth::check() == false) {
            return Redirect::action('MarketsController@getSecureView', array(1))
                ->with('message', 'You have been logged out, please login again to submit your order.')
                ->with('message-level', 'alert-info');
        }

        // Check if User is activated
        $user = Auth::user();

        if ($user->status != 'active') {
            $link = HTML::secureLink('users/activation-email', 'Resend activation email.');
            return Redirect::action('UsersController@getOrderHistory')
                ->with('message', 'Please activate your account to have full access on Crypteon. ' . $link)
                ->with('message-level', 'alert-danger');
        }

        // Allow only one order to be created per 5 seconds
        $auth_user_id = $user->id;
        $cou_key = 'lock_order_or_withdrawal_user' . $auth_user_id;
        $cou_key_count = $redis->incr($cou_key);
        $redis->expire($cou_key, 10);

        if ($cou_key_count > 1) {
            return Redirect::action('UsersController@getOrderHistory')
                ->with('message', 'For the sake of security and data integrity, order/withdrawal request is limited to 1 per 10 seconds. Please try again later.')
                ->with('message-level', 'alert-danger');
        }

        $mode = Input::get('mode');
        $raw_market_id = Input::get('market_id');
        $market_id = intval($raw_market_id);
        $coin_market = DB::table('markets')->select('coin', 'base', 'fee_rate', 'fee_mode')->where('id', $market_id)->first();

        // Check to see if market exist
        if (empty($coin_market)) {
            return Redirect::action('MarketsController@getSecureView', array(1))
                ->with('message', 'The hidden field value is invalid, please try again without altering the hidden field')
                ->with('message-level', 'alert-danger');
        }

        // Check to see if the hiddenfield "mode" is altered
        if ($mode == 's') {
            $validator = Validator::make(Input::all(), Order::$sell_order_rules);
        } elseif ($mode == 'b') {
            $validator = Validator::make(Input::all(), Order::$buy_order_rules);
        } else {
            return Redirect::action('MarketsController@getSecureView', array($market_id))
                ->with('message', 'The hidden field value is invalid, please try again without altering the hidden field')
                ->with('message-level', 'alert-danger');
        }

        // DONETODO: Add mode checking in Order Validator (Only allow 0 and 1)
        // DONETODO: seperate sell and buy form input, ie: sell_rate, buy_rate
        // DONETODO: redefine $rate base on mode
        // DONETODO: Properly show error popover if validation fails

        if (!($validator->passes())) {
            return Redirect::action('MarketsController@getSecureView', array($market_id))
                ->with('message', 'Unsuccessful form submit, please check the errors below.')
                ->with('message-level', 'alert-danger')
                ->withErrors($validator)
                ->withInput(Input::except(array('market_id', 'mode')));
        }

        // Check if User has enough available funding to make the order
        if ($mode == 's') {
            $raw_rate = Input::get('sell_rate');
            $rate = bcadd($raw_rate, '0', 8); // For a secure Redis lock, key 'rate_0.004' != key 'rate_0.00400000'
            $scoin = $coin_market->coin;
            $bcoin = $coin_market->base;

            $scoin_available_balance = $scoin.'_available_balance';
            $scoin_order_balance = $scoin.'_order_balance';
            $bcoin_available_balance = $bcoin.'_available_balance';
            $bcoin_order_balance = $bcoin.'_order_balance';
            $profile = Profile::find($auth_user_id, array($scoin_available_balance, $scoin_order_balance, $bcoin_available_balance, 'fee_rate'));

            if ($coin_market->fee_mode == 'normal') {
                $fee_rate = $profile->fee_rate;
            } else {
                $fee_rate = $coin_market->fee_rate;
            }

            $original_sell_amount = Input::get('original_sell_amount');
            $original_buy_amount = bcmul($original_sell_amount, $rate, 8);
            $fee = bcmul($original_buy_amount, $fee_rate, 8);

            // Check if minimum order amount is match
            if ($original_buy_amount < 0.00010000) {
                return Redirect::action('MarketsController@getSecureView', array($market_id))
                    ->with('message', 'Unsuccessful form submit, please check the errors below.')
                    ->with('message-level', 'alert-danger')
                    ->with('sell_fee', 1)
                    ->withInput(Input::except(array('market_id', 'mode')));
            }

            $long_mode = 'Sell';
        } else {
            $raw_rate = Input::get('buy_rate');
            $rate = bcadd($raw_rate, '0', 8); // For a secure Redis lock, key 'rate_0.004' != key 'rate_0.00400000'
            $scoin = $coin_market->base;
            $bcoin = $coin_market->coin;

            $scoin_available_balance = $scoin.'_available_balance';
            $scoin_order_balance = $scoin.'_order_balance';
            $bcoin_available_balance = $bcoin.'_available_balance';
            $bcoin_order_balance = $bcoin.'_order_balance';
            $profile = Profile::find($auth_user_id, array($scoin_available_balance, $scoin_order_balance, $bcoin_available_balance, 'fee_rate'));

            if ($coin_market->fee_mode == 'normal') {
                $fee_rate = $profile->fee_rate;
            } else {
                $fee_rate = $coin_market->fee_rate;
            }

            $original_buy_amount = Input::get('original_buy_amount');
            $original_sell_amount = bcmul($original_buy_amount, $rate, 8);
            $fee = bcmul($original_sell_amount, $fee_rate, 8);

            // Check if minimum order amount is match
            if ($original_sell_amount < 0.00010000) {
                return Redirect::action('MarketsController@getSecureView', array($market_id))
                    ->with('message', 'Unsuccessful form submit, please check the errors below.')
                    ->with('message-level', 'alert-danger')
                    ->with('buy_fee', 1)
                    ->withInput(Input::except(array('market_id', 'mode')));
            }

            $long_mode = 'Buy';
        }

        $fee_coin = $coin_market->base;
        $order_amount = $original_sell_amount;
        $user_scoin_available_balance = $profile->{$scoin_available_balance};

        if ($mode == 's') {
            if ($order_amount > $user_scoin_available_balance) {
                return Redirect::action('MarketsController@getSecureView', array($market_id))
                    ->with('message', 'Insufficient balance, please enter a new amount and submit again.')
                    ->with('message-level', 'alert-danger');
            }
        } else {
            $order_amount_with_fee = bcadd($order_amount, $fee, 8);
            if ($order_amount_with_fee > $user_scoin_available_balance) {
                return Redirect::action('MarketsController@getSecureView', array($market_id))
                    ->with('message', 'Insufficient balance, please enter a new amount and submit again.')
                    ->with('message-level', 'alert-danger');
            }
        }

        // Queue for concurrent processing
        $lock_key = 'sell_'.$scoin.'_buy_'.$bcoin.'_'.$rate;
        $queue_key = 'sell_'.$scoin.'_buy_'.$bcoin.'_queue_'.$rate; // raw queue {array}

        // Retrieve Queue
        $queue = $redis->lrange($queue_key, 0, -1); // The queue is consist of order_create and order_cancel requests.

        // Make sure the values in the Queue are unique.
        if (!empty($queue)) {
            if (in_array($auth_user_id, $queue)) {
                return Redirect::action('MarketsController@getSecureView', array($market_id))
                    ->with('message', 'The trade engine is currently too busy with simultaneous orders, please try again later.')
                    ->with('message-level', 'alert-warning');
            }
        }

        // Update Queue
        $redis->rpush($queue_key, $auth_user_id);

        // Get Information for the next order in queue
        $next_in_queue_array = $redis->lrange($queue_key, 0 , 0);
        $next_in_queue = $next_in_queue_array[0];

        if ($next_in_queue[0] == 'x') { // Key value format - order_create: {$user_id}, order_cancel: {'x'.$user_id}
            $niquid = substr($next_in_queue, 1); //next_in_queue_user_id
            $niqt = 'cancel'; //next_in_queue_type
        } else {
            $niquid = $next_in_queue; //next_in_queue_user_id
            $niqt = 'create'; //next_in_queue_type
        }

        while ($redis->exists($lock_key) || ($niquid != $auth_user_id) || ($niqt != 'create')) {
            usleep(5000);

            // Update the information for the next order in queue
            $next_in_queue_array = $redis->lrange($queue_key, 0 , 0);
            $next_in_queue = $next_in_queue_array[0];

            if ($next_in_queue[0] == 'x') { // Key value format - order_create: {$user_id}, order_cancel: {'x'.$user_id}
                $niquid = substr($next_in_queue, 1); //next_in_queue_user_id
                $niqt = 'cancel'; //next_in_queue_type
            } else {
                $niquid = $next_in_queue; //next_in_queue_user_id
                $niqt = 'create'; //next_in_queue_type
            }

        }

        // Lock the atomic order key
        $redis->setex($lock_key, 30, $auth_user_id);

        // Check to see if there is any open corresponding buy order.
        $corresponding_buy_orders_select = array('persistent_orders.id',
            'persistent_orders.order_id',
            'profiles.'.$bcoin_available_balance,
            'profiles.'.$bcoin_order_balance,
            'profiles.'.$scoin_available_balance,
            'profiles.email',
            'profiles.daemon_server_id',
            'orders.user_id',
            'orders.created_at',
            'orders.completed_percentage',
            'orders.collected_fee',
            'orders.pending_fee',
            'orders.fee_rate',
            'orders.current_buy_amount',
            'orders.current_sell_amount',
            'orders.bought_amount',
            'orders.sold_amount',
            'orders.status',
            'orders.type');

        $corresponding_buy_orders = DB::table('profiles')
            ->join('orders', 'profiles.id', '=', 'orders.user_id')
            ->join('persistent_orders', 'orders.id', '=', 'persistent_orders.order_id')
            ->where('persistent_orders.rate', '=', $rate)
            ->where('persistent_orders.sell_coin', '=', $bcoin)
            ->where('persistent_orders.buy_coin', '=', $scoin)
            ->where('persistent_orders.status', '=', 'open')
            ->orderBy('orders.created_at', 'asc')
            ->select($corresponding_buy_orders_select)
            ->get(); // The return result is an array of php standard class (object).

        if (empty($corresponding_buy_orders)) {

            // DONETODO: Save as a Type_2 Open order
            // DONETODO: Redirect and unlock cache key

            // Need to save the order record first, so that the fk is available for:
            // transient_order, internal_transaction, user_balance_transaction, and pivot table
            $new_order = new Order;
            $new_order->status = 'open';
            $new_order->sell_coin = $scoin;
            $new_order->buy_coin = $bcoin;
            $new_order->fee_coin = $fee_coin;
            $new_order->fee_rate = $fee_rate;
            $new_order->original_fee = $fee;
            $new_order->pending_fee = $fee;
            $new_order->collected_fee = 0;
            $new_order->original_sell_amount = $original_sell_amount;
            $new_order->original_buy_amount = $original_buy_amount;
            $new_order->sold_amount = 0;
            $new_order->bought_amount = 0;
            $new_order->current_sell_amount = $original_sell_amount;
            $new_order->current_buy_amount = $original_buy_amount;
            $new_order->rate = $rate;
            $new_order->type = 'type_2';
            $new_order->mode = $long_mode;
            $new_order->completed_percentage = 0;
            $new_order->market_id = $market_id;
            $new_order->user_id = $user->id;
            $new_order->save();
            $new_order_id = $new_order->id; // order_id for fk from the newly created order.

            // In-Progress Order Related Persistent Order
            $new_persistent_order = new PersistentOrder;
            $new_persistent_order->sell_coin = $scoin;
            $new_persistent_order->buy_coin = $bcoin;
            $new_persistent_order->rate = $rate;
            $new_persistent_order->sell_amount = $original_sell_amount;
            $new_persistent_order->buy_amount = $original_buy_amount;
            $new_persistent_order->available_sell_amount = $original_sell_amount;
            $new_persistent_order->available_buy_amount = $original_buy_amount;
            $new_persistent_order->status = 'open';
            $new_persistent_order->mode = $long_mode;
            $new_persistent_order->order_id = $new_order_id;
            $new_persistent_order->user_id = $user->id;
            $new_persistent_order->save();

            // DONETODO: Add User Balance Transaction and Update Profile Balance.
            $mocubt = array(); // mass_order_create_user_balance_transaction

            // Fee calculation for the current newly created Type_2 order
            if ($mode == 's') { // This Type_2 order buys bases {BTC, LTC, DOGE}, so the fee won't be presented in the initial UserBalanceTransaction.
                $tx_amount = $original_sell_amount;
            } else { // This Type_2 order sells bases {BTC, LTC, DOGE}, so the fee will be presented in the initial UserBalanceTransaction.
                $tx_amount = bcadd($original_sell_amount, $fee, 8);
            }

            $date = date('Y-m-d H:i:s');

            $ipocuabt = array(); // in_progress_order_create_user_available_balance_transaction
            $ipocuabt['instance_type'] = $scoin_available_balance;
            $ipocuabt['transaction_action'] = 'decrease_balance';
            $ipocuabt['transaction_amount'] = $tx_amount;
            $ipocuabt['type'] = 'order_created';
            $ipocuabt['order_id'] = $new_order_id;
            $ipocuabt['user_id'] = $user->id;
            $ipocuabt['created_at'] = $date;
            $ipocuabt['updated_at'] = $date;

            $mocubt[] = $ipocuabt; // Insert into the mass user balance transaction array

            $ipocuobt = array(); // in_progress_order_create_user_order_balance_transaction
            $ipocuobt['instance_type'] = $scoin_order_balance;
            $ipocuobt['transaction_action'] = 'increase_balance';
            $ipocuobt['transaction_amount'] = $tx_amount;
            $ipocuobt['type'] = 'order_created';
            $ipocuobt['order_id'] = $new_order_id;
            $ipocuobt['user_id'] = $user->id;
            $ipocuobt['created_at'] = $date;
            $ipocuobt['updated_at'] = $date;

            $mocubt[] = $ipocuobt; // Insert into the mass user balance transaction array

            // Insert User Balance Transactions
            DB::table('user_balance_transactions')->insert($mocubt);

            // Update User Profile
            DB::statement(DB::raw(
                "UPDATE profiles SET " .
                $scoin_available_balance . " = " . $scoin_available_balance . " - " . $tx_amount . ", " .
                $scoin_order_balance . " = " . $scoin_order_balance . " + " . $tx_amount .
                ", updated_at = now() WHERE id = " . $user->id
            ));

            // Update the Queue
            $redis->lpop($queue_key);

            // Unlock the atomic order key
            $redis->del($lock_key);

            // Redirect
            return Redirect::action('UsersController@getOrderHistory', array('open'))
                ->with('message', 'Order has been submitted!')
                ->with('message-level', 'alert-success');

        } else {

            // Compare the sell amount and buy amount between the persistent orders and the in-progress order.
            // We will determine if this in-progress order is a type_1 or type_3 order.

            $type = 'type_3'; // setting the in-progress order default type
            $mofp = array(); // mass_order_fulfilled_pivot
            $mofubt = array(); // mass_order_fulfilled_user_balance_transactions
            $mofit = array(); // mass_order_fulfilled_internal_transactions
            $mofpu = array(); // mass_order_fulfilled_profiles_update
            $mofou = array(); // mass_order_fulfilled_orders_update
            $mofpou = array(); // mass_order_fulfilled_persistent_order_update
            $mofpuc = array(); // mass_order_fulfilled_profile_updates_check

            // Processing Each Corresponding Buy Order
            foreach ($corresponding_buy_orders as $cbo) {
                $date = date('Y-m-d H:i:s');
                if ($cbo->current_buy_amount < $order_amount) {

                    // The corresponding buy order is completed
                    // The in-progress order is partially fulfilled
                    // Assign all pivot table relationships
                    // Update all Mass Insert and Mass Update array
                    // Only users from those persistent orders

                    // Add Persistent Order id into pivot table
                    $cbopop = array(); // corresponding_buy_order_persistent_order_pivot
                    $cbopop['persistent_order_id'] = $cbo->id;
                    $cbopop['created_at'] = $date;
                    $cbopop['updated_at'] = $date;

                    $mofp[] = $cbopop; // Insert into mass pivot array

                    // Fee Calculation for the current corresponding buy order
                    if ($mode == 's') { // This corresponding buy order sells bases {BTC, LTC, DOGE}, so we will collect the fee when we decrease the base balance
                        $fee_to_collect = bcmul($cbo->current_sell_amount, $cbo->fee_rate, 8);
                        $decrease_tx_amount = bcadd($cbo->current_sell_amount, $fee_to_collect, 8);
                        $increase_tx_amount = $cbo->current_buy_amount;
                    } else { // This corresponding buy order buys bases {BTC, LTC, DOGE}, so we will collect the fee when we increase the base balance
                        $fee_to_collect = bcmul($cbo->current_buy_amount, $cbo->fee_rate, 8);
                        $decrease_tx_amount = $cbo->current_sell_amount;
                        $increase_tx_amount = bcsub($cbo->current_buy_amount, $fee_to_collect, 8);
                    }

                    // Update Corresponding Buy Order
                    $cbou = array(); // corresponding_buy_order_update
                    $cbou['id'] = $cbo->order_id;
                    $cbou['bought_amount'] = bcadd($cbo->bought_amount, $cbo->current_buy_amount, 8);
                    $cbou['collected_fee'] = bcadd($cbo->collected_fee, $fee_to_collect, 8);
                    $cbou['pending_fee'] = bcsub($cbo->pending_fee, $fee_to_collect, 8);
                    $cbou['current_buy_amount'] = 0;
                    $cbou['sold_amount'] = bcadd($cbo->sold_amount, $cbo->current_sell_amount, 8);
                    $cbou['current_sell_amount'] = 0;
                    $cbou['status'] = 'completed';
                    $cbou['completed_percentage'] = 100;

                    $mofou[] = $cbou; // Insert into mass order update array

                    // Update Corresponding Buy Persistent Order
                    $cbpou = array(); // corresponding_buy_persistent_order_update
                    $cbpou['id'] = $cbo->id;
                    $cbpou['available_sell_amount'] = 0;
                    $cbpou['available_buy_amount'] = 0;
                    $cbpou['status'] = 'completed';

                    $mofpou[] = $cbpou; // Insert into mass peristent order update array

                    // Insert User Balance Transaction
                    $cbosuabt = array(); // corresponding_buy_order_sold_user_available_balance_transaction
                    $cbosuabt['instance_type'] = $bcoin_order_balance;
                    $cbosuabt['transaction_action'] = 'decrease_balance';
                    $cbosuabt['transaction_amount'] = $decrease_tx_amount;
                    $cbosuabt['type'] = 'order_sold';
                    $cbosuabt['order_id'] = $cbo->order_id;
                    $cbosuabt['user_id'] = $cbo->user_id;
                    $cbosuabt['created_at'] = $date;
                    $cbosuabt['updated_at'] = $date;

                    $mofubt[] = $cbosuabt; // Insert into mass user balance transaction array.

                    $cbobuabt = array(); // corresponding_buy_order_bought_user_available_balance_transaction
                    $cbobuabt['instance_type'] = $scoin_available_balance;
                    $cbobuabt['transaction_action'] = 'increase_balance';
                    $cbobuabt['transaction_amount'] = $increase_tx_amount;
                    $cbobuabt['type'] = 'order_bought';
                    $cbobuabt['order_id'] = $cbo->order_id;
                    $cbobuabt['user_id'] = $cbo->user_id;
                    $cbobuabt['created_at'] = $date;
                    $cbobuabt['updated_at'] = $date;

                    $mofubt[] = $cbobuabt; // Insert into mass user balance transaction array.

                    // Update User Profile
                    $cbopu= array(); // corresponding_buy_order_profile_update
                    $cbopu['id'] = $cbo->user_id;
                    $cbopu[$scoin_available_balance] = $scoin_available_balance . ' + ' . $cbobuabt['transaction_amount'];
                    $cbopu[$scoin_order_balance] = $scoin_order_balance; // Notify the mass update that we don't update this field
                    $cbopu[$bcoin_available_balance] = $bcoin_available_balance; // Notify the mass update that we don't update this field
                    $cbopu[$bcoin_order_balance] = $bcoin_order_balance . ' - ' . $cbosuabt['transaction_amount'];

                    if (in_array($cbo->user_id, $mofpuc)) {
                        // Can't use the += or -= operator because it doesn't work with null. ie: z = null, z += 7, z = null whereas z + 7, z = 7
                        $mofpu[$cbo->user_id][$scoin_available_balance] = $mofpu[$cbo->user_id][$scoin_available_balance] . ' + ' . $cbobuabt['transaction_amount'];
                        $mofpu[$cbo->user_id][$bcoin_order_balance] = $mofpu[$cbo->user_id][$bcoin_order_balance] . ' - ' . $cbosuabt['transaction_amount'];
                    } else {
                        $mofpuc[] = $cbo->user_id;
                        $mofpu[$cbo->user_id] = $cbopu;
                    }

                    // Insert Internal Transaction
                    $cbosit = array(); // corresponding_buy_order_sold_internal_transaction
                    $cbosit['amount'] = $cbo->current_sell_amount;
                    $cbosit['coin'] = $bcoin;
                    $cbosit['from_account'] = $cbo->email;
                    $cbosit['to_account'] = 'default_' . $bcoin;
                    $cbosit['status'] = 'pending';
                    $cbosit['type'] = 'order_sold';
                    $cbosit['order_id'] = $cbo->order_id;
                    $cbosit['user_id'] = $cbo->user_id;
                    $cbosit['daemon_server_id'] = $cbo->daemon_server_id;
                    $cbosit['created_at'] = $date;
                    $cbosit['updated_at'] = $date;

                    $mofit[] = $cbosit; // Insert into the mass internal transaction array

                    $cbobit = array(); // corresponding_buy_order_bought_internal_transaction
                    $cbobit['amount'] = $cbo->current_buy_amount;
                    $cbobit['coin'] = $scoin;
                    $cbobit['from_account'] = 'default_' . $scoin;
                    $cbobit['to_account'] = $cbo->email;
                    $cbobit['status'] = 'pending';
                    $cbobit['type'] = 'order_bought';
                    $cbobit['order_id'] = $cbo->order_id;
                    $cbobit['user_id'] = $cbo->user_id;
                    $cbobit['daemon_server_id'] = $cbo->daemon_server_id;
                    $cbobit['created_at'] = $date;
                    $cbobit['updated_at'] = $date;

                    $mofit[] = $cbobit; // Insert into the mass internal transaction array

                    if ($fee_to_collect != 0) {
                        $cbotfit = array(); // corresponding_buy_order_traded_fee_internal_transaction
                        $cbotfit['amount'] = $fee_to_collect;
                        $cbotfit['coin'] = $fee_coin;
                        $cbotfit['from_account'] = $cbo->email;
                        $cbotfit['to_account'] = 'default_' . $fee_coin . '_fee';
                        $cbotfit['status'] = 'pending';
                        $cbotfit['type'] = 'order_traded_fee';
                        $cbotfit['order_id'] = $cbo->order_id;
                        $cbotfit['user_id'] = $cbo->user_id;
                        $cbotfit['daemon_server_id'] = $cbo->daemon_server_id;
                        $cbotfit['created_at'] = $date;
                        $cbotfit['updated_at'] = $date;

                        $mofit[] = $cbotfit; // Insert into the mass internal transaction array
                    }

                    // DONETODO: Continue to apply the FEE STRUCTURE for corresponding buy order below and TYPE_1 order below

                    // update for order_amount in next iteration
                    $order_amount = bcsub($order_amount, $cbo->current_buy_amount, 8);

                } else {

                    $type = 'type_1'; // This is a transient only order!
                    // break the loop, since the in-progress order will be completed by the current corresponding buy order
                    // The corresponding buy order is partially fulfilled
                    // The in-progress order is completed
                    // Assign all pivot table relationships
                    // Update all Mass Insert and Mass Update array
                    // Only users from those persistent orders

                    // Add Persistent Order id into pivot table
                    $cbopop = array(); // corresponding_buy_order_persistent_order_pivot
                    $cbopop['persistent_order_id'] = $cbo->id;
                    $cbopop['created_at'] = $date;
                    $cbopop['updated_at'] = $date;

                    $mofp[] = $cbopop; // Insert into mass pivot array

                    // Fee Calculation for the current corresponding buy order
                    if ($mode == 's') { // This corresponding buy order sells bases {BTC, LTC, DOGE}, so we will collect the fee when we decrease the base balance
                        $converted_amount = bcmul($order_amount, $rate, 8);
                        $fee_to_collect = bcmul($converted_amount, $cbo->fee_rate, 8);
                        $decrease_tx_amount = bcadd($converted_amount, $fee_to_collect, 8);
                        $increase_tx_amount = $order_amount;
                    } else { // This corresponding buy order buys bases {BTC, LTC, DOGE}, so we will collect the fee when we increase the base balance
                        $converted_amount = bcdiv($order_amount, $rate, 8);
                        $fee_to_collect = bcmul($order_amount, $cbo->fee_rate, 8);
                        $decrease_tx_amount = $converted_amount;
                        $increase_tx_amount = bcsub($order_amount, $fee_to_collect, 8);
                    }

                    // Update Corresponding Buy Order
                    $cbou = array(); // corresponding_buy_order_update
                    $cbou['id'] = $cbo->order_id;
                    $cbou['collected_fee'] = bcadd($cbo->collected_fee, $fee_to_collect, 8);
                    $cbou['pending_fee'] = bcsub($cbo->pending_fee, $fee_to_collect, 8);
                    $cbou['bought_amount'] = bcadd($cbo->bought_amount, $order_amount, 8);
                    $cbou['current_buy_amount'] = bcsub($cbo->current_buy_amount, $order_amount, 8);
                    $cbou['sold_amount'] = bcadd($cbo->sold_amount, $converted_amount, 8);
                    $cbou['current_sell_amount'] = bcsub($cbo->current_sell_amount, $converted_amount, 8);
                    $completed_percentage_top = bcmul($cbou['sold_amount'], '100', 8);
                    $completed_percentage_bottom = bcadd($cbou['sold_amount'], $cbou['current_sell_amount'], 8);
                    $percentage = $completed_percentage_top / $completed_percentage_bottom;
                    $cbou['completed_percentage'] = round($percentage, 3); // BCMATH is bad at shrinking precision, ie: something like 66.667 will become 66.666

                    if ($cbou['completed_percentage'] == 100) { // The current transient order might finish off the current corresponding buying order
                        $cbou['status'] = 'completed';
                    } else {
                        $cbou['status'] = 'open';
                    }

                    $mofou[] = $cbou; // Insert into mass order update array

                    // Update Corresponding Buy Persistent Order
                    $cbpou = array(); // corresponding_buy_persistent_order_update
                    $cbpou['id'] = $cbo->id;
                    $cbpou['available_sell_amount'] = $cbou['current_sell_amount'];
                    $cbpou['available_buy_amount'] = $cbou['current_buy_amount'];
                    $cbpou['status'] = $cbou['status'];

                    $mofpou[] = $cbpou; // Insert into mass peristent order update array

                    // DONETODO: Continue to Insert User Balance Transaction, Update Profile and Insert Internal Transactions

                    // Insert User Balance Transaction
                    $cbosuabt = array(); // corresponding_buy_order_sold_user_available_balance_transaction
                    $cbosuabt['instance_type'] = $bcoin_order_balance;
                    $cbosuabt['transaction_action'] = 'decrease_balance';
                    $cbosuabt['transaction_amount'] = $decrease_tx_amount;
                    $cbosuabt['type'] = 'order_sold';
                    $cbosuabt['order_id'] = $cbo->order_id;
                    $cbosuabt['user_id'] = $cbo->user_id;
                    $cbosuabt['created_at'] = $date;
                    $cbosuabt['updated_at'] = $date;

                    $mofubt[] = $cbosuabt; // Insert into mass user balance transaction array.

                    $cbobuabt = array(); // corresponding_buy_order_bought_user_available_balance_transaction
                    $cbobuabt['instance_type'] = $scoin_available_balance;
                    $cbobuabt['transaction_action'] = 'increase_balance';
                    $cbobuabt['transaction_amount'] = $increase_tx_amount;
                    $cbobuabt['type'] = 'order_bought';
                    $cbobuabt['order_id'] = $cbo->order_id;
                    $cbobuabt['user_id'] = $cbo->user_id;
                    $cbobuabt['created_at'] = $date;
                    $cbobuabt['updated_at'] = $date;

                    $mofubt[] = $cbobuabt; // Insert into mass user balance transaction array.

                    // Update User Profile
                    $cbopu= array(); // corresponding_buy_order_profile_update
                    $cbopu['id'] = $cbo->user_id;
                    $cbopu[$scoin_available_balance] = $scoin_available_balance . ' + ' . $cbobuabt['transaction_amount'];
                    $cbopu[$scoin_order_balance] = $scoin_order_balance; // Notify the mass update that we don't update this field
                    $cbopu[$bcoin_available_balance] = $bcoin_available_balance; // Notify the mass update that we don't update this field
                    $cbopu[$bcoin_order_balance] = $bcoin_order_balance . ' - ' . $cbosuabt['transaction_amount'];

                    if (in_array($cbo->user_id, $mofpuc)) {
                        $mofpu[$cbo->user_id][$scoin_available_balance] = $mofpu[$cbo->user_id][$scoin_available_balance] . ' + ' . $cbobuabt['transaction_amount'];
                        $mofpu[$cbo->user_id][$bcoin_order_balance] = $mofpu[$cbo->user_id][$bcoin_order_balance] . ' - ' . $cbosuabt['transaction_amount'];
                    } else {
                        $mofpuc[] = $cbo->user_id;
                        $mofpu[$cbo->user_id] = $cbopu;
                    }

                    // Insert Internal Transaction
                    $cbosit = array(); // corresponding_buy_order_sold_internal_transaction
                    $cbosit['amount'] = $converted_amount;
                    $cbosit['coin'] = $bcoin;
                    $cbosit['from_account'] = $cbo->email;
                    $cbosit['to_account'] = 'default_' . $bcoin;
                    $cbosit['status'] = 'pending';
                    $cbosit['type'] = 'order_sold';
                    $cbosit['order_id'] = $cbo->order_id;
                    $cbosit['user_id'] = $cbo->user_id;
                    $cbosit['daemon_server_id'] = $cbo->daemon_server_id;
                    $cbosit['created_at'] = $date;
                    $cbosit['updated_at'] = $date;

                    $mofit[] = $cbosit; // Insert into the mass internal transaction array

                    $cbobit = array(); // corresponding_buy_order_bought_internal_transaction
                    $cbobit['amount'] = $order_amount;
                    $cbobit['coin'] = $scoin;
                    $cbobit['from_account'] = 'default_' . $scoin;
                    $cbobit['to_account'] = $cbo->email;
                    $cbobit['status'] = 'pending';
                    $cbobit['type'] = 'order_bought';
                    $cbobit['order_id'] = $cbo->order_id;
                    $cbobit['user_id'] = $cbo->user_id;
                    $cbobit['daemon_server_id'] = $cbo->daemon_server_id;
                    $cbobit['created_at'] = $date;
                    $cbobit['updated_at'] = $date;

                    $mofit[] = $cbobit; // Insert into the mass internal transaction array

                    if ($fee_to_collect != 0) {
                        $cbotfit = array(); // corresponding_buy_order_traded_fee_internal_transaction
                        $cbotfit['amount'] = $fee_to_collect;
                        $cbotfit['coin'] = $fee_coin;
                        $cbotfit['from_account'] = $cbo->email;
                        $cbotfit['to_account'] = 'default_' . $fee_coin . '_fee';
                        $cbotfit['status'] = 'pending';
                        $cbotfit['type'] = 'order_traded_fee';
                        $cbotfit['order_id'] = $cbo->order_id;
                        $cbotfit['user_id'] = $cbo->user_id;
                        $cbotfit['daemon_server_id'] = $cbo->daemon_server_id;
                        $cbotfit['created_at'] = $date;
                        $cbotfit['updated_at'] = $date;

                        $mofit[] = $cbotfit; // Insert into the mass internal transaction array
                    }

                    // break the loop because the in-progress transient order is completed.
                    break;
                }
            }

            // Last check to see if some other user hijacked the locked operation by chance or malice
            if ($redis->get($lock_key) != $auth_user_id) {
                // Update the Queue
                $redis->lpop($queue_key);

                // Unlock the atomic order key
                $redis->del($lock_key);

                // save warning log
                Log::warning('redis_lock_broken: simultaneous_order_insert on key: ' . $lock_key);

                // redirect
                return Redirect::action('MarketsController@getSecureView', array($market_id))
                    ->with('message', 'The trade engine is currently too busy with simultaneous orders, please try again later.')
                    ->with('message-level', 'alert-warning');
            }


            // Processing the In-Progress Order
            if ($type == 'type_1') {
                // Set Redirect param
                $redirect_param = 'completed';

                // Need to save the order and transient_order record first, so that the fk is available for:
                // transient_order, internal_transaction, user_balance_transaction, and pivot table
                $new_order = new Order;
                $new_order->status = 'completed';
                $new_order->sell_coin = $scoin;
                $new_order->buy_coin = $bcoin;
                $new_order->fee_coin = $fee_coin;
                $new_order->fee_rate = $fee_rate;
                $new_order->original_fee = $fee;
                $new_order->collected_fee = $fee;
                $new_order->pending_fee = 0;
                $new_order->original_sell_amount = $original_sell_amount;
                $new_order->original_buy_amount = $original_buy_amount;
                $new_order->sold_amount = $original_sell_amount;
                $new_order->bought_amount = $original_buy_amount;
                $new_order->current_sell_amount = 0;
                $new_order->current_buy_amount = 0;
                $new_order->rate = $rate;
                $new_order->type = 'type_1';
                $new_order->mode = $long_mode;
                $new_order->completed_percentage = 100;
                $new_order->market_id = $market_id;
                $new_order->user_id = $user->id;
                $new_order->save();
                $new_order_id = $new_order->id; // order_id for fk from the newly created order.

                // In-Progress Order Related Transient Order
                $new_transient_order = new TransientOrder;
                $new_transient_order->sell_coin = $scoin;
                $new_transient_order->buy_coin = $bcoin;
                $new_transient_order->rate = $rate;
                $new_transient_order->sold_amount = $original_sell_amount;
                $new_transient_order->bought_amount = $original_buy_amount;
                $new_transient_order->mode = $long_mode;
                $new_transient_order->order_id = $new_order_id;
                $new_transient_order->user_id = $user->id;
                $new_transient_order->market_id = $market_id;
                $new_transient_order->save();
                $new_transient_order_id = $new_transient_order->id; // transient_order_id for pivot table;

                // In_Progress Order Add transient_order_id to pivot table
                foreach ($mofp as &$sofp) {
                    $sofp['transient_order_id'] = $new_transient_order_id;
                }

                $date = date('Y-m-d H:i:s');

                // Fee calculation for the current newly created Type_1 order
                if ($mode == 's') { // This Type_1 order buys bases {BTC, LTC, DOGE}, so we will collect the fee when we increase the base balance
                    $fee_to_collect = bcmul($original_buy_amount, $fee_rate, 8);
                    $decrease_tx_amount = $original_sell_amount;
                    $increase_tx_amount = bcsub($original_buy_amount, $fee_to_collect, 8);
                } else { // This Type_1 order sells bases {BTC, LTC, DOGE}, so we will collect the fee when we decrease the base balance
                    $fee_to_collect = bcmul($original_sell_amount, $fee_rate, 8);
                    $decrease_tx_amount = bcadd($original_sell_amount, $fee_to_collect, 8);
                    $increase_tx_amount = $original_buy_amount;
                }

                // In-Progress Order User Balance Transaction Records
                $iposubt = array(); // in_progress_order_sold_user_balance_transaction
                $iposubt['instance_type'] = $scoin_available_balance;
                $iposubt['transaction_action'] = 'decrease_balance';
                $iposubt['transaction_amount'] = $decrease_tx_amount;
                $iposubt['type'] = 'order_sold';
                $iposubt['order_id'] = $new_order_id;
                $iposubt['user_id'] = $user->id;
                $iposubt['created_at'] = $date;
                $iposubt['updated_at'] = $date;

                $mofubt[] = $iposubt; // Insert to the mass user balance transaction array

                $ipobubt = array(); // in_progress_order_bought_user_balance_transaction
                $ipobubt['instance_type'] = $bcoin_available_balance;
                $ipobubt['transaction_action'] = 'increase_balance';
                $ipobubt['transaction_amount'] = $increase_tx_amount;
                $ipobubt['type'] = 'order_bought';
                $ipobubt['order_id'] = $new_order_id;
                $ipobubt['user_id'] = $user->id;
                $ipobubt['created_at'] = $date;
                $ipobubt['updated_at'] = $date;

                $mofubt[] = $ipobubt; // Insert into the mass user balance transaction array

                // In-Progress Order Profile Update
                $ipopu = array(); // in_progress_order_profile_update

                $ipopu[$scoin_available_balance] = $scoin_available_balance . ' - ' . $iposubt['transaction_amount'];
                $ipopu[$scoin_order_balance] = $scoin_order_balance;
                $ipopu[$bcoin_available_balance] = $bcoin_available_balance . ' + ' . $ipobubt['transaction_amount'];
                $ipopu[$bcoin_order_balance] = $bcoin_order_balance;

                $ipopu['id'] = $user->id;

                if (in_array($user->id, $mofpuc)) {
                    $mofpu[$user->id][$scoin_available_balance] = $mofpu[$user->id][$scoin_available_balance] . ' - ' . $iposubt['transaction_amount'];
                    $mofpu[$user->id][$bcoin_available_balance] = $mofpu[$user->id][$bcoin_available_balance] . ' + ' . $ipobubt['transaction_amount'];
                } else {
                    $mofpuc[] = $user->id;
                    $mofpu[$user->id] = $ipopu;
                }

                // DONETODO: Make a CRON access function to pick up pending internal_transactions and execute them.
                // In-Progress Order Internal Transaction Records
                $iposit = array(); // in_progress_order_sold_internal_transaction
                $iposit['amount'] = $original_sell_amount;
                $iposit['coin'] = $scoin;
                $iposit['from_account'] = $user->email;
                $iposit['to_account'] = 'default_' . $scoin;
                $iposit['status'] = 'pending';
                $iposit['type'] = 'order_sold';
                $iposit['order_id'] = $new_order_id;
                $iposit['user_id'] = $user->id;
                $iposit['daemon_server_id'] = $user->daemon_server_id;
                $iposit['created_at'] = $date;
                $iposit['updated_at'] = $date;

                $mofit[] = $iposit; // Insert into the mass internal transaction array

                $ipobit = array(); // in_progress_order_bought_internal_transaction
                $ipobit['amount'] = $original_buy_amount;
                $ipobit['coin'] = $bcoin;
                $ipobit['from_account'] = 'default_' . $bcoin;
                $ipobit['to_account'] = $user->email;
                $ipobit['status'] = 'pending';
                $ipobit['type'] = 'order_bought';
                $ipobit['order_id'] = $new_order_id;
                $ipobit['user_id'] = $user->id;
                $ipobit['daemon_server_id'] = $user->daemon_server_id;
                $ipobit['created_at'] = $date;
                $ipobit['updated_at'] = $date;

                $mofit[] = $ipobit; // Insert into the mass internal transaction array

                if ($fee_to_collect != 0) {
                    $ipotfit = array(); // in_progress_order_traded_fee_internal_transaction
                    $ipotfit['amount'] = $fee_to_collect;
                    $ipotfit['coin'] = $fee_coin;
                    $ipotfit['from_account'] = $user->email;
                    $ipotfit['to_account'] = 'default_' . $fee_coin . '_fee';
                    $ipotfit['status'] = 'pending';
                    $ipotfit['type'] = 'order_traded_fee';
                    $ipotfit['order_id'] = $new_order_id;
                    $ipotfit['user_id'] = $user->id;
                    $ipotfit['daemon_server_id'] = $user->daemon_server_id;
                    $ipotfit['created_at'] = $date;
                    $ipotfit['updated_at'] = $date;

                    $mofit[] = $ipotfit; // Insert into the mass internal transaction array
                }
            } elseif ($type == 'type_3') {
                // Set redirect param
                $redirect_param = 'open';

                // Fee calculation for the current newly created Type_3 order
                if ($mode == 's') { // This Type_3 order buys bases {BTC, LTC, DOGE}, so we will collect the fee when we increase the base balance
                    $converted_amount = bcmul($order_amount, $rate, 8);
                    $sold_amount = bcsub($original_sell_amount, $order_amount, 8);
                    $bought_amount = bcsub($original_buy_amount, $converted_amount, 8);
                    $fee_to_collect = bcmul($bought_amount, $fee_rate, 8);
                    $decrease_tx_amount = $sold_amount;
                    $increase_tx_amount = bcsub($bought_amount, $fee_to_collect, 8);
                } else { // This Type_3 order sells bases {BTC, LTC, DOGE}, so we will collect the fee when we decrease the base balance
                    $converted_amount = bcdiv($order_amount, $rate, 8);
                    $sold_amount = bcsub($original_sell_amount, $order_amount, 8);
                    $bought_amount = bcsub($original_buy_amount, $converted_amount, 8);
                    $fee_to_collect = bcmul($sold_amount, $fee_rate, 8);
                    $decrease_tx_amount = bcadd($sold_amount, $fee_to_collect, 8);
                    $increase_tx_amount = $bought_amount;
                }

                // Need to save the order record first, so that the fk is available for:
                // transient_order, internal_transaction, user_balance_transaction, and pivot table
                $new_order = new Order;
                $new_order->status = 'open';
                $new_order->sell_coin = $scoin;
                $new_order->buy_coin = $bcoin;
                $new_order->fee_coin = $fee_coin;
                $new_order->fee_rate = $fee_rate;
                $new_order->original_fee = $fee;
                $new_order->collected_fee = $fee_to_collect;
                $new_order->pending_fee = bcsub($fee, $fee_to_collect, 8);
                $new_order->original_sell_amount = $original_sell_amount;
                $new_order->original_buy_amount = $original_buy_amount;
                $new_order->sold_amount = $sold_amount;
                $new_order->bought_amount = $bought_amount;
                $new_order->current_sell_amount = $order_amount;
                $new_order->current_buy_amount = $converted_amount;
                $new_order->rate = $rate;
                $new_order->type = 'type_3';
                $new_order->mode = $long_mode;
                $completed_percentage_left = $sold_amount;
                $completed_percentage_right = bcdiv('100', $original_sell_amount, 8);
                $percentage = $completed_percentage_left * $completed_percentage_right;
                $new_order->completed_percentage = round($percentage, 3);
                $new_order->market_id = $market_id;
                $new_order->user_id = $user->id;
                $new_order->save();
                $new_order_id = $new_order->id; // order_id for fk from the newly created order.

                // In-Progress Order Related Transient Order
                $new_transient_order = new TransientOrder;
                $new_transient_order->sell_coin = $scoin;
                $new_transient_order->buy_coin = $bcoin;
                $new_transient_order->rate = $rate;
                $new_transient_order->sold_amount = $new_order->sold_amount;
                $new_transient_order->bought_amount = $new_order->bought_amount;
                $new_transient_order->mode = $long_mode;
                $new_transient_order->order_id = $new_order_id;
                $new_transient_order->user_id = $user->id;
                $new_transient_order->market_id = $market_id;
                $new_transient_order->save();
                $new_transient_order_id = $new_transient_order->id; // transient_order_id for pivot table;

                // In_Progress Order Add transient_order_id to pivot table
                foreach ($mofp as &$sofp) {
                    $sofp['transient_order_id'] = $new_transient_order_id;
                }

                // In-Progress Order Related Persistent Order
                $new_persistent_order = new PersistentOrder;
                $new_persistent_order->sell_coin = $scoin;
                $new_persistent_order->buy_coin = $bcoin;
                $new_persistent_order->rate = $rate;
                $new_persistent_order->sell_amount = $new_order->current_sell_amount;
                $new_persistent_order->buy_amount = $new_order->current_buy_amount;
                $new_persistent_order->available_sell_amount = $new_order->current_sell_amount;
                $new_persistent_order->available_buy_amount = $new_order->current_buy_amount;
                $new_persistent_order->mode = $long_mode;
                $new_persistent_order->status = 'open';
                $new_persistent_order->order_id = $new_order_id;
                $new_persistent_order->user_id = $user->id;
                $new_persistent_order->save();

                $date = date('Y-m-d H:i:s');

                // In-Progress Order User Balance Transaction Records
                $iposubt = array(); // in_progress_order_sold_user_balance_transaction
                $iposubt['instance_type'] = $scoin_available_balance;
                $iposubt['transaction_action'] = 'decrease_balance';
                $iposubt['transaction_amount'] = $decrease_tx_amount;
                $iposubt['type'] = 'order_sold';
                $iposubt['order_id'] = $new_order_id;
                $iposubt['user_id'] = $user->id;
                $iposubt['created_at'] = $date;
                $iposubt['updated_at'] = $date;

                $mofubt[] = $iposubt; // Insert to the mass user balance transaction array

                $ipocuabt = array(); // in_progress_order_create_user_available_balance_transaction
                $ipocuabt['instance_type'] = $scoin_available_balance;
                $ipocuabt['transaction_action'] = 'decrease_balance';
                $ipocuabt['transaction_amount'] = $new_order->current_sell_amount;
                $ipocuabt['type'] = 'order_created';
                $ipocuabt['order_id'] = $new_order_id;
                $ipocuabt['user_id'] = $user->id;
                $ipocuabt['created_at'] = $date;
                $ipocuabt['updated_at'] = $date;

                $mofubt[] = $ipocuabt; // Insert into the mass user balance transaction array

                $ipocuobt = array(); // in_progress_order_create_user_order_balance_transaction
                $ipocuobt['instance_type'] = $scoin_order_balance;
                $ipocuobt['transaction_action'] = 'increase_balance';
                $ipocuobt['transaction_amount'] = $new_order->current_sell_amount;
                $ipocuobt['type'] = 'order_created';
                $ipocuobt['order_id'] = $new_order_id;
                $ipocuobt['user_id'] = $user->id;
                $ipocuobt['created_at'] = $date;
                $ipocuobt['updated_at'] = $date;

                $mofubt[] = $ipocuobt; // Insert into the mass user balance transaction array

                $ipobubt = array(); // in_progress_order_bought_user_balance_transaction
                $ipobubt['instance_type'] = $bcoin_available_balance;
                $ipobubt['transaction_action'] = 'increase_balance';
                $ipobubt['transaction_amount'] = $increase_tx_amount;
                $ipobubt['type'] = 'order_bought';
                $ipobubt['order_id'] = $new_order_id;
                $ipobubt['user_id'] = $user->id;
                $ipobubt['created_at'] = $date;
                $ipobubt['updated_at'] = $date;

                $mofubt[] = $ipobubt; // Insert into the mass user balance transaction array

                // In-Progress Order Profile Update
                $ipopu = array(); // in_progress_order_profile_update

                $ipopu[$scoin_available_balance] = $scoin_available_balance . ' - ' . $iposubt['transaction_amount'] . ' - ' . $ipocuabt['transaction_amount'];
                $ipopu[$scoin_order_balance] = $scoin_order_balance . ' + ' . $ipocuobt['transaction_amount'];
                $ipopu[$bcoin_available_balance] = $bcoin_available_balance . ' + ' . $ipobubt['transaction_amount'];
                $ipopu[$bcoin_order_balance] = $bcoin_order_balance; // Notify the mass update that we don't update this field

                $ipopu['id'] = $user->id;

                if (in_array($user->id, $mofpuc)) {
                    $mofpu[$user->id][$scoin_available_balance] = $mofpu[$user->id][$scoin_available_balance] . ' - ' . $iposubt['transaction_amount'] . ' - ' . $ipocuabt['transaction_amount'];
                    $mofpu[$user->id][$scoin_order_balance] = $mofpu[$user->id][$scoin_order_balance] . ' + ' . $ipocuobt['transaction_amount'];
                    $mofpu[$user->id][$bcoin_available_balance] = $mofpu[$user->id][$bcoin_available_balance] . ' + ' . $ipobubt['transaction_amount'];
                } else {
                    $mofpuc[] = $user->id;
                    $mofpu[$user->id] = $ipopu;
                }

                // In-Progress Order Internal Transaction Records
                $iposit = array(); // in_progress_order_sold_internal_transaction
                $iposit['amount'] = $new_order->sold_amount;
                $iposit['coin'] = $scoin;
                $iposit['from_account'] = $user->email;
                $iposit['to_account'] = 'default_' . $scoin;
                $iposit['status'] = 'pending';
                $iposit['type'] = 'order_sold';
                $iposit['order_id'] = $new_order_id;
                $iposit['user_id'] = $user->id;
                $iposit['daemon_server_id'] = $user->daemon_server_id;
                $iposit['created_at'] = $date;
                $iposit['updated_at'] = $date;

                $mofit[] = $iposit; // Insert into the mass internal transaction array

                $ipobit = array(); // in_progress_order_bought_internal_transaction
                $ipobit['amount'] = $new_order->bought_amount;
                $ipobit['coin'] = $bcoin;
                $ipobit['from_account'] = 'default_' . $bcoin;
                $ipobit['to_account'] = $user->email;
                $ipobit['status'] = 'pending';
                $ipobit['type'] = 'order_bought';
                $ipobit['order_id'] = $new_order_id;
                $ipobit['user_id'] = $user->id;
                $ipobit['daemon_server_id'] = $user->daemon_server_id;
                $ipobit['created_at'] = $date;
                $ipobit['updated_at'] = $date;

                $mofit[] = $ipobit; // Insert into the mass internal transaction array

                if ($fee_to_collect != 0) {
                    $ipotfit = array(); // in_progress_order_traded_fee_internal_transaction
                    $ipotfit['amount'] = $fee_to_collect;
                    $ipotfit['coin'] = $fee_coin;
                    $ipotfit['from_account'] = $user->email;
                    $ipotfit['to_account'] = 'default_' . $fee_coin . '_fee';
                    $ipotfit['status'] = 'pending';
                    $ipotfit['type'] = 'order_traded_fee';
                    $ipotfit['order_id'] = $new_order_id;
                    $ipotfit['user_id'] = $user->id;
                    $ipotfit['daemon_server_id'] = $user->daemon_server_id;
                    $ipotfit['created_at'] = $date;
                    $ipotfit['updated_at'] = $date;

                    $mofit[] = $ipotfit; // Insert into the mass internal transaction array
                }
            }

            // DONETODO: Fire all Mass Insert and Mass Update query
            // DONETODO: Add Transient and Persistent Orders to pivot table

            // Finish saving the order and redirect

            // Mass Insert in user_balance_transactions table using $mofubt
            DB::table('user_balance_transactions')->insert($mofubt);

            // Mass Insert in internal_transactions table using $mofit
            DB::table('internal_transactions')->insert($mofit);

            // Mass Insert in pivot table (persistent_order_transient_order) using $mofp
            DB::table('persistent_order_transient_order')->insert($mofp);

            // Mass Update in profiles table using $mofpu

            // Preparing for Mass Update in profiles to reflect all inserted user_balance_transactions records
            $when_then_p_cab = ''; // WHEN THEN statement for {coin_available_balance} in profiles update
            $when_then_p_cob = ''; // WHEN THEN statement for {coin_order_balance} in profiles update
            $when_then_p_bab = ''; // WHEN THEN statement for {bcoin_available_balance} in profiles update
            $when_then_p_bob = ''; // WHEN THEN statement for {bcoin_order_balance} in profiles update
            $in_profiles = ''; // IN statement in profiles update

            foreach ($mofpu as $sofpu) {
                $when_then_p_cab = $when_then_p_cab . "WHEN " . $sofpu['id'] . " THEN " . $sofpu[$scoin_available_balance] . " ";
                $when_then_p_cob = $when_then_p_cob . "WHEN " . $sofpu['id'] . " THEN " . $sofpu[$scoin_order_balance] . " ";
                $when_then_p_bab = $when_then_p_bab . "WHEN " . $sofpu['id'] . " THEN " . $sofpu[$bcoin_available_balance] . " ";
                $when_then_p_bob = $when_then_p_bob . "WHEN " . $sofpu['id'] . " THEN " . $sofpu[$bcoin_order_balance] . " ";
                $in_profiles = $in_profiles . $sofpu['id'] . ',';
            }

            $in_profiles = substr($in_profiles, 0, -1);

            // added on 3-26-2014
            if (empty($when_then_p_cab)) {
                $scoin_available_balance_case = "";
            } else {
                $scoin_available_balance_case = $scoin_available_balance . " = CASE id ";
                $when_then_p_cab = $when_then_p_cab . "ELSE " . $scoin_available_balance . " END, ";
            }

            if (empty($when_then_p_cob)) {
                $scoin_order_balance_case = "";
            } else {
                $scoin_order_balance_case = $scoin_order_balance . " = CASE id ";
                $when_then_p_cob = $when_then_p_cob . "ELSE " . $scoin_order_balance . " END, ";
            }

            if (empty($when_then_p_bab)) {
                $bcoin_available_balance_case = "";
            } else {
                $bcoin_available_balance_case = $bcoin_available_balance . " = CASE id ";
                $when_then_p_bab = $when_then_p_bab . "ELSE " . $bcoin_available_balance . " END, ";
            }

            if (empty($when_then_p_bob)) {
                $bcoin_order_balance_case = "";
            } else {
                $bcoin_order_balance_case = $bcoin_order_balance . " = CASE id ";
                $when_then_p_bob = $when_then_p_bob . "ELSE " . $bcoin_order_balance . " END, ";
            }
            // end of added on 3-26-2014

            // DONETODO: FIX RAW SQL UPDATE BUG, look at the raw_sql_bug in sublime text 2
            // DONETODO: Continue to test out TYPE_1 and TYPE_3 orders.
            // DONETODO: build cancel order function
            // DONETODO: Test all cancel order functionality
            // DONETODO: Add Fee Structure to this whole order process
            // DONETODO: Reverse Type_1 and Type_3 order testing


            // Mass Update profiles
            DB::statement(DB::raw(
                "UPDATE profiles SET " .
                $scoin_available_balance_case . $when_then_p_cab .
                $scoin_order_balance_case . $when_then_p_cob .
                $bcoin_available_balance_case . $when_then_p_bab .
                $bcoin_order_balance_case . $when_then_p_bob .
                "updated_at = now() WHERE id IN (" . $in_profiles . ")"));

            // Mass Update in orders table using $mofou

            // Update Corresponding Buy Order
            $wtoba = ''; // when_then_order_bought_amount
            $wtocba = ''; // when_then_order_current_buy_amount
            $wtosa = ''; // when_then_order_sold_amount
            $wtocsa = ''; // when_then_order_current_sell_amount
            $wtocf = ''; // when_then_order_collected_fee
            $wtopf = ''; // when_then_order_pending_fee
            $wtos = ''; // when_then_order_status
            $wtocp = ''; // when_then_order_completed_percentage
            $in_orders = ''; // IN statement in orders update

            foreach ($mofou as $sofou) {
                $wtoba = $wtoba . "WHEN " . $sofou['id'] . " THEN " . "'" . $sofou['bought_amount'] . "' ";
                $wtocba = $wtocba . "WHEN " . $sofou['id'] . " THEN " . "'" . $sofou['current_buy_amount'] . "' ";
                $wtosa = $wtosa . "WHEN " . $sofou['id'] . " THEN " . "'" . $sofou['sold_amount'] . "' ";
                $wtocsa = $wtocsa . "WHEN " . $sofou['id'] . " THEN " . "'" . $sofou['current_sell_amount'] . "' ";
                $wtocf = $wtocf . "WHEN " . $sofou['id'] . " THEN " . "'" . $sofou['collected_fee'] . "' ";
                $wtopf = $wtopf . "WHEN " . $sofou['id'] . " THEN " . "'" . $sofou['pending_fee'] . "' ";
                $wtos = $wtos . "WHEN " . $sofou['id'] . " THEN " . "'" . $sofou['status'] . "' ";
                $wtocp = $wtocp . "WHEN " . $sofou['id'] . " THEN " . "'" . $sofou['completed_percentage'] . "' ";
                $in_orders = $in_orders . $sofou['id'] . ',';
            }

            $in_orders = substr($in_orders, 0, -1);

            // Mass update orders
            DB::statement(DB::raw(
                "UPDATE orders SET bought_amount = CASE id " .
                $wtoba . "END, current_buy_amount = CASE id " .
                $wtocba . "END, sold_amount = CASE id " .
                $wtosa . "END, current_sell_amount = CASE id " .
                $wtocsa . "END, collected_fee = CASE id " .
                $wtocf . "END, pending_fee = CASE id " .
                $wtopf . "END, status = CASE id " .
                $wtos . "END, completed_percentage = CASE id " .
                $wtocp . "END, updated_at = now() WHERE id IN (" . $in_orders . ")"));
            // Mass Update in persistent_orders table using $mofpou

            // Update Corresponding Buy Persistent Order
            $wtpoasa = ''; // when_then_persistent_order_available_sell_amount
            $wtpoaba = ''; // when_then_persistent_order_available_buy_amount
            $wtpos = ''; // when_then_persistent_order_status
            $in_persistent_orders = ''; // IN statement in persistent_orders update

            foreach ($mofpou as $sofpou) {
                $wtpoasa = $wtpoasa . "WHEN " . $sofpou['id'] . " THEN " . $sofpou['available_sell_amount'] . " ";
                $wtpoaba = $wtpoaba . "WHEN " . $sofpou['id'] . " THEN " . $sofpou['available_buy_amount'] . " ";
                $wtpos = $wtpos . "WHEN " . $sofpou['id'] . " THEN " . "'" . $sofpou['status'] . "' ";
                $in_persistent_orders = $in_persistent_orders . $sofpou['id'] . ',';
            }

            $in_persistent_orders = substr($in_persistent_orders, 0, -1);

            DB::statement(DB::raw(
                "UPDATE persistent_orders SET available_sell_amount = CASE id " .
                $wtpoasa . "END, available_buy_amount = CASE id " .
                $wtpoaba . "END, status = CASE id " .
                $wtpos . "END, updated_at = now() WHERE id IN (" . $in_persistent_orders . ")"));

            // Update Sidebar Statistic
            if ($mode == 's') {
                $coin = $scoin;
                $base = $bcoin;
            } else {
                $coin = $bcoin;
                $base = $scoin;
            }

            $market_prefix = $base . '_' . $coin . '_';
            $mrck = $market_prefix . 'rate_current'; // market_rate_current_key
            $mrdp = $market_prefix . 'delta_percent'; // market_rate_current_key
            $mrm = $market_prefix . 'movement'; // market_rate_current_key
            $myrlk = $market_prefix . 'yesterday_rate_list'; // market_yesterday_rate_list_key
            $mlr = $market_prefix . 'last_rate';
            $mrdhk = $market_prefix . 'rate_daily_high'; // market_rate_daily_high_key
            $mrdlk = $market_prefix . 'rate_daily_low'; // market_rate_daily_low_key
            $mvdk = $market_prefix . 'volume_daily'; // market_volume_daily_key

            $rate_daily_high = $redis->get($mrdhk);
            $rate_daily_low = $redis->get($mrdlk);
            $raw_yesterday_rate = $redis->lindex($myrlk, 0);
            $volume_daily = $redis->get($mvdk);

            // Only Calculate the percentage if the rate is rate 24 hours ago is not zero
            $current_rate = $rate;

            if ($raw_yesterday_rate != 0) {
                $yesterday_rate = strval($raw_yesterday_rate);
                $numerator = bcsub($current_rate, $yesterday_rate, 8);
                $delta = bcdiv($numerator, $yesterday_rate, 8);
                $delta_percent = bcmul($delta, '100', 2);
            } else {
                $delta_percent = '0.00';
            }

            if ($delta_percent == 0) {
                $movement = 'same';
            } elseif ($delta_percent < 0) {
                $movement = 'down';
            } else {
                $movement = 'up';
            }

            if ($current_rate > $rate_daily_high) {
                $redis->set($mrdhk, $current_rate);
            }

            if ($current_rate < $rate_daily_low) {
                $redis->set($mrdlk, $current_rate);
            }

            if ($fee_coin == $scoin) {
                $new_volume = $new_transient_order->sold_amount;
            } else {
                $new_volume = $new_transient_order->bought_amount;
            }

            $updated_volume_daily = bcadd($volume_daily, $new_volume, 8);

            $redis->set($mvdk, $updated_volume_daily);
            $redis->set($mlr, $current_rate);
            $redis->hset('market_rate_current', $mrck, $current_rate);
            $redis->hset('market_delta_percent', $mrdp, $delta_percent);
            $redis->hset('market_movement', $mrm, $movement);

            // Update the Queue
            $redis->lpop($queue_key);

            // Unlock the atomic order key
            $redis->del($lock_key);

            // Redirect
            return Redirect::action('UsersController@getOrderHistory', array($redirect_param))
                ->with('message', 'Order has been submitted!')
                ->with('message-level', 'alert-success');

        }
    }
}