<?php

class ApiMarketsController extends BaseController {

    public function __construct() {
        $this->beforeFilter('basic.once', array('except' => 'getRawStat'));
    }

    public function getYesterday($base = null, $coin = null, $rate = null) {
        // Check if Admin
        $user = Auth::user();
        if ($user->group != 'admin') {
            return Redirect::to('markets/secure-view')
                ->with('message', "Oops, we couldn't find what you're looking for.")
                ->with('message-level', 'alert-info');
        }

        $redis = Redis::connection();
        $myrlk = $base . '_' . $coin . '_yesterday_rate_list';

        for ($i = 1; $i <= 144; $i++) {
            $redis->rpush($myrlk, $rate);
        }

        return $myrlk . ' generated!';
    }

    public function getRefresh($id = null) {
        // Declare end time first
        $end = time();

        // Check if Admin
        $user = Auth::user();
        if ($user->group != 'admin') {
            return Redirect::to('markets/secure-view')
                ->with('message', "Oops, we couldn't find what you're looking for.")
                ->with('message-level', 'alert-info');
        }

        // Handling Parameter
        if (is_numeric($id)) {
            $id = ltrim($id, '0');
            $count = Market::count();
            if ($id > $count) {
                $id = 1;
            }
        } else {
            $id = 1;
        }

        // Getting the Market coin and base
        $market = Market::find($id);
        $coin = $market->coin;
        $base = $market->base;
        $market_prefix = $base . '_' . $coin . '_';

        // Universal variable declaration
        $redis = Redis::connection();
        $string_market_last_rate = $market_prefix . 'last_rate';
        $string_market_open_rate = $market_prefix . 'open_rate';
        $string_market_last_refreshed = $market_prefix . 'last_refreshed';
        $string_market_raw_data = $market_prefix . 'raw_data';
        $string_market_data = $market_prefix . 'data';
        $myrlk = $market_prefix . 'yesterday_rate_list'; // market_yesterday_rate_list_key rate history of the last 24 hours in 10 minutes interval, 144 elements supposedly
        $mrck = $market_prefix . 'rate_current'; // market_rate_current_key
        $mrdhk = $market_prefix . 'rate_daily_high'; // market_rate_daily_high_key
        $mrdlk = $market_prefix . 'rate_daily_low'; // market_rate_daily_low_key
        $mvdk = $market_prefix . 'volume_daily'; // market_volume_daily_key
        $mrdpk = $market_prefix . 'delta_percent'; // market_rate_current_key
        $mmk = $market_prefix . 'movement'; // market_movement_key
        $start = $redis->get($string_market_last_refreshed);
        $daily_start = $end - 86400; // From yesterday to today

        // Get 24 hour statistic
        $daily_sold_array = DB::select(DB::raw(
            "SELECT SUM(`bought_amount`) as volume_sold, MAX(`rate`) as high_rate, MIN(`rate`) as low_rate " .
            "FROM `transient_orders` " .
            "WHERE `market_id` = " . $id . " " .
            "AND `mode` = 'Sell' " .
            "AND (`created_at` BETWEEN FROM_UNIXTIME(" . $daily_start . ") AND FROM_UNIXTIME(" . $end . "))"
        ));
        $daily_sold_object = $daily_sold_array[0];
        $daily_sold_volume = $daily_sold_object->volume_sold;
        $daily_sold_high = $daily_sold_object->high_rate;
        $daily_sold_low = $daily_sold_object->low_rate;

        $daily_bought_array = DB::select(DB::raw(
            "SELECT SUM(`sold_amount`) as volume_bought, MAX(`rate`) as high_rate, MIN(`rate`) as low_rate " .
            "FROM `transient_orders` " .
            "WHERE `market_id` = " . $id . " " .
            "AND `mode` = 'Buy' " .
            "AND (`created_at` BETWEEN FROM_UNIXTIME(" . $daily_start . ") AND FROM_UNIXTIME(" . $end . "))"
        ));
        $daily_bought_object = $daily_bought_array[0];
        $daily_bought_volume = $daily_bought_object->volume_bought;
        $daily_bought_high = $daily_bought_object->high_rate;
        $daily_bought_low = $daily_bought_object->low_rate;

        $volume_daily = bcadd($daily_sold_volume, $daily_bought_volume, 8);

        $redis->set($mvdk, $volume_daily);

        // Only Change the 24hr high and low if there was volume in the last 24hr
        if ($volume_daily != 0) {
            if ((!empty($daily_sold_low) && !empty($daily_bought_low))) {
                $rate_daily_low = min($daily_bought_low, $daily_sold_low);
            } elseif (empty($daily_sold_low)) {
                $rate_daily_low = $daily_bought_low;
            } else {
                $rate_daily_low = $daily_sold_low;
            }

            $rate_daily_high = max($daily_bought_high, $daily_sold_high);
            $redis->set($mrdhk, $rate_daily_high);
            $redis->set($mrdlk, $rate_daily_low);
        }

        // Get Volume, note: volume is measured in base, regardless the order type.
        $volume_sold_array = DB::select(DB::raw(
            "SELECT SUM(`bought_amount`) as volume_sold " .
            "FROM `transient_orders` " .
            "WHERE `market_id` = " . $id . " " .
            "AND `mode` = 'Sell' " .
            "AND (`created_at` BETWEEN FROM_UNIXTIME(" . $start . ") AND FROM_UNIXTIME(" . $end . "))"
        ));
        $volume_sold_object = $volume_sold_array[0];
        $volume_sold = $volume_sold_object->volume_sold;
        $volume_bought_array = DB::select(DB::raw(
            "SELECT SUM(`sold_amount`) as volume_bought " .
            "FROM `transient_orders` " .
            "WHERE `market_id` = " . $id . " " .
            "AND `mode` = 'Buy' " .
            "AND (`created_at` BETWEEN FROM_UNIXTIME(" . $start . ") AND FROM_UNIXTIME(" . $end . "))"
        ));
        $volume_bought_object = $volume_bought_array[0];
        $volume_bought = $volume_bought_object->volume_bought;

        // If there was no order between the refresh and now, the volume is of course 0 in 8th decimal place.
        if (is_null($volume_sold) && is_null($volume_bought)) {
            $volume = "0.00000000";
        } else {
            $volume = bcadd($volume_sold, $volume_bought, 8);
        }

        // Get Open, Close, High and Low rate
        if ($volume == '0.00000000') {
            // No need to update any rate
            $last_rate = $redis->get($string_market_last_rate);
            $open_rate = $last_rate;
            $close_rate = $last_rate;
            $high_rate = $last_rate;
            $low_rate = $last_rate;
        } else {
            // Get Open Rate
            $open_rate = $redis->get($string_market_open_rate);

            // Get Close Rate
            $close_rate_object = DB::table('transient_orders')->select('rate')
                ->where('market_id', $id)->orderBy('created_at', 'desc')->first();
            $close_rate = $close_rate_object->rate;

            // Update Open Rate
            $redis->set($string_market_open_rate, $close_rate);

            // Get High and Low Rate
            $rates = DB::select(DB::raw(
                "SELECT MAX(`rate`) as high_rate, MIN(`rate`) as low_rate " .
                "FROM `transient_orders` " .
                "WHERE `market_id` = " . $id . " " .
                "AND (`created_at` BETWEEN FROM_UNIXTIME(" . $start . ") AND FROM_UNIXTIME(" . $end . "))"
            ));
            $rate = $rates[0];
            $high_rate = $rate->high_rate;
            $low_rate = $rate->low_rate;
        }

        // Update Sidebar Statistic
        $raw_yesterday_rate = $redis->lindex($myrlk, 0);

        // Only Calculate the percentage if the rate 24 hours ago is not zero
        $current_rate = strval($close_rate);

        if ($raw_yesterday_rate != 0) {
            $yesterday_rate = strval($raw_yesterday_rate);
            $numerator = bcsub($current_rate, $yesterday_rate, 8);
            $delta = bcdiv($numerator, $yesterday_rate, 8);
            $delta_percent = bcmul($delta, '100', 2);
        } else {
            $delta_percent = '0.00';
        }

        if ($delta_percent == 0) {
            $movement = 'same';
        } elseif ($delta_percent < 0) {
            $movement = 'down';
        } else {
            $movement = 'up';
        }

        // Update 24 HR rate list history
        $redis->rpush($myrlk, $current_rate);
        $redis->lpop($myrlk);

        // Set new data for Sidebar display
        $redis->hset('market_rate_current', $mrck, $current_rate);
        $redis->hset('market_delta_percent', $mrdpk, $delta_percent);
        $redis->hset('market_movement', $mmk, $movement);

        // Update Market Data
        $open_rate = floatval($open_rate);
        $high_rate = floatval($high_rate);
        $low_rate = floatval($low_rate);
        $close_rate = floatval($close_rate);
        $volume = floatval($volume);
        $date = $end * 1000;

        // Raw API (Internal Use)
        $raw_json_array = array($date, $open_rate, $high_rate, $low_rate, $close_rate, $volume);
        $raw_json_string = json_encode($raw_json_array);
        $raw_current_market_data = $redis->get($string_market_raw_data);
        if (!empty($raw_current_market_data)) {
            $raw_updated_market_data_part_a = substr($raw_current_market_data, 0, -1);
            $raw_updated_market_data_part_b = ',' . $raw_json_string . ']';
            $raw_updated_market_data = $raw_updated_market_data_part_a . $raw_updated_market_data_part_b;
        } else {
            $raw_updated_market_data = '[' . $raw_json_string . ']';
        }
        $redis->set($string_market_raw_data, $raw_updated_market_data);

        // Public API (Accessible by anyone)
        $json_array = array('date' => $date, 'open' => $open_rate, 'high' => $high_rate, 'low' => $low_rate, 'close' => $close_rate, 'volume' => $volume);
        $json_string = json_encode($json_array);
        $current_market_data = $redis->get($string_market_data);
        if (!empty($current_market_data)) {
            $updated_market_data_part_a = substr($current_market_data, 0, -1);
            $updated_market_data_part_b = ',' . $json_string . ']';
            $updated_market_data = $updated_market_data_part_a . $updated_market_data_part_b;
        } else {
            $updated_market_data = '[' . $json_string . ']';
        }
        $redis->set($string_market_data, $updated_market_data);

        // Update last refreshed time of this market
        $redis->set($string_market_last_refreshed, $end);

        return 'Market statistics refreshed!';
    }

    public function getStat($id = null) {
        // Check if Admin
        $user = Auth::user();
        if ($user->group != 'admin') {
            return Redirect::to('markets/secure-view')
                ->with('message', "Oops, we couldn't find what you're looking for.")
                ->with('message-level', 'alert-info');
        }

        // Handling Parameter
        if (is_numeric($id)) {
            $id = ltrim($id, '0');
            $count = Market::count();
            if ($id > $count) {
                $id = 1;
            }
        } else {
            $id = 1;
        }

        // Getting the Market coin and base
        $market = Market::find($id);
        $coin = $market->coin;
        $base = $market->base;
        $market_prefix = $base . '_' . $coin . '_';
        $string_market_data = $market_prefix . 'data';
        $redis = Redis::connection();
        $market_data = $redis->get($string_market_data);

        if (!empty($market_data)) {
            $contents = $market_data;
            $response = Response::make($contents, 200);
            $response->header('Content-Type', 'application/json');
            return $response;
        } else {
            return 'Market statistics not available.';
        }
    }

    public function getRawStat($id = null) {
        // Handling Parameter
        if (is_numeric($id)) {
            $id = ltrim($id, '0');
            $count = Market::count();
            if ($id > $count) {
                $id = 1;
            }
        } else {
            $id = 1;
        }

        // Getting the Market coin and base
        $market = Market::find($id);
        $coin = $market->coin;
        $base = $market->base;
        $market_prefix = $base . '_' . $coin . '_';
        $string_market_data = $market_prefix . 'raw_data';
        $redis = Redis::connection();
        $market_data = $redis->get($string_market_data);

        if (!empty($market_data)) {
            $contents = $market_data;
            $response = Response::make($contents, 200);
            $response->header('Content-Type', 'application/json');
            return $response;
        } else {
            return 'Market statistics not available.';
        }
    }
}