<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreFieldsToDepositsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('deposits', function(Blueprint $table)
        {
            $table->string('last_block', 255);
            $table->string('status', 20);
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('deposits', function(Blueprint $table)
        {
            $table->dropColumn('last_block');
            $table->dropColumn('status');
        });
	}

}
