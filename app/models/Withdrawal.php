<?php

class Withdrawal extends Eloquent {

    protected $table = 'withdrawals';

    public static $process_rules = array(
        'to_address'=>'required|alpha_num',
        'withdrawal_amount'=>'required|numeric',
        'password'=>'required|passcheck'
    );

    // There exists a 1 to 1 relationship between Withdrawal{Entity} and UserBalanceTransaction{Entity}
    public function userBalanceTransaction()
    {
        return $this->hasOne('UserBalanceTransaction');
    }

    // There exists a 1 to 1 relationship between Withdrawal{Entity} and InternalTransaction{Entity}
    public function internalTransaction()
    {
        return $this->hasOne('InternalTransaction');
    }

    // There exists a N to 1 relationship between Withdrawal{Entity} and User{Entity}.
    public function user()
    {
        return $this->belongsTo('User');
    }

    // There exists a N to 1 relationship between Withdrawal{Entity} and DaemonServer{Entity}.
    public function daemonServer()
    {
        return $this->belongsTo('DaemonServer');
    }

}