<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepositsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('deposits', function(Blueprint $table)
		{
            $table->increments('id');
            $table->string('coin', 20); // which coin daemon
            $table->decimal('total_received_before', 20, 8);
            $table->decimal('total_received_after', 20, 8);
            $table->decimal('deposit_amount', 20, 8);
            $table->integer('user_id');
            $table->integer('daemon_server_id');
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('deposits');
	}

}
