<?php

class DaemonServer extends Eloquent {

    protected $table = 'daemon_servers';

    // There exists a 1 to N relationship between DaemonServer{Entity} and User{Entity}.
    public function users()
    {
        return $this->hasMany('User');
    }

    // There exists a 1 to N relationship between DaemonServer{Entity} and Profile{Entity}.
    public function profiles()
    {
        return $this->hasMany('Profile');
    }

    // There exists a 1 to N relationship between DaemonServer{Entity} and InternalTransaction{Entity}.
    public function internalTransactions()
    {
        return $this->hasMany('InternalTransaction');
    }

    // There exists a 1 to N relationship between DaemonServer{Entity} and Withdrawal{Entity}.
    public function withdrawals()
    {
        return $this->hasMany('Withdrawal');
    }

    // There exists a 1 to N relationship between DaemonServer{Entity} and Deposit{Entity}.
    public function deposits()
    {
        return $this->hasMany('Deposit');
    }

}