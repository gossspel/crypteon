<?php

/*
|--------------------------------------------------------------------------
| Register The Laravel Class Loader
|--------------------------------------------------------------------------
|
| In addition to using Composer, you may use the Laravel class loader to
| load your controllers and models. This is useful for keeping all of
| your classes in the "global" namespace without Composer updating.
|
*/

ClassLoader::addDirectories(array(

	app_path().'/commands',
	app_path().'/controllers',
	app_path().'/models',
	app_path().'/database/seeds',

));

/*
|--------------------------------------------------------------------------
| Application Error Logger
|--------------------------------------------------------------------------
|
| Here we will configure the error logger setup for the application which
| is built on top of the wonderful Monolog library. By default we will
| build a basic log file setup which creates a single file for logs.
|
*/

Log::useFiles(storage_path().'/logs/laravel.log');

/*
|--------------------------------------------------------------------------
| Application Error Handler
|--------------------------------------------------------------------------
|
| Here you may handle any errors that occur in your application, including
| logging them or displaying custom views for specific errors. You may
| even register several error handlers to handle different types of
| exceptions. If nothing is returned, the default error view is
| shown, which includes a detailed stack trace during debug.
|
*/

App::error(function(Exception $exception, $code)
{
	Log::error($exception);
    // added on 2-21-2014 to test error handling
//    return Response::view('errors.missing', array(), 500);
//    return Redirect::to('users/register')->with('message', 'Something went wrong, please try again!');
    // end of added part on 2-21-2014
});

/*
|--------------------------------------------------------------------------
| Maintenance Mode Handler
|--------------------------------------------------------------------------
|
| The "down" Artisan command gives you the ability to put an application
| into maintenance mode. Here, you will define what is displayed back
| to the user if maintenance mode is in effect for the application.
|
*/

App::down(function()
{
	return View::make('maintenance');
});

// 404 Not Found Error Handler
App::missing(function($exception)
{
    if (Auth::check()) {
        return Redirect::to('markets/secure-view')
            ->with('message', "Oops, we couldn't find what you're looking for.")
            ->with('message-level', 'alert-info');
    } else {
        return Redirect::to('/')
            ->with('message', "Oops, we couldn't find what you're looking for.")
            ->with('message-level', 'alert-info');
    }
});

/*
|--------------------------------------------------------------------------
| Require The Filters File
|--------------------------------------------------------------------------
|
| Next we will load the filters file for the application. This gives us
| a nice separate location to store our route and application filter
| definitions instead of putting them all in the main routes file.
|
*/

require app_path().'/filters.php';

/*
 * Require The Validators File
 *
 * Custom Validation Rules, added on 3-5-2014
 */

require app_path().'/validators.php';

/*
 * Require The JSON RPC Client File
 *
 * JSON RPC Client for crypto-currency, added on 3-19-2014
 */

require app_path().'/jsonRPCClient.php';

/*
 * Require The ReCaptcha Library File
 *
 * ReCaptcha Library for Registration Captcha, added on 4-26-2014
 */

require app_path().'/recaptchalib.php';

/*
 * Require The Google Authenticator File
 *
 * Google Authenticator for 2 factor authentication, added on 5-14-2014
 */

require app_path().'/GoogleAuthenticator.php';