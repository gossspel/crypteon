<?php

class DepositsController extends BaseController {

    public function __construct() {
        $this->beforeFilter('basic.once');
    }

    public function getProcess($coin = null, $minconf = null, $daemon_server_id = null) {
        // Check if Admin
        $user = Auth::user();
        if ($user->group != 'admin') {
            return Redirect::to('markets/secure-view')
                ->with('message', "Oops, we couldn't find what you're looking for.")
                ->with('message-level', 'alert-info');
        }

        // Check if server is in Maintenance mode
        $redis = Redis::connection();

        if ($redis->get('server_mode') == 'maintenance') {
            return 'Server is on maintenance mode. Deposit Process is postponed.';
        }

        // Check if CRON Process is locked
        $cron_key = 'CRON_deposits_' . $coin;

        if ($redis->exists($cron_key)) {
            $custom_error = 'deposit_process_aborted: locked CRON process in ' . $coin . ' Daemon at Daemon Server '. $daemon_server_id;

            // Email to report error
            $mail_data = array('error_message' => $custom_error);
            Mail::later(10, 'emails.admin.error', $mail_data, function($message)
            {
                $message->to('crypteonnet@gmail.com', 'Crypteon')->subject('Error on server');
            });

            Log::error($custom_error);
            return $custom_error;
        } else {
            // Lock CRON Process
            $lock_time = time();
            $redis->setex($cron_key, 600, $lock_time);
        }

        // Extend Maximum Execution Time
        set_time_limit(900);

        // Getting parameter for calling listsinceblock in coin daemon.
        $redis_key = 'listsinceblock_' . $coin;
        $redis_lrt = 'lrt_' . $coin; // redis_last_received_time
        $redis_lrti = 'lrti_' . $coin; // redis_last_received_transactions_id
        $listsinceblock_param = $redis->get($redis_key);
        $last_received_time = $redis->get($redis_lrt);
        $last_received_transactions = $redis->smembers($redis_lrti);

        if (empty($last_received_time)) {
            $last_received_time = '0';
        }

        if (empty($last_received_transactions)) {
            $last_received_transactions = array('filler_haystack');
        }

        // Getting Coin Daemon
        $coin_json_rpc = $coin . '_JSON_RPC_DS_' . $daemon_server_id;
        $json_rpc_url = 'http://' . $_ENV[$coin_json_rpc] . '/';
        $coind = new jsonRPCClient($json_rpc_url);

        // Profile Update Variable Declaration
        $profiles_update = array();
        $has_pending_update = false;

        // Getting deposits that are still pending
        $deposit_select = array('deposits.id',
            'deposits.txid',
            'deposits.deposit_amount',
            'deposits.user_id',
            'users.email');

        $pending_deposits = DB::table('deposits')
            ->join('users', 'deposits.user_id', '=', 'users.id')
            ->select($deposit_select)
            ->where('deposits.coin', $coin)
            ->where('deposits.status', 'pending')
            ->where('deposits.daemon_server_id', $daemon_server_id)
            ->get(); // The returned result is an array of php standard classes (objects).

        $completing_deposits = DB::table('deposits')
            ->join('users', 'deposits.user_id', '=', 'users.id')
            ->select($deposit_select)
            ->where('deposits.coin', $coin)
            ->where('deposits.status', 'completing')
            ->where('deposits.daemon_server_id', $daemon_server_id)
            ->get(); // The returned result is an array of php standard classes (objects).

        if ((!empty($pending_deposits)) || (!empty($completing_deposits))) {
            $unix_now = time();

            $mdu = array(); // Mass Deposit Update
            $mdu_completed = false; // Variable for checking if a pending deposit became completed
            $mdu_failed = false; // Variable for checking if a pending deposit became failed
            $mdu_completed_list = ''; // List of pending deposits that became completed
            $mdu_failed_list = ''; // List of pending deposits that became failed
            $mdu_full_list = ''; // List of all pending deposits
            $mubti = array(); // Mass User Balance Transaction Insert
            $date = date('Y-m-d H:i:s');

            if (!empty($pending_deposits)) {
                foreach ($pending_deposits as $pending_deposit) {
                    // Perform Daemon "gettransaction" call
                    try {
                        $pdt = $coind->gettransaction($pending_deposit->txid); // pending_deposit_transaction
                    } catch (Exception $e) {
                        // Unlock CRON Process
                        $redis->del($cron_key);

                        // Log Error Redirect
                        $error = $e->getMessage();
                        $custom_error = 'deposit_process_aborted: failed gettransaction from ' . $coin . ' Daemon at Daemon Server '. $daemon_server_id;
                        Log::error($custom_error);
                        Log::error($error);
                        return $custom_error;
                    }

                    $pending_duration = $unix_now - $pdt['timereceived'];

                    if ($pdt['confirmations'] >= $minconf) {
                        // Single User Balance Transaction Insert
                        $subti = array();
                        $subti['instance_type'] = $coin . '_available_balance';
                        $subti['transaction_amount'] = $pending_deposit->deposit_amount;
                        $subti['transaction_action'] = 'increase_balance';
                        $subti['type'] = 'deposit';
                        $subti['deposit_id'] = $pending_deposit->id;
                        $subti['user_id'] = $pending_deposit->user_id;
                        $subti['created_at'] = $date;
                        $subti['updated_at'] = $date;

                        $mubti[] = $subti;

                        // Single Deposit Update
                        $sdu = array();
                        $sdu['id'] = $pending_deposit->id;
                        $sdu['status'] = 'completed';

                        $mdu[] = $sdu;
                        $mdu_completed = true;
                        $mdu_completed_list = $mdu_completed_list . $pending_deposit->id . ',';

                        // Set amount to be 0 if an account does not have a deposit in this round yet
                        if(!isset($profiles_update[$pending_deposit->email])) {
                            $profiles_update[$pending_deposit->email] = '0';
                        }
                        $profiles_update[$pending_deposit->email] = bcadd(strval($profiles_update[$pending_deposit->email]), $pending_deposit->deposit_amount, 8);

                        $has_pending_update = true;

                    } elseif ($pending_duration > 86400) { // Deposit failed after not being confirmed for more than one day. Most likely ended up in abandoned fork
                        $sdu = array(); // Single Deposit Update
                        $sdu['id'] = $pending_deposit->id;
                        $sdu['status'] = 'failed';

                        $mdu[] = $sdu;
                        $mdu_failed = true;
                        $mdu_failed_list = $mdu_failed_list . $pending_deposit->id . ',';
                    }

                    $mdu_full_list = $mdu_full_list . $pending_deposit->id . ',';
                }
            }

            if (!empty($completing_deposits)) {
                foreach ($completing_deposits as $completing_deposit) {
                    // Single User Balance Transaction Insert
                    $subti = array();
                    $subti['instance_type'] = $coin . '_available_balance';
                    $subti['transaction_amount'] = $completing_deposit->deposit_amount;
                    $subti['transaction_action'] = 'increase_balance';
                    $subti['type'] = 'deposit';
                    $subti['deposit_id'] = $completing_deposit->id;
                    $subti['user_id'] = $completing_deposit->user_id;
                    $subti['created_at'] = $date;
                    $subti['updated_at'] = $date;

                    $mubti[] = $subti;

                    // Single Deposit Update
                    $sdu = array();
                    $sdu['id'] = $completing_deposit->id;
                    $sdu['status'] = 'completed';

                    $mdu[] = $sdu;
                    $mdu_completed = true;
                    $mdu_completed_list = $mdu_completed_list . $completing_deposit->id . ',';
                }
            }

            // Mass Pending Deposit Update and User Balance Transaction Insert
            if (($mdu_completed == true) && ($mdu_failed == true)) { // Have both completed and failed pending deposits
                $mdu_full_list = substr($mdu_full_list, 0, -1);
                $mdu_completed_list = substr($mdu_completed_list, 0, -1);
                $mdu_failed_list = substr($mdu_failed_list, 0, -1);
                DB::statement(DB::raw(
                    "UPDATE deposits SET status = CASE " .
                        "WHEN id IN (" . $mdu_completed_list . ") THEN 'completed' " .
                        "WHEN id IN (" . $mdu_failed_list . ") THEN 'failed' END, " .
                        "updated_at = now() WHERE id IN (" . $mdu_full_list . ")"
                ));
                DB::table('user_balance_transactions')->insert($mubti);
            } elseif ($mdu_completed == true) { // Only have completed pending deposits (most likely case)
                $mdu_completed_list = substr($mdu_completed_list, 0, -1);
                DB::statement(DB::raw(
                    "UPDATE deposits SET status = 'completed', " .
                        "updated_at = now() WHERE id IN (" . $mdu_completed_list . ")"
                ));
                DB::table('user_balance_transactions')->insert($mubti);
            } elseif ($mdu_failed == true) { // Only have failed pending deposits
                $mdu_failed_list = substr($mdu_failed_list, 0, -1);
                DB::statement(DB::raw(
                    "UPDATE deposits SET status = 'failed', " .
                        "updated_at = now() WHERE id IN (" . $mdu_failed_list . ")"
                ));
            }
        }

        // Perform Daemon "listsinceblock" call
        try {
            if (empty($listsinceblock_param)) {
                $blocks = $coind->listsinceblock();
            } else {
                $blocks = $coind->listsinceblock($listsinceblock_param);
            }
        } catch (Exception $e) {
            // Unlock CRON Process
            $redis->del($cron_key);

            // Log Error Redirect
            $error = $e->getMessage();
            $custom_error = 'deposit_process_aborted: failed listsinceblock from ' . $coin . ' Daemon at Daemon Server '. $daemon_server_id;
            Log::error($custom_error);
            Log::error($error);
            return $custom_error;
        }

        // Update listsinceblock parameter for next round
        $last_block = $blocks['lastblock'];
        $redis->set($redis_key, $last_block);

        $transactions = $blocks['transactions'];
        $confirmed_deposits = array();
        $unconfirmed_deposits = array();

        $default_COIN = 'default_' . $coin;
        $default_COIN_fee = 'default_' . $coin . '_fee';
        $midip = array(); // Mass Insert Deposit In Profiles
        $new_last_received_transactions = array();
        $nrtc = 0; // new_received_transactions_count

        // Process transactions from "listsinceblock" call
        if (!empty($transactions)) {
            foreach ($transactions as $transaction) {
                $is_repeated = false;

                // Make sure the transaction's category is "receive"
                if (($transaction['category'] == 'receive') && ($transaction['timereceived'] >= $last_received_time)) {
                    // Eliminate repeated transaction
                    if ($transaction['timereceived'] == $last_received_time) {
                        if (in_array($transaction['txid'], $last_received_transactions)) {
                            $is_repeated = true;
                        }
                    }

                    // Eliminate all transactions related to non-user accounts, then sort the remaining transactions into confirmed_deposits and unconfirmed_deposits
                    if (($transaction['account'] != $default_COIN) && ($transaction['account'] != $default_COIN_fee) && ($transaction['account'] != '') && ($is_repeated == false)) {
                        $nrtc++;

                        if ($nrtc == 1) {
                            $new_last_received_time = $transaction['timereceived'];
                        }

                        $midip[] = $transaction['account'];
                        $new_last_received_transactions[] = $transaction['txid'];
                        if ($transaction['confirmations'] >= $minconf) {
                            $confirmed_deposits[] = $transaction;

                            // Set amount to be 0 if an account does not have a deposit in this round yet
                            if(!isset($profiles_update[$transaction['account']])) {
                                $profiles_update[$transaction['account']] = 0;
                            }

                            $profiles_update[$transaction['account']] = bcadd(strval($profiles_update[$transaction['account']]), strval($transaction['amount']), 8);
                        } else {
                            $unconfirmed_deposits[] = $transaction;
                        }
                    }
                }
            }

            // Update last_received_time and last_received_transactions for next round
            if ($nrtc >= 1) {
                $redis->set($redis_lrt, $new_last_received_time);
                $redis->del($redis_lrti);
                $redis->sadd($redis_lrti, $new_last_received_transactions);
            }

            if ((empty($confirmed_deposits) && empty($unconfirmed_deposits) && ($has_pending_update == false))) {
                // Unlock CRON Process
                $redis->del($cron_key);
                return 'No pending Deposits on ' . $coin . ' at Daemon Server ' . $daemon_server_id;
            }
        } elseif ($has_pending_update == false) {
            // Unlock CRON Process
            $redis->del($cron_key);
            return 'No pending Deposits on ' . $coin . ' at Daemon Server ' . $daemon_server_id;
        }

        // Mass Insert in deposits table
        if ((!empty($confirmed_deposits)) || (!empty($unconfirmed_deposits))) {
            // Prepare for Mass Insert in deposits table
            $profile_select = array('id',
                'email');

            $profiles = DB::table('profiles')
                ->select($profile_select)
                ->whereIn('email', $midip)
                ->where('daemon_server_id', $daemon_server_id)
                ->get(); // The return result is a php standard class (object).

            $profile_index = array();

            foreach ($profiles as $profile) {
                $profile_index[$profile->email] = $profile->id;
            }

            $mdi = array(); // Mass Deposit Insert
            $date = date('Y-m-d H:i:s');

            if (!empty($confirmed_deposits)) {
                foreach ($confirmed_deposits as $confirmed_deposit) {
                    $sdi = array(); // Single Deposit Insert
                    $sdi['coin'] = $coin;
                    $sdi['deposit_amount'] = $confirmed_deposit['amount'];
                    $sdi['user_id'] = $profile_index[$confirmed_deposit['account']];
                    $sdi['daemon_server_id'] = $daemon_server_id;
                    $sdi['txid'] = $confirmed_deposit['txid'];
                    $sdi['status'] = 'completing';
                    $sdi['last_block'] = $last_block;
                    $sdi['created_at'] = $date;
                    $sdi['updated_at'] = $date;

                    $mdi[] = $sdi;
                }
            }

            if (!empty($unconfirmed_deposits)) {
                foreach ($unconfirmed_deposits as $unconfirmed_deposit) {
                    $sdi = array(); // Single Deposit Insert
                    $sdi['coin'] = $coin;
                    $sdi['deposit_amount'] = $unconfirmed_deposit['amount'];
                    $sdi['user_id'] = $profile_index[$unconfirmed_deposit['account']];
                    $sdi['daemon_server_id'] = $daemon_server_id;
                    $sdi['txid'] = $unconfirmed_deposit['txid'];
                    $sdi['status'] = 'pending';
                    $sdi['last_block'] = $last_block;
                    $sdi['created_at'] = $date;
                    $sdi['updated_at'] = $date;

                    $mdi[] = $sdi;
                }
            }

            // Execute Mass Insert
            DB::table('deposits')->insert($mdi);
        }

        // Mass update in profiles table
        if (!empty($profiles_update)) {
            // Prepare for Mass Update in profiles table
            $string_coin_total_received = $coin . '_total_received';
            $string_coin_available_balance = $coin . '_available_balance';
            $when_then_p_cab = ''; // WHEN THEN statement for {coin_available_balance} in profiles update
            $when_then_p_ctr = ''; // WHEN THEN statement for {coin_total_received} in profiles update
            $raw_in_profiles = '';

            foreach ($profiles_update as $key => $value) {
                $when_then_p_cab = $when_then_p_cab . 'WHEN ' . '"' . $key . '"' . ' THEN ' . $string_coin_available_balance . ' + ' . $value . ' ';
                $when_then_p_ctr = $when_then_p_ctr . 'WHEN ' . '"' . $key . '"' . ' THEN ' . $string_coin_total_received . ' + ' . $value . ' ';
                $raw_in_profiles = $raw_in_profiles . '"' . $key . '"' . ',';
            }

            $raw_in_profiles = substr($raw_in_profiles, 0, -1);

            // Execute Mass Update
            DB::statement(DB::raw(
                "UPDATE profiles SET " .
                    $string_coin_available_balance . " = CASE email " .
                    $when_then_p_cab . "END, " .
                    $string_coin_total_received . " = CASE email ".
                    $when_then_p_ctr . "END, " .
                    "updated_at = now() WHERE email IN (" . $raw_in_profiles . ")"
            ));
        }

        // Unlock CRON Process
        $redis->del($cron_key);
        
        // Return Finish Message
        return 'Deposit Process completed on ' . $coin . ' at Daemon Server ' . $daemon_server_id . '.';
    }
}