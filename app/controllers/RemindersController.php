<?php

class RemindersController extends BaseController {

    protected $layout = "layouts.test";

    public function __construct() {
        $this->beforeFilter('csrf', array('on'=>'post'));
    }

	/**
	 * Display the password reminder view.
	 *
	 * @return Response
	 */
	public function getRemind()
	{
        $this->layout->content = View::make('password.remind');
	}

	/**
	 * Handle a POST request to remind a user of their password.
	 *
	 * @return Response
	 */
	public function postRemind()
	{
        // Variable declaration
        $redis = Redis::connection();
        $email = Input::get('email');
        $key = $email . '_password_reset';
        $lock_key = $email . '_password_reset_disabled';

        // Check if server is in Maintenance mode
        if ($redis->get('server_mode') == 'maintenance') {
            return View::make('maintenance');
        }

        // Check if email is valid
        $rules = array('email'=>'required|email');
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->passes()) {
            $user = DB::table('users')->select('id')->where('email', $email)->first();
        } else {
            sleep(3);
            $message = 'Please enter a valid email address.';
            return Redirect::to('password/remind')->with('message', $message)
                ->with('message-level', 'alert-danger');
        }

        if (empty($user)) {
            sleep(3);
            $message = 'The email you entered did not match any account, please try gain.';
            return Redirect::to('password/remind')->with('message', $message)
                ->with('message-level', 'alert-danger');
        }

        // Check if password reset is disabled
        if ($redis->exists($lock_key)) {
            $message = 'Password reset on this account is temporarily disabled, please try again later.';
            return Redirect::to('password/remind')->with('message', $message)
                ->with('message-level', 'alert-danger');
        }

        // Temporarily lock account if there are at least 5 password reset attempts
        if ($redis->exists($key)) {
            $current_count = $redis->get($key);
            if ($current_count >= 5) {
                $message = 'Password reset on this account has been disabled for 30 minutes because there are too many password reset attempts.';
                $ip = Request::getClientIp();
                $redis->setex($lock_key, 1800, '1');
                $redis->rpush('password_reset_spammers', $email . ', ' . $ip);
                return Redirect::to('/')->with('message', $message)
                    ->with('message-level', 'alert-danger');
            } else {
                $redis->incr($key);
            }
        } else {
            $redis->incr($key);
            $redis->expire($key, 3600);
        }

        switch ($response = Password::remind(Input::only('email'), function($message)
        {
            $message->subject('Password Reset');
        }))
		{
			case Password::INVALID_USER:
                sleep(2);
				return Redirect::to('password/remind')->with('message', Lang::get($response))
                    ->with('message-level', 'alert-danger');

			case Password::REMINDER_SENT:
                return Redirect::to('password/remind')->with('message', Lang::get($response))
                    ->with('message-level', 'alert-success');
		}
	}

	/**
	 * Display the password reset view for the given token.
	 *
	 * @param  string  $token
	 * @return Response
	 */
	public function getReset($token = null)
	{
		if (is_null($token)) {
            return Redirect::to('/')
                ->with('message', "Oops, we couldn't find what you're looking for.")
                ->with('message-level', 'alert-info');
        }

        $this->layout->content = View::make('password.reset')
            ->with('token', $token);
	}

	/**
	 * Handle a POST request to reset a user's password.
	 *
	 * @return Response
	 */
	public function postReset()
	{
        // Check if server is in Maintenance mode
        $redis = Redis::connection();

        if ($redis->get('server_mode') == 'maintenance') {
            return View::make('maintenance');
        }

        // Check for token existence
        $token = Input::get('token');

        if (empty($token)) {
            return Redirect::back()->with('message', 'Error detected, please refresh and try again.')
                ->with('message-level', 'alert-danger');
        }

        // Form validation
        $rules = array('email'=>'required|email',
            'password'=>'required|alpha_num|between:6,12|confirmed',
            'password_confirmation'=>'required|alpha_num|between:6,12');

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->passes()) {
            $credentials = Input::only(
                'email', 'password', 'password_confirmation', 'token'
            );
        } else {
            // validation has failed, display error messages
            return Redirect::back()
                ->with('message', 'Unsuccessful form submit, please check the errors below.')
                ->with('message-level', 'alert-danger')
                ->withInput(Input::only('email'))
                ->withErrors($validator);
        }

        // Response Handling
		$response = Password::reset($credentials, function($user, $password)
		{
			$user->password = Hash::make($password);

			$user->save();
		});

		switch ($response)
		{
			case Password::INVALID_PASSWORD:
			case Password::INVALID_TOKEN:
			case Password::INVALID_USER:
				return Redirect::back()->with('message', Lang::get($response))
                    ->with('message-level', 'alert-danger');

			case Password::PASSWORD_RESET:
                $reset_user_id_object = DB::table('users')->select('id')->where('email', $credentials['email'])->first();
                $reset_user_id = $reset_user_id_object->id;
                Auth::loginUsingId($reset_user_id);
				return Redirect::to('users/wallet')->with('message', 'Password updated successfully, have fun trading!')
                    ->with('message-level', 'alert-success');
		}
	}

}