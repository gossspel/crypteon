<?php

class InternalTransactionsController extends BaseController {

    public function __construct() {
        $this->beforeFilter('basic.once');
    }

    public function getProcess($coin, $daemon_server_id) {
        // Check if Admin
        $user = Auth::user();
        if ($user->group != 'admin') {
            return Redirect::to('markets/secure-view')
                ->with('message', "Oops, we couldn't find what you're looking for.")
                ->with('message-level', 'alert-info');
        }

        // Check if server is in Maintenance mode
        $redis = Redis::connection();

        if ($redis->get('server_mode') == 'maintenance') {
            return 'Server is on maintenance mode. Internal Transaction Process is postponed.';
        }

        // Check if CRON Process is locked
        $cron_key = 'CRON_internal_transactions_' . $coin;

        if ($redis->exists($cron_key)) {
            $custom_error = 'internal_transaction_process_aborted: locked CRON process in ' . $coin . ' Daemon at Daemon Server '. $daemon_server_id;

            // Email to report error
            $mail_data = array('error_message' => $custom_error);
            Mail::later(10, 'emails.admin.error', $mail_data, function($message)
            {
                $message->to('crypteonnet@gmail.com', 'Crypteon')->subject('Error on server');
            });

            Log::error($custom_error);
            return $custom_error;
        } else {
            // Lock CRON Process
            $lock_time = time();
            $redis->setex($cron_key, 600, $lock_time);
        }

        // Getting Internal Transaction array
        $internal_tx_select = array('id',
            'amount',
            'from_account',
            'to_account');

        $internal_txs = DB::table('internal_transactions')
            ->select($internal_tx_select)
            ->where('daemon_server_id', $daemon_server_id)
            ->where('coin', $coin)
            ->where('status', 'pending')
            ->get(); // The return result is an array of php standard class (objects).

        // Redirect if there is no pending internal transactions
        if (empty($internal_txs)) {
            // Unlock CRON Process
            $redis->del($cron_key);

            return 'No pending Internal Transactions on ' . $coin . ' at Daemon Server ' . $daemon_server_id . '.';
        }

        // Extend Maximum Execution Time
        set_time_limit(900);

        // Getting Coin Daemon
        $coin_json_rpc = $coin . '_JSON_RPC_DS_' . $daemon_server_id;
        $json_rpc_url = 'http://' . $_ENV[$coin_json_rpc] . '/';
        $coind = new jsonRPCClient($json_rpc_url);

        // Processing the internal transactions
        $all = array();
        $completed = array();
        $failed = array();

        foreach ($internal_txs as $internal_tx) {
            // CoinDaemon::move
            $move = false;
            $all[] = $internal_tx->id;

            try {
                $move = $coind->move($internal_tx->from_account, $internal_tx->to_account, floatval($internal_tx->amount));
            } catch (Exception $e) {
                $error = $e->getMessage();
                $custom_error = 'internal_transaction_process_error: failed internal_transaction id ' . $internal_tx->id . ' from ' . $coin . ' Daemon at Daemon Server '. $daemon_server_id;
                Log::error($custom_error);
                Log::error($error);
                $failed[] = $internal_tx->id;
            }

            if ($move == true) {
                $completed[] = $internal_tx->id;
            }
        }

        // Mass InternalTransaction update

        if (empty($failed)) {
            $completed_ids = implode(",", $completed);
            DB::statement(DB::raw(
                "UPDATE internal_transactions SET status = 'completed', ".
                "updated_at = now() WHERE id IN (" . $completed_ids . ")"));
        } elseif (empty($completed)) {
            $failed_ids = implode(",", $failed);
            DB::statement(DB::raw(
                "UPDATE internal_transactions SET status = 'failed', ".
                "updated_at = now() WHERE id IN (" . $failed_ids . ")"));
            // TODO Email Error Notification to crypteonnet@gmail.com
        } else {
            $completed_ids = implode(",", $completed);
            $failed_ids = implode(",", $failed);
            $all_ids = implode(",", $all);
            DB::statement(DB::raw(
                "UPDATE internal_transactions SET status = CASE " .
                "WHEN id IN (" . $completed_ids . ") THEN 'completed' " .
                "WHEN id IN (" . $failed_ids . ") THEN 'failed' END, " .
                "updated_at = now() WHERE id IN (" . $all_ids . ")"));
            // TODO Email Error Notification to crypteonnet@gmail.com
        }

        // Unlock CRON Process
        $redis->del($cron_key);

        // Return Finish Message
        return 'Internal Transaction Process completed on ' . $coin . ' at Daemon Server ' . $daemon_server_id . '.';

    }

}