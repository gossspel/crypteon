<?php

class RawTransactionsController extends BaseController {

    public function __construct() {
        $this->beforeFilter('basic.once');
    }

    public function getProcess($coin, $daemon_server_id) {
        // Check if Admin
        $user = Auth::user();
        if ($user->group != 'admin') {
            return Redirect::to('markets/secure-view')
                ->with('message', "Oops, we couldn't find what you're looking for.")
                ->with('message-level', 'alert-info');
        }

        // Check if server is in Maintenance mode
        $redis = Redis::connection();

        if ($redis->get('server_mode') == 'maintenance') {
            return 'Server is on maintenance mode. Raw Transaction Process is postponed.';
        }

        // Check if CRON Process is locked
        $cron_key = 'CRON_raw_transactions_' . $coin;

        if ($redis->exists($cron_key)) {
            $custom_error = 'raw_transaction_process_aborted: locked CRON process in ' . $coin . ' Daemon at Daemon Server '. $daemon_server_id;

            // Email to report error
            $mail_data = array('error_message' => $custom_error);
            Mail::later(10, 'emails.admin.error', $mail_data, function($message)
            {
                $message->to('crypteonnet@gmail.com', 'Crypteon')->subject('Error on server');
            });

            Log::error($custom_error);
            return $custom_error;
        } else {
            // Lock CRON Process
            $lock_time = time();
            $redis->setex($cron_key, 600, $lock_time);
        }

        // Getting Raw Transaction array
        $raw_tx_select = array('id',
            'txid');

        $raw_txs = DB::table('raw_transactions')
            ->select($raw_tx_select)
            ->where('daemon_server_id', $daemon_server_id)
            ->where('coin', $coin)
            ->where('status', 'pending')
            ->get(); // The return result is an array of php standard class (objects).

        // Redirect if there is no pending raw transactions
        if (empty($raw_txs)) {
            // Unlock CRON Process
            $redis->del($cron_key);

            return 'No pending Raw Transactions on ' . $coin . ' at Daemon Server ' . $daemon_server_id . '.';
        }

        // Getting Coin Daemon
        $coin_json_rpc = $coin . '_JSON_RPC_DS_' . $daemon_server_id;
        $json_rpc_url = 'http://' . $_ENV[$coin_json_rpc] . '/';
        $coind = new jsonRPCClient($json_rpc_url);

        // Processing the internal transactions
        $cof = array(); // array of ids that is either confirmed or failed.
        $confirmed = array();
        $failed = array();

        foreach ($raw_txs as $raw_tx) {
            // Coindaemon:gettransaction
            $tx['confirmations'] = 0;

            try {
                $tx = $coind->gettransaction($raw_tx->txid);
            } catch (Exception $e) {
                $error = $e->getMessage();
                $custom_error = 'raw_transaction_process_error: failed gettransaction with txid ' . $raw_tx->id . ' from ' . $coin . ' Daemon at Daemon Server '. $daemon_server_id;
                Log::error($custom_error);
                Log::error($error);
                $cof[] = $raw_tx->id;
                $failed[] = $raw_tx->id;
            }

            if ($tx['confirmations'] >= 1) {
                $cof[] = $raw_tx->id;
                $confirmed[] = $raw_tx->id;
            }
        }

        // Redirect if there is no pending raw transactions
        if (empty($cof)) {
            // Unlock CRON Process
            $redis->del($cron_key);

            return 'No confirmed Raw Transactions on ' . $coin . ' at Daemon Server ' . $daemon_server_id . '.';
        }

        if (empty($failed)) {
            $confirmed_ids = implode(",", $confirmed);

            // Mass RawTransaction Update
            DB::statement(DB::raw(
                "UPDATE raw_transactions SET status = 'confirmed', " .
                "updated_at = now() WHERE id IN (" . $confirmed_ids . ")"
            ));

            // Mass Withdrawal Update
            DB::statement(DB::raw(
                "UPDATE withdrawals SET status = 'completed', " .
                "updated_at = now() WHERE raw_transaction_id IN (" . $confirmed_ids . ")"
            ));
        } elseif (empty($confirmed)) {
            $failed_ids = implode(",", $failed);

            // Mass RawTransaction Update
            DB::statement(DB::raw(
                "UPDATE raw_transactions SET status = 'failed', " .
                "updated_at = now() WHERE id IN (" . $failed_ids . ")"
            ));

            // Mass Withdrawal Update
            DB::statement(DB::raw(
                "UPDATE withdrawals SET status = 'failed, retrying', " .
                "updated_at = now() WHERE raw_transaction_id IN (" . $failed_ids . ")"
            ));

        } else {
            $confirmed_ids = implode(",", $confirmed);
            $failed_ids = implode(",", $failed);
            $cof_ids = implode(",", $cof);

            // Mass RawTransaction Update
            DB::statement(DB::raw(
                "UPDATE raw_transactions SET status = CASE " .
                "WHEN id IN (" . $confirmed_ids . ") THEN 'confirmed' " .
                "WHEN id IN (" . $failed_ids . ") THEN 'failed' END, " .
                "updated_at = now() WHERE id IN (" . $cof_ids . ")"
            ));

            // Mass Withdrawal Update
            DB::statement(DB::raw(
                "UPDATE withdrawals SET status = CASE " .
                "WHEN raw_transaction_id IN (" . $confirmed_ids . ") THEN 'completed' " .
                "WHEN raw_transaction_id IN (" . $failed_ids . ") THEN 'failed, retrying' END, " .
                "updated_at = now() WHERE raw_transaction_id IN (" . $cof_ids . ")"
            ));

        }

        // Unlock CRON Process
        $redis->del($cron_key);

        // Return Finish Message
        return 'Raw Transaction Process completed on ' . $coin . ' at Daemon Server ' . $daemon_server_id . '.';

    }

}