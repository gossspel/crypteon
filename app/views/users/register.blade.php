<div class="row">
    <div class="col-lg-12 basic-mainframe">
        <!-- Include Alert Message here, better than showing it in the layout to more easily control css -->
        @include('row-alert')
    </div>
    <div class="col-lg-6 col-lg-offset-3">
        {{ HTML::script('js/responsive_recaptcha.js') }}
        @section('css')
            {{ HTML::style('css/responsive_recaptcha.css') }}
        @stop

        <div class="panel panel-default div-form-top">
            <div class="panel-heading div-form-title">
                Account Registration
            </div>
            <div class="panel-body">
                {{ Form::open(array('url'=>'users/create', 'class'=>'form-signup')) }}
                <div class="form-group">
                    {{ Form::label('invitation_code', 'Invitation Code') }}
                    {{ Form::text('invitation_code', null, array('class'=>'form-control', 'placeholder'=>'Invitation Code')) }}
                </div>
                <div class="form-group">
                    {{ Form::label('firstname', 'First Name') }}
                    {{ Form::text('firstname', null, array('class'=>'form-control', 'placeholder'=>'First Name')) }}
                </div>
                @if (!($errors->isEmpty()) && !empty($errors->first('firstname')))
                <div class="popover bottom">
                    <div class="arrow"></div>
                    <h3 class="popover-title">First Name Error</h3>
                    <div class="popover-content">
                        <p>{{ $errors->first('firstname') }}</p>
                    </div>
                </div>
                @endif
                <div class="form-group">
                    {{ Form::label('lastname', 'Last Name') }}
                    {{ Form::text('lastname', null, array('class'=>'form-control', 'placeholder'=>'Last Name')) }}
                </div>
                @if (!($errors->isEmpty()) && !empty($errors->first('lastname')))
                <div class="popover bottom">
                    <div class="arrow"></div>
                    <h3 class="popover-title">Last Name Error</h3>
                    <div class="popover-content">
                        <p>{{ $errors->first('lastname') }}</p>
                    </div>
                </div>
                @endif
                <div class="form-group">
                    {{ Form::label('email', 'Email') }}
                    {{ Form::text('email', null, array('class'=>'form-control', 'placeholder'=>'Email')) }}
                </div>
                @if (!($errors->isEmpty()) && !empty($errors->first('email')))
                <div class="popover bottom">
                    <div class="arrow"></div>
                    <h3 class="popover-title">Email Error</h3>
                    <div class="popover-content">
                        <p>{{ $errors->first('email') }}</p>
                    </div>
                </div>
                @endif
                <div class="form-group">
                    {{ Form::label('password', 'Password') }}
                    {{ Form::password('password', array('class'=>'form-control', 'placeholder'=>'Password')) }}
                </div>
                @if (!($errors->isEmpty()) && !empty($errors->first('password')))
                <div class="popover bottom">
                    <div class="arrow"></div>
                    <h3 class="popover-title">Password Error</h3>
                    <div class="popover-content">
                        <p>{{ $errors->first('password') }}</p>
                    </div>
                </div>
                @endif
                <div class="form-group">
                    {{ Form::label('password_confirmation', 'Password Confirmation') }}
                    {{ Form::password('password_confirmation', array('class'=>'form-control', 'placeholder'=>'Confirm Password')) }}
                </div>
                @if (!($errors->isEmpty()) && !empty($errors->first('password_confirmation')))
                <div class="popover bottom">
                    <div class="arrow"></div>
                    <h3 class="popover-title">Password Confirmation Error</h3>
                    <div class="popover-content">
                        <p>{{ $errors->first('password_confirmation') }}</p>
                    </div>
                </div>
                @endif
                <div class="form-group">
                    @include('recaptcha')
                </div>
                {{ Form::submit('Register', array('class'=>'btn btn-success btn-block'))}}
                {{ Form::close() }}
            </div>
        </div>


    </div>
</div>