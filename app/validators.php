<?php

// Check if market exists for Orders, no longer used, but good example as Validator custom extensions
Validator::extend('market', function($attribute, $value, $parameters)
{
    $market = Market::find($value);
    if (empty($market)) {
        return false;
    } else {
        return true;
    }

}, 'The market does not exist, please refresh and try again.');

// Check if password is correct
Validator::extend('passcheck', function($attribute, $value, $parameters)
{
    $user = Auth::user();
    $hashedPassword = $user->password;

    if (Hash::check($value, $hashedPassword)) {
        return true;
    } else {
        return false;
    }

}, 'The password is incorrect, please re-enter and try again.');

// Check Order minimum rate
Validator::extend('minrate', function($attribute, $value, $parameters)
{
    if ($value > 0.00000001) {
        return true;
    } else {
        return false;
    }

}, 'Minimum price must be greater than 0.00000010.');