<?php

class MarketsController extends BaseController {

    protected $layout = "layouts.test";

    public function __construct() {
        $this->beforeFilter('csrf', array('on'=>'post'));
        $this->beforeFilter('auth', array('except'=>array('getView')));
    }

    // Function to add new coin info to Redis
    public function getCoinInit($daemon_server_id = null, $coin = null, $tx_fee = null, $bitcointalk_topic_decimal = null) {
        // Check if Admin
        $user = Auth::user();
        if ($user->group != 'admin') {
            return Redirect::to('markets/secure-view')
                ->with('message', "Oops, we couldn't find what you're looking for.")
                ->with('message-level', 'alert-info');
        }

        // Check if this coin is added already
        $redis = Redis::connection();
        $coin_test_key = $coin . '_coin';

        if ($redis->exists($coin_test_key)) {
            return Redirect::to('markets/secure-view')
                ->with('message', $coin . " has been already been added before!")
                ->with('message-level', 'alert-danger');
        } else {
            $redis->set($coin_test_key, 'online');
        }


        // Getting Coin Daemon
        $coin_json_rpc = $coin . '_JSON_RPC_DS_' . $daemon_server_id;
        $json_rpc_url = 'http://' . $_ENV[$coin_json_rpc] . '/';
        $coind = new jsonRPCClient($json_rpc_url);

        // Get COIN Default Account Address
        try {
            $return_addr = $coind->getaccountaddress('default_' . $coin);
        } catch (Exception $e) {
            // Log Error Redirect
            $error = $e->getMessage();
            $custom_error = 'coin_init_aborted: failed getaccountaddress default_COIN from ' . $coin . ' Daemon at Daemon Server '. $daemon_server_id;
            Log::error($custom_error);
            Log::error($error);
            return $custom_error;
        }

        // Get COIN Default FEE Account Address
        try {
            $coind->getaccountaddress('default_' . $coin . '_fee');
        } catch (Exception $e) {
            // Log Error Redirect
            $error = $e->getMessage();
            $custom_error = 'coin_init_aborted: failed getaccountaddress default_COIN_fee from ' . $coin . ' Daemon at Daemon Server '. $daemon_server_id;
            Log::error($custom_error);
            Log::error($error);
            return $custom_error;
        }

        // Saving initial coin data on Redis
        $string_coin_bitcointalk_link = $coin . '_bitcointalk_link';
        $bitcointalk_prefix = 'https://bitcointalk.org/index.php?topic=';
        $bitcointalk_link = $bitcointalk_prefix . $bitcointalk_topic_decimal;

        $redis->hset('coin_tx_fee', $coin, $tx_fee);
        $redis->rpush('available_coins', $coin);
        $redis->set($string_coin_bitcointalk_link, $bitcointalk_link);
        $redis->bgsave();

        return Redirect::to('markets/secure-view')
            ->with('message', $coin . " has been added successfully! Return address is " . $return_addr)
            ->with('message-level', 'alert-success');
    }

    public function getMarketInit($base = null, $coin = null, $min_amount = null) {
        // Check if Admin
        $user = Auth::user();
        if ($user->group != 'admin') {
            return Redirect::to('markets/secure-view')
                ->with('message', "Oops, we couldn't find what you're looking for.")
                ->with('message-level', 'alert-info');
        }

        // Setting initial Redis Data for this Market
        $redis = Redis::connection();
        $pair = $coin . '/' . $base;
        $market_prefix = $base . '_' . $coin . '_';
        $market_test_key = $market_prefix . 'market';

        if ($redis->exists($market_test_key)) {
            return Redirect::to('markets/secure-view')
                ->with('message', $pair . " market has been already been added before!")
                ->with('message-level', 'alert-danger');
        } else {
            $redis->set($market_test_key, 'online');
        }

        $string_market_rate_current = $market_prefix . 'rate_current';
        $string_market_delta_percent = $market_prefix . 'delta_percent';
        $string_market_movement = $market_prefix . 'movement';
        $string_market_last_refreshed = $market_prefix . 'last_refreshed';
        $string_market_last_rate = $market_prefix . 'last_rate';
        $string_market_open_rate = $market_prefix . 'open_rate';
        $string_market_rate_daily_high = $market_prefix . 'rate_daily_high';
        $string_market_rate_daily_low = $market_prefix . 'rate_daily_low';
        $string_market_min_amount = $market_prefix . 'min_amount';
        $string_market_yesterday_rate_list = $market_prefix . 'yesterday_rate_list';
        $string_market_volume_daily = $market_prefix . 'volume_daily';

        $unix_timestamp = time();

        $redis->hset('market_rate_current', $string_market_rate_current, '0.00000000');
        $redis->hset('market_delta_percent', $string_market_delta_percent, '0.00');
        $redis->hset('market_movement', $string_market_movement, 'same');
        $redis->set($string_market_last_refreshed, $unix_timestamp);
        $redis->set($string_market_last_rate, '0.00000000');
        $redis->set($string_market_open_rate, '0.00000000');
        $redis->set($string_market_rate_daily_high, '0.00000000');
        $redis->set($string_market_rate_daily_low, '0.00000000');
        $redis->set($string_market_volume_daily, '0.00000000');
        $redis->set($string_market_min_amount, $min_amount);

        for ($i = 1; $i <= 144; $i++) {
            $redis->rpush($string_market_yesterday_rate_list, '0.00000000');
        }

        $redis->bgsave();

        // Inserting Market into Database;
        $market = new Market;
        $market->coin = $coin;
        $market->base = $base;
        $market->fee_rate = 0.00140000;
        $market->pair = $pair;
        $market->save();

        return Redirect::to('markets/secure-view')
            ->with('message', $pair . " market has been added successfully!")
            ->with('message-level', 'alert-success');
    }

    public function getView($id = null) {
        // Handling Parameter
        if (is_numeric($id)) {
            $id = ltrim($id, '0');
            $count = Market::count();
            if ($id > $count) {
                $id = 1;
            }
        } else {
            $id = 1;
        }

        // Redirect Authentication User to Secure View
        if (Auth::check()) {
            return Redirect::action('MarketsController@getSecureView', array($id));
        }

        // Get Order history in this market
        $finished_orders_select = array('id', 'bought_amount', 'sold_amount', 'created_at', 'rate', 'mode');
        $finished_orders = DB::table('transient_orders')->select($finished_orders_select)->where('market_id', $id)->orderBy('created_at', 'desc')->take(20)->get();

        $market = Market::find($id);
        $coin = $market->coin;
        $base = $market->base;
        $pair = $market->pair;

        // Get Market Last Rate
        $redis = Redis::connection();
        $market_prefix = $base . '_' . $coin . '_';
        $string_market_last_rate = $market_prefix . 'last_rate';
        $string_market_volume_daily = $market_prefix . 'volume_daily';
        $string_market_rate_daily_high = $market_prefix . 'rate_daily_high';
        $string_market_rate_daily_low = $market_prefix . 'rate_daily_low';
        $string_market_bitcointalk_link = $coin . '_bitcointalk_link';
        $market_last_rate = $redis->get($string_market_last_rate);
        $market_volume_daily = $redis->get($string_market_volume_daily);
        $market_rate_daily_high = $redis->get($string_market_rate_daily_high);
        $market_rate_daily_low = $redis->get($string_market_rate_daily_low);
        $market_bitcointalk_link = $redis->get($string_market_bitcointalk_link);

        if (empty($market_last_rate)) {
            $market_last_rate = '0.00000000';
        }

        // Get Market Suggested Min Amount
        $string_market_min_amount = $base . '_' . $coin . '_min_amount';
        $market_min_amount = $redis->get($string_market_min_amount);

        if (empty($market_min_amount)) {
            $market_min_amount = '0.00000000';
        }

        // Get Market Fee Rate
        if ($market->fee_mode == 'normal') {
            $fee_rate = '0.0014';
        } else {
            $fee_rate = $market->fee_rate;
        }

        $fee_rate_percent = bcmul($fee_rate, '100', 2);

        $market_array = array('coin' => $coin, 'base' => $base, 'pair' => $pair, 'id' => $id, 'fee_rate' => $fee_rate,
            'fee_rate_percent' => $fee_rate_percent, 'last_rate' => $market_last_rate, 'min_amount' => $market_min_amount,
            'volume_daily' => $market_volume_daily, 'rate_daily_high' => $market_rate_daily_high,
            'rate_daily_low' => $market_rate_daily_low, 'bitcointalk_link' => $market_bitcointalk_link
        );

        $sell_orders = DB::table('orders')->select(DB::raw('rate, SUM(current_sell_amount) as sum_sell, SUM(current_buy_amount) as sum_buy'))
            ->where('sell_coin', '=', $coin)
            ->where('buy_coin', '=', $base)
            ->where('status', '=', 'open')
            ->groupBy('rate')
            ->orderBy('rate', 'asc')
            ->get();

        $buy_orders = DB::table('orders')->select(DB::raw('rate, SUM(current_sell_amount) as sum_sell, SUM(current_buy_amount) as sum_buy'))
            ->where('sell_coin', '=', $base)
            ->where('buy_coin', '=', $coin)
            ->where('status', '=', 'open')
            ->groupBy('rate')
            ->orderBy('rate', 'desc')
            ->get();

        // Set content for layout
        $register_link = HTML::secureLink('users/register', 'register');
        $login_link = HTML::secureLink('users/login', 'login');

        $this->layout->content = View::make('markets.view')
            ->with('register_link', $register_link)
            ->with('login_link', $login_link)
            ->with('market_array', $market_array)
            ->with('finished_orders', $finished_orders)
            ->with('sell_orders', $sell_orders)
            ->with('buy_orders', $buy_orders);

        /*
         * View Design Info:
         * Row 1 - Graph
         * Row 2 - 2 columns, one buy coin form with top sell order below, one sell coin form with top buy order below.
         * Row 3 - Last 200 history (Transient Orders)
         *
         * Mobile Fluid Design Drop Down Panel
         *
         * 1st level - Graph
         * 2nd level - Buy coin form with top sell order below
         * 3rd level - Sell coin form with top buy order below
         * 4th level - History
         */
    }

    public function getSecureView($id = null) {
        // Handling Parameter
        if (is_numeric($id)) {
            $count = Market::count();
            if ($id > $count) {
                $id = 1;
            }
        } else {
            $id = 1;
        }

        $user = Auth::user();
        $user_id = $user->getAuthIdentifier();

        // Get all orders of that user, then group them into "open" orders, "canceled" orders, "completed" orders
        $orders_select = array('id',
            'buy_coin',
            'sell_coin',
            'original_sell_amount',
            'original_buy_amount',
            'current_sell_amount',
            'current_buy_amount',
            'completed_percentage',
            'created_at',
            'status',
            'rate',
            'mode',
            'sold_amount',
            'bought_amount',
            'pending_fee',
            'collected_fee');
        $orders = DB::table('orders')->select($orders_select)->where('user_id', $user_id)->where('market_id', $id)->orderBy('created_at', 'desc')->get();

        // Get Order history in this market
        $finished_orders_select = array('id', 'bought_amount', 'sold_amount', 'created_at', 'rate', 'mode');
        $finished_orders = DB::table('transient_orders')->select($finished_orders_select)->where('market_id', $id)->orderBy('created_at', 'desc')->take(20)->get();

        $open_orders = array();
        $cancel_orders = array();
        $completed_orders = array();

        // Divide the orders into groups:
        foreach ($orders as $order) {
            switch ($order->status) {
                case 'open':
                    $open_orders[] = $order;
                    break;
                case 'canceled':
                    $cancel_orders[] = $order;
                    break;
                case 'completed':
                    $completed_orders[] = $order;
                    break;
            }
        }

        $market = Market::find($id);
        $coin = $market->coin;
        $base = $market->base;
        $pair = $market->pair;

        // Get Market Last Rate
        $redis = Redis::connection();
        $market_prefix = $base . '_' . $coin . '_';
        $string_market_last_rate = $market_prefix . 'last_rate';
        $string_market_volume_daily = $market_prefix . 'volume_daily';
        $string_market_rate_daily_high = $market_prefix . 'rate_daily_high';
        $string_market_rate_daily_low = $market_prefix . 'rate_daily_low';
        $string_market_bitcointalk_link = $coin . '_bitcointalk_link';
        $market_last_rate = $redis->get($string_market_last_rate);
        $market_volume_daily = $redis->get($string_market_volume_daily);
        $market_rate_daily_high = $redis->get($string_market_rate_daily_high);
        $market_rate_daily_low = $redis->get($string_market_rate_daily_low);
        $market_bitcointalk_link = $redis->get($string_market_bitcointalk_link);

        if (empty($market_last_rate)) {
            $market_last_rate = '0.00000000';
        }

        // Get Market Suggested Min Amount
        $string_market_min_amount = $base . '_' . $coin . '_min_amount';
        $market_min_amount = $redis->get($string_market_min_amount);

        if (empty($market_min_amount)) {
            $market_min_amount = '0.00000000';
        }

        if ($market->fee_mode == 'normal') {
            if ($user->status == 'active') {
                $fee_rate = $user->fee_rate;
            } else {
                $account_cache_key = $user->email . '_cache';
                $fee_rate = $redis->hget($account_cache_key, 'fee_rate');
            }
        } else {
            $fee_rate = $market->fee_rate;
        }

        $fee_rate_percent = bcmul($fee_rate, '100', 2);

        $market_array = array('coin' => $coin, 'base' => $base, 'pair' => $pair, 'id' => $id, 'fee_rate' => $fee_rate,
            'fee_rate_percent' => $fee_rate_percent, 'last_rate' => $market_last_rate, 'min_amount' => $market_min_amount,
            'volume_daily' => $market_volume_daily, 'rate_daily_high' => $market_rate_daily_high,
            'rate_daily_low' => $market_rate_daily_low, 'bitcointalk_link' => $market_bitcointalk_link
        );

        $sell_orders = DB::table('orders')->select(DB::raw('rate, SUM(current_sell_amount) as sum_sell, SUM(current_buy_amount) as sum_buy'))
            ->where('sell_coin', '=', $coin)
            ->where('buy_coin', '=', $base)
            ->where('status', '=', 'open')
            ->groupBy('rate')
            ->orderBy('rate', 'asc')
            ->get();

        $buy_orders = DB::table('orders')->select(DB::raw('rate, SUM(current_sell_amount) as sum_sell, SUM(current_buy_amount) as sum_buy'))
            ->where('sell_coin', '=', $base)
            ->where('buy_coin', '=', $coin)
            ->where('status', '=', 'open')
            ->groupBy('rate')
            ->orderBy('rate', 'desc')
            ->get();

        // Set content for layout
        $this->layout->content = View::make('markets.secure-view')
            ->with('market_array', $market_array)
            ->with('finished_orders', $finished_orders)
            ->with('open_orders', $open_orders)
            ->with('canceled_orders', $cancel_orders)
            ->with('completed_orders', $completed_orders)
            ->with('sell_orders', $sell_orders)
            ->with('buy_orders', $buy_orders);
        
        /*
         * View Design Info:
         * Row 1 - Graph
         * Row 2 - 2 columns, one buy coin form with top sell order below, one sell coin form with top buy order below.
         * Row 3 - Last 200 history (Transient Orders)
         *
         * Mobile Fluid Design Drop Down Panel
         *
         * 1st level - Graph
         * 2nd level - Buy coin form with top sell order below
         * 3rd level - Sell coin form with top buy order below
         * 4th level - History
         */
    }

    public function getBestSellRate($id) {
        /*
         * This is an API/reserved function for cron to pick up the lowest sell rate from open orders in a market.
         * Cron will access this function every minute to update lowest sell rate session key.
         * The lowest sell rate session key is used for setting the default order rate for buy orders.
         */
    }

    public function getBestBuyRate($id) {
        /*
         * This is an API/reserved function for cron to pick up the highest buy rate from open orders in a market.
         * Cron will access this function every minute to update highest buy rate session key.
         * The highest buy rate session key is used for setting the default order rate for sell orders.
         */
    }
}