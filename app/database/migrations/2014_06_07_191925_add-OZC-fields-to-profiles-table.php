<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOZCFieldsToProfilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('profiles', function(Blueprint $table)
        {
            $table->string('OZC_deposit_addr', 100)->nullable();
            $table->decimal('OZC_total_received', 20, 8)->default(0); // used for checking and inserting deposit records
            $table->decimal('OZC_available_balance', 20, 8)->default(0); // combine with OZC_order_balance, you will get OZC_current_balance
            $table->decimal('OZC_order_balance', 20, 8)->default(0);
            $table->string('OZC_activity', 20)->default('inactive');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('profiles', function(Blueprint $table)
        {
            $table->dropColumn('OZC_deposit_addr');
            $table->dropColumn('OZC_total_received');
            $table->dropColumn('OZC_available_balance');
            $table->dropColumn('OZC_order_balance');
            $table->dropColumn('OZC_activity');
        });
	}

}
