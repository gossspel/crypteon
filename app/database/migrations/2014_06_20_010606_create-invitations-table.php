<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvitationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('invitations', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('invitation_code', 50);
            $table->string('status', 20)->default('new'); // 'new' => new key that is sent out in email, 'used' => key that is used
            $table->string('email', 100)->unique();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('invitations');
	}

}
