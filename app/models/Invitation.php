<?php

class Invitation extends Eloquent {

    protected $table = 'invitations';

    public static $rules = array(
        'email'=>'required|email|unique:invitations'
    );
}