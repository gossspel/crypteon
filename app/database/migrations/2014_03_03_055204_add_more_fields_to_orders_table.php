<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreFieldsToOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('orders', function(Blueprint $table)
		{
            $table->string('buy_coin',20);
            $table->decimal('original_buy_amount',20,8);
            $table->decimal('current_buy_amount',20,8);
            $table->decimal('bought_amount',20,8);
            $table->string('sell_coin',20);
            $table->decimal('original_sell_amount',20,8);
            $table->decimal('current_sell_amount',20,8);
            $table->decimal('sold_amount',20,8);
            $table->decimal('completed_percentage',11,8);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('orders', function(Blueprint $table)
		{
            $table->dropColumn('buy_coin');
            $table->dropColumn('original_buy_amount');
            $table->dropColumn('current_buy_amount');
            $table->dropColumn('bought_amount');
            $table->dropColumn('sell_coin');
            $table->dropColumn('original_sell_amount');
            $table->dropColumn('current_sell_amount');
            $table->dropColumn('sold_amount');
            $table->dropColumn('completed_percentage');
		});
	}

}