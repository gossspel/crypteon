<div class="row">
    <div class="col-md-12 basic-mainframe">

        <!-- Include Alert Message here, better than showing it in the layout to more easily control css -->
        @include('alert')

        <!-- Page Title -->
        <div class="div-table-title">
            Order History
        </div>

        <!-- Nav tabs -->
        <ul class="nav nav-tabs nav-justified">
            <li class="{{ $open_class }}"><a href="#open" data-toggle="tab">All Open Orders</a></li>
            <li class="{{ $completed_class }}"><a href="#completed" data-toggle="tab">All Completed Orders</a></li>
            <li class="{{ $canceled_class }}"><a href="#canceled" data-toggle="tab">All Canceled Orders</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div id="open" class="tab-pane {{ $open_class }}">
                <div class="panel panel-default div-table div-table-no-max-height">
                    <table class="table table-striped table-hover table-bordered">
                        @if (!empty($open_orders))
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th class="hidden-xs">
                                        @if ($sort == 'desc')
                                            <a href="{{ url('users/order-history', array('open', 'asc')) }}">
                                                Market
                                                <span class="glyphicon glyphicon-chevron-down sort-icon"></span>
                                            </a>
                                        @else
                                            <a href="{{ url('users/order-history', array('open', 'desc')) }}">
                                                Market
                                                <span class="glyphicon glyphicon-chevron-up sort-icon"></span>
                                            </a>
                                        @endif
                                    </th>
                                    <th class="hidden-xs">Type</th>
                                    <th class="visible-md visible-lg">Amount</th>
                                    <th class="visible-md visible-lg">Rate</th>
                                    <th>Detail</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($open_orders as $open_order)
                                    <tr>
                                        <td>{{ $open_order->created_at }}</td>
                                        <td class="hidden-xs">{{ $open_order->pair }}</td>
                                        <td class="hidden-xs">{{ $open_order->mode }}</td>
                                        @if ($open_order->mode == 'Sell')
                                        <td class="visible-md visible-lg">{{ $open_order->original_sell_amount }}</td>
                                        @else
                                        <td class="visible-md visible-lg">{{ $open_order->original_buy_amount }}</td>
                                        @endif
                                        <td class="visible-md visible-lg">{{ $open_order->rate }}</td>
                                        <td>
                                            <!-- Button trigger modal -->
                                            <button class="btn btn-info" data-toggle="modal" data-target="#myModal-{{ $open_order->id }}">
                                                Show
                                            </button>

                                            <!-- Modal -->
                                            <div class="modal detail-modal fade" id="myModal-{{ $open_order->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel-{{ $open_order->id }}" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                            <h4 class="modal-title" id="myModalLabel-{{ $open_order->id }}">Order Detail</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <ul class="ul-form-panel">
                                                                <li class="li-form-panel">
                                                                    <span>Date:</span>
                                                                    <span class="span-form-panel up">{{ $open_order->created_at }}</span>
                                                                </li>
                                                                <li class="li-form-panel">
                                                                    <span>Status:</span>
                                                                    <span class="span-form-panel up">Open</span>
                                                                </li>
                                                                <li class="li-form-panel">
                                                                    <span>Market:</span>
                                                                    <span class="span-form-panel up">{{ $open_order->pair }}</span>
                                                                </li>
                                                                @if ($open_order->mode == 'Sell')
                                                                <li class="li-form-panel">
                                                                    <span>Type:</span>
                                                                    <span class="span-form-panel up">Sell {{ $open_order->sell_coin }} Buy {{ $open_order->buy_coin }}</span>
                                                                </li>
                                                                <li class="li-form-panel">
                                                                    <span>Rate:</span>
                                                                    <span class="span-form-panel up">{{ '1 ' . $open_order->sell_coin . ' = ' . $open_order->rate . ' ' . $open_order->buy_coin }}</span>
                                                                </li>
                                                                <li class="li-form-panel">
                                                                    <span>Pending Sell Amount:</span>
                                                                    <span class="span-form-panel up">{{ $open_order->current_sell_amount . ' ' . $open_order->sell_coin }}</span>
                                                                </li>
                                                                <li class="li-form-panel">
                                                                    <span>Pending Buy Amount:</span>
                                                                    <span class="span-form-panel up">{{ $open_order->current_buy_amount . ' ' . $open_order->buy_coin }}</span>
                                                                </li>
                                                                <li class="li-form-panel">
                                                                    <span>Sold Amount:</span>
                                                                    <span class="span-form-panel up">{{ $open_order->sold_amount . ' ' . $open_order->sell_coin }}</span>
                                                                </li>
                                                                <li class="li-form-panel">
                                                                    <span>Bought Amount:</span>
                                                                    <span class="span-form-panel up">{{ $open_order->bought_amount . ' ' . $open_order->buy_coin }}</span>
                                                                </li>
                                                                <li class="li-form-panel">
                                                                    <span>Pending Fee:</span>
                                                                    <span class="span-form-panel up">{{ $open_order->pending_fee . ' ' . $open_order->buy_coin }}</span>
                                                                </li>
                                                                <li class="li-form-panel">
                                                                    <span>Collected Fee:</span>
                                                                    <span class="span-form-panel up">{{ $open_order->collected_fee . ' ' . $open_order->buy_coin }}</span>
                                                                </li>
                                                                @else
                                                                <li class="li-form-panel">
                                                                    <span>Type:</span>
                                                                    <span class="span-form-panel up">Buy {{ $open_order->buy_coin }} Sell {{ $open_order->sell_coin }}</span>
                                                                </li>
                                                                <li class="li-form-panel">
                                                                    <span>Rate:</span>
                                                                    <span class="span-form-panel up">{{ '1 ' . $open_order->buy_coin . ' = ' . $open_order->rate . ' ' . $open_order->sell_coin }}</span>
                                                                </li>
                                                                <li class="li-form-panel">
                                                                    <span>Pending Buy Amount:</span>
                                                                    <span class="span-form-panel up">{{ $open_order->current_buy_amount . ' ' . $open_order->buy_coin }}</span>
                                                                </li>
                                                                <li class="li-form-panel">
                                                                    <span>Pending Sell Amount:</span>
                                                                    <span class="span-form-panel up">{{ $open_order->current_sell_amount . ' ' . $open_order->sell_coin }}</span>
                                                                </li>
                                                                <li class="li-form-panel">
                                                                    <span>Bought Amount:</span>
                                                                    <span class="span-form-panel up">{{ $open_order->bought_amount . ' ' . $open_order->buy_coin }}</span>
                                                                </li>
                                                                <li class="li-form-panel">
                                                                    <span>Sold Amount:</span>
                                                                    <span class="span-form-panel up">{{ $open_order->sold_amount . ' ' . $open_order->sell_coin }}</span>
                                                                </li>
                                                                <li class="li-form-panel">
                                                                    <span>Pending Fee:</span>
                                                                    <span class="span-form-panel up">{{ $open_order->pending_fee . ' ' . $open_order->sell_coin }}</span>
                                                                </li>
                                                                <li class="li-form-panel">
                                                                    <span>Collected Fee:</span>
                                                                    <span class="span-form-panel up">{{ $open_order->collected_fee . ' ' . $open_order->sell_coin }}</span>
                                                                </li>
                                                                @endif
                                                                <li class="li-form-panel">
                                                                    <span>Completion percentage:</span>
                                                                    <span class="span-form-panel up">{{ $open_order->completed_percentage . ' %' }}</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            {{ Form::open(array('url'=>'orders/cancel', 'class'=>'form-order')) }}
                                            {{ Form::hidden('id', $open_order->id, array('class'=>'form-control')) }}
                                            {{ Form::submit('Cancel', array('class'=>'btn btn-danger'))}}
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        @else
                            <tr>
                                <td>You don't have any open orders.</td>
                            </tr>
                        @endif
                    </table>
                </div>
            </div>
            <div id="completed" class="tab-pane {{ $completed_class }}">
                <div class="panel panel-default div-table div-table-no-max-height">
                    <table class="table table-striped table-hover table-bordered">
                        @if (!empty($completed_orders))
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th class="hidden-xs">
                                        @if ($sort == 'desc')
                                        <a href="{{ url('users/order-history', array('completed', 'asc')) }}">
                                            Market
                                            <span class="glyphicon glyphicon-chevron-down sort-icon"></span>
                                        </a>
                                        @else
                                        <a href="{{ url('users/order-history', array('completed', 'desc')) }}">
                                            Market
                                            <span class="glyphicon glyphicon-chevron-up sort-icon"></span>
                                        </a>
                                        @endif
                                    </th>
                                    <th class="hidden-xs">Type</th>
                                    <th class="visible-md visible-lg">Amount</th>
                                    <th class="visible-md visible-lg">Rate</th>
                                    <th>Detail</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($completed_orders as $completed_order)
                                    <tr>
                                        <td>{{ $completed_order->created_at }}</td>
                                        <td class="hidden-xs">{{ $completed_order->pair }}</td>
                                        <td class="hidden-xs">{{ $completed_order->mode }}</td>
                                        @if ($completed_order->mode == 'Sell')
                                        <td class="visible-md visible-lg">{{ $completed_order->original_sell_amount }}</td>
                                        @else
                                        <td class="visible-md visible-lg">{{ $completed_order->original_buy_amount }}</td>
                                        @endif
                                        <td class="visible-md visible-lg">{{ $completed_order->rate }}</td>
                                        <td>
                                            <!-- Button trigger modal -->
                                            <button class="btn btn-info" data-toggle="modal" data-target="#myModal-completed-{{ $completed_order->id }}">
                                                Show
                                            </button>

                                            <!-- Modal -->
                                            <div class="modal detail-modal fade" id="myModal-completed-{{ $completed_order->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel-completed-{{ $completed_order->id }}" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                            <h4 class="modal-title" id="myModalLabel-completed-{{ $completed_order->id }}">Order Detail</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <ul class="ul-form-panel">
                                                                <li class="li-form-panel">
                                                                    <span>Date:</span>
                                                                    <span class="span-form-panel up">{{ $completed_order->created_at }}</span>
                                                                </li>
                                                                <li class="li-form-panel">
                                                                    <span>Status:</span>
                                                                    <span class="span-form-panel up">Completed</span>
                                                                </li>
                                                                <li class="li-form-panel">
                                                                    <span>Market:</span>
                                                                    <span class="span-form-panel up">{{ $completed_order->pair }}</span>
                                                                </li>
                                                                @if ($completed_order->mode == 'Sell')
                                                                <li class="li-form-panel">
                                                                    <span>Type:</span>
                                                                    <span class="span-form-panel up">Sell {{ $completed_order->sell_coin }} Buy {{ $completed_order->buy_coin }}</span>
                                                                </li>
                                                                <li class="li-form-panel">
                                                                    <span>Rate:</span>
                                                                    <span class="span-form-panel up">{{ '1 ' . $completed_order->sell_coin . ' = ' . $completed_order->rate . ' ' . $completed_order->buy_coin }}</span>
                                                                </li>
                                                                <li class="li-form-panel">
                                                                    <span>Pending Sell Amount:</span>
                                                                    <span class="span-form-panel up">{{ $completed_order->current_sell_amount . ' ' . $completed_order->sell_coin }}</span>
                                                                </li>
                                                                <li class="li-form-panel">
                                                                    <span>Pending Buy Amount:</span>
                                                                    <span class="span-form-panel up">{{ $completed_order->current_buy_amount . ' ' . $completed_order->buy_coin }}</span>
                                                                </li>
                                                                <li class="li-form-panel">
                                                                    <span>Sold Amount:</span>
                                                                    <span class="span-form-panel up">{{ $completed_order->sold_amount . ' ' . $completed_order->sell_coin }}</span>
                                                                </li>
                                                                <li class="li-form-panel">
                                                                    <span>Bought Amount:</span>
                                                                    <span class="span-form-panel up">{{ $completed_order->bought_amount . ' ' . $completed_order->buy_coin }}</span>
                                                                </li>
                                                                <li class="li-form-panel">
                                                                    <span>Pending Fee:</span>
                                                                    <span class="span-form-panel up">{{ $completed_order->pending_fee . ' ' . $completed_order->buy_coin }}</span>
                                                                </li>
                                                                <li class="li-form-panel">
                                                                    <span>Collected Fee:</span>
                                                                    <span class="span-form-panel up">{{ $completed_order->collected_fee . ' ' . $completed_order->buy_coin }}</span>
                                                                </li>
                                                                @else
                                                                <li class="li-form-panel">
                                                                    <span>Type:</span>
                                                                    <span class="span-form-panel up">Buy {{ $completed_order->buy_coin }} Sell {{ $completed_order->sell_coin }}</span>
                                                                </li>
                                                                <li class="li-form-panel">
                                                                    <span>Rate:</span>
                                                                    <span class="span-form-panel up">{{ '1 ' . $completed_order->buy_coin . ' = ' . $completed_order->rate . ' ' . $completed_order->sell_coin }}</span>
                                                                </li>
                                                                <li class="li-form-panel">
                                                                    <span>Pending Buy Amount:</span>
                                                                    <span class="span-form-panel up">{{ $completed_order->current_buy_amount . ' ' . $completed_order->buy_coin }}</span>
                                                                </li>
                                                                <li class="li-form-panel">
                                                                    <span>Pending Sell Amount:</span>
                                                                    <span class="span-form-panel up">{{ $completed_order->current_sell_amount . ' ' . $completed_order->sell_coin }}</span>
                                                                </li>
                                                                <li class="li-form-panel">
                                                                    <span>Bought Amount:</span>
                                                                    <span class="span-form-panel up">{{ $completed_order->bought_amount . ' ' . $completed_order->buy_coin }}</span>
                                                                </li>
                                                                <li class="li-form-panel">
                                                                    <span>Sold Amount:</span>
                                                                    <span class="span-form-panel up">{{ $completed_order->sold_amount . ' ' . $completed_order->sell_coin }}</span>
                                                                </li>
                                                                <li class="li-form-panel">
                                                                    <span>Pending Fee:</span>
                                                                    <span class="span-form-panel up">{{ $completed_order->pending_fee . ' ' . $completed_order->sell_coin }}</span>
                                                                </li>
                                                                <li class="li-form-panel">
                                                                    <span>Collected Fee:</span>
                                                                    <span class="span-form-panel up">{{ $completed_order->collected_fee . ' ' . $completed_order->sell_coin }}</span>
                                                                </li>
                                                                @endif
                                                                <li class="li-form-panel">
                                                                    <span>Completion percentage:</span>
                                                                    <span class="span-form-panel up">{{ $completed_order->completed_percentage . ' %' }}</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        @else
                            <tr>
                                <td>You don't have any completed orders.</td>
                            </tr>
                        @endif
                    </table>
                </div>
            </div>
            <div id="canceled" class="tab-pane {{ $canceled_class }}">
                <div class="panel panel-default div-table div-table-no-max-height">
                    <table class="table table-striped table-hover table-bordered">
                        @if (!empty($canceled_orders))
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th class="hidden-xs">
                                        @if ($sort == 'desc')
                                        <a href="{{ url('users/order-history', array('canceled', 'asc')) }}">
                                            Market
                                            <span class="glyphicon glyphicon-chevron-down sort-icon"></span>
                                        </a>
                                        @else
                                        <a href="{{ url('users/order-history', array('canceled', 'desc')) }}">
                                            Market
                                            <span class="glyphicon glyphicon-chevron-up sort-icon"></span>
                                        </a>
                                        @endif
                                    </th>
                                    <th class="hidden-xs">Type</th>
                                    <th class="visible-md visible-lg">Amount</th>
                                    <th class="visible-md visible-lg">Rate</th>
                                    <th>Detail</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($canceled_orders as $canceled_order)
                                <tr>
                                    <td>{{ $canceled_order->created_at }}</td>
                                    <td class="hidden-xs">{{ $canceled_order->pair }}</td>
                                    <td class="hidden-xs">{{ $canceled_order->mode }}</td>
                                    @if ($canceled_order->mode == 'Sell')
                                    <td class="visible-md visible-lg">{{ $canceled_order->original_sell_amount }}</td>
                                    @else
                                    <td class="visible-md visible-lg">{{ $canceled_order->original_buy_amount }}</td>
                                    @endif
                                    <td class="visible-md visible-lg">{{ $canceled_order->rate }}</td>
                                    <td>
                                        <!-- Button trigger modal -->
                                        <button class="btn btn-info" data-toggle="modal" data-target="#myModal-canceled-{{ $canceled_order->id }}">
                                            Show
                                        </button>

                                        <!-- Modal -->
                                        <div class="modal detail-modal fade" id="myModal-canceled-{{ $canceled_order->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel-canceled-{{ $canceled_order->id }}" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                        <h4 class="modal-title" id="myModalLabel-canceled-{{ $canceled_order->id }}">Order Detail</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <ul class="ul-form-panel">
                                                            <li class="li-form-panel">
                                                                <span>Date:</span>
                                                                <span class="span-form-panel up">{{ $canceled_order->created_at }}</span>
                                                            </li>
                                                            <li class="li-form-panel">
                                                                <span>Status:</span>
                                                                <span class="span-form-panel up">Canceled</span>
                                                            </li>
                                                            <li class="li-form-panel">
                                                                <span>Market:</span>
                                                                <span class="span-form-panel up">{{ $canceled_order->pair }}</span>
                                                            </li>
                                                            @if ($canceled_order->mode == 'Sell')
                                                            <li class="li-form-panel">
                                                                <span>Type:</span>
                                                                <span class="span-form-panel up">Sell {{ $canceled_order->sell_coin }} Buy {{ $canceled_order->buy_coin }}</span>
                                                            </li>
                                                            <li class="li-form-panel">
                                                                <span>Rate:</span>
                                                                <span class="span-form-panel up">{{ '1 ' . $canceled_order->sell_coin . ' = ' . $canceled_order->rate . ' ' . $canceled_order->buy_coin }}</span>
                                                            </li>
                                                            <li class="li-form-panel">
                                                                <span>Returned Amount (To Wallet Balance):</span>
                                                                <span class="span-form-panel up">{{ $canceled_order->current_sell_amount . ' ' . $canceled_order->sell_coin }}</span>
                                                            </li>
                                                            <li class="li-form-panel">
                                                                <span>Sold Amount:</span>
                                                                <span class="span-form-panel up">{{ $canceled_order->sold_amount . ' ' . $canceled_order->sell_coin }}</span>
                                                            </li>
                                                            <li class="li-form-panel">
                                                                <span>Bought Amount:</span>
                                                                <span class="span-form-panel up">{{ $canceled_order->bought_amount . ' ' . $canceled_order->buy_coin }}</span>
                                                            </li>
                                                            <li class="li-form-panel">
                                                                <span>Collected Fee:</span>
                                                                <span class="span-form-panel up">{{ $canceled_order->collected_fee . ' ' . $canceled_order->buy_coin }}</span>
                                                            </li>
                                                            @else
                                                            <li class="li-form-panel">
                                                                <span>Type:</span>
                                                                <span class="span-form-panel up">Buy {{ $canceled_order->buy_coin }} Sell {{ $canceled_order->sell_coin }}</span>
                                                            </li>
                                                            <li class="li-form-panel">
                                                                <span>Rate:</span>
                                                                <span class="span-form-panel up">{{ '1 ' . $canceled_order->buy_coin . ' = ' . $canceled_order->rate . ' ' . $canceled_order->sell_coin }}</span>
                                                            </li>
                                                            <li class="li-form-panel">
                                                                <span>Returned Amount (To Wallet Balance):</span>
                                                                <span class="span-form-panel up">{{ $canceled_order->current_sell_amount . ' ' . $canceled_order->sell_coin }}</span>
                                                            </li>
                                                            <li class="li-form-panel">
                                                                <span>Returned Fee (To Wallet Balance):</span>
                                                                <span class="span-form-panel up">{{ $canceled_order->pending_fee . ' ' . $canceled_order->sell_coin }}</span>
                                                            </li>
                                                            <li class="li-form-panel">
                                                                <span>Bought Amount:</span>
                                                                <span class="span-form-panel up">{{ $canceled_order->bought_amount . ' ' . $canceled_order->buy_coin }}</span>
                                                            </li>
                                                            <li class="li-form-panel">
                                                                <span>Sold Amount:</span>
                                                                <span class="span-form-panel up">{{ $canceled_order->sold_amount . ' ' . $canceled_order->sell_coin }}</span>
                                                            </li>
                                                            <li class="li-form-panel">
                                                                <span>Collected Fee:</span>
                                                                <span class="span-form-panel up">{{ $canceled_order->collected_fee . ' ' . $canceled_order->sell_coin }}</span>
                                                            </li>
                                                            @endif
                                                            <li class="li-form-panel">
                                                                <span>Completion percentage:</span>
                                                                <span class="span-form-panel up">{{ $canceled_order->completed_percentage . ' %' }}</span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        @else
                            <tr>
                                <td>You don't have any canceled orders.</td>
                            </tr>
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>