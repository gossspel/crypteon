<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<p>Hi Team,</p>

		<p>
            We have an error on the server. Please check out the message below:
		</p>
        <p>
            {{ $error_message }}
        </p>
        <p>
            <div>Regards,</div>
            <div>Crypteon Mailer</div>
        </p>
	</body>
</html>