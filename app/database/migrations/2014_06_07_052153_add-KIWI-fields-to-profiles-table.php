<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKIWIFieldsToProfilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('profiles', function(Blueprint $table)
        {
            $table->string('KIWI_deposit_addr', 100)->nullable();
            $table->decimal('KIWI_total_received', 20, 8)->default(0); // used for checking and inserting deposit records
            $table->decimal('KIWI_available_balance', 20, 8)->default(0); // combine with KIWI_order_balance, you will get KIWI_current_balance
            $table->decimal('KIWI_order_balance', 20, 8)->default(0);
            $table->string('KIWI_activity', 20)->default('inactive');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('profiles', function(Blueprint $table)
        {
            $table->dropColumn('KIWI_deposit_addr');
            $table->dropColumn('KIWI_total_received');
            $table->dropColumn('KIWI_available_balance');
            $table->dropColumn('KIWI_order_balance');
            $table->dropColumn('KIWI_activity');
        });
	}

}
