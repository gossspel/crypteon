<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCoinActivityFieldsToProfilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('profiles', function(Blueprint $table)
        {
            $table->string('LTC_activity', 20)->default('inactive');
            $table->string('DOGE_activity', 20)->default('inactive');
            $table->string('MOON_activity', 20)->default('inactive');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('profiles', function(Blueprint $table)
        {
            $table->dropColumn('LTC_activity');
            $table->dropColumn('DOGE_activity');
            $table->dropColumn('MOON_activity');
        });
	}

}
