<div class="row">
    <div class="col-lg-12 basic-mainframe">
        <!-- Include Alert Message here, better than showing it in the layout to more easily control css -->
        @include('row-alert')
    </div>
    <div class="col-lg-6 col-lg-offset-3">
        <div class="panel panel-default div-form-top">
            <div class="panel-heading div-form-title">
                Withdraw {{ $coin }}
            </div>
            <div class="panel-body">
                {{ Form::open(array('url'=>'withdrawals/create', 'class'=>'form-order')) }}
                <div class="form-group">
                    {{ Form::label('to_address', $coin . ' Receiving Address') }}
                    {{ Form::text('to_address', null, array('class'=>'form-control', 'placeholder'=>$coin . ' Receiving Address')) }}
                </div>
                @if (!($errors->isEmpty()) && !empty($errors->first('to_address')))
                <div class="popover bottom">
                    <div class="arrow"></div>
                    <h3 class="popover-title">{{ $coin }} Receiving Address Error</h3>
                    <div class="popover-content">
                        <p>{{ $errors->first('to_address') }}</p>
                    </div>
                </div>
                @endif
                <div class="form-group">
                    {{ Form::label('withdrawal_amount', 'Withdrawal Amount') }}
                    {{ Form::text('withdrawal_amount', '0.00000000', array('class'=>'form-control', 'id'=>'amount', 'placeholder'=>'Withdrawal Amount')) }}
                </div>
                @if (!($errors->isEmpty()) && !empty($errors->first('withdrawal_amount')))
                <div class="popover bottom">
                    <div class="arrow"></div>
                    <h3 class="popover-title">Amount Error</h3>
                    <div class="popover-content">
                        <p>{{ $errors->first('withdrawal_amount') }}</p>
                    </div>
                </div>
                @endif
                <div class="form-group">
                    {{ Form::label('password', 'Account Password') }}
                    {{ Form::password('password', array('class'=>'form-control', 'placeholder'=>'Account Password')) }}
                </div>
                @if (!($errors->isEmpty()) && !empty($errors->first('password')))
                <div class="popover bottom">
                    <div class="arrow"></div>
                    <h3 class="popover-title">Account Password Error</h3>
                    <div class="popover-content">
                        <p>{{ $errors->first('password') }}</p>
                    </div>
                </div>
                @endif
                <div class="well">
                    <ul class="ul-form-panel">
                        <li class="li-form-panel">
                            <span>Change in {{ $coin }} Balance</span>
                            <span class="span-form-panel" id="coin">- 0.00000000 {{ $coin }}</span>
                        </li>
                        <li class="li-form-panel">
                            <span>Tx Fee ({{ $tx_fee }} {{ $coin }})</span>
                            <span class="span-form-panel" id="tx-fee">- 0.00000000 {{ $coin }}</span>
                        </li>
                        <li class="li-form-panel">
                            <span>Net Change in {{ $coin }} Balance</span>
                            <span class="span-form-panel down" id="net">- 0.00000000 {{ $coin }}</span>
                        </li>
                    </ul>
                </div>
                {{ Form::hidden('coin', $coin, array('class'=>'form-control')) }}
                {{ Form::submit('Withdraw ' . $coin, array('class'=>'btn btn-success btn-block'))}}
                {{ HTML::link('users/wallet', 'Cancel', array('class' => 'btn btn-default btn-block')) }}
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>

@section('plscripts')
    <script>
        $(document).ready(function() {
            // Initial Calculation
            var raw_coin_init_string = $("#amount").val();
            var raw_coin_init = parseFloat(raw_coin_init_string);
            var raw_tx_fee_init_string = "{{ $tx_fee }}";
            var raw_tx_fee_init = parseFloat(raw_tx_fee_init_string);
            var cooked_net_init = (raw_coin_init + raw_tx_fee_init).toFixed(8);
            var cooked_coin_init = raw_coin_init.toFixed(8);
            var cooked_tx_fee_init = raw_tx_fee_init.toFixed(8);

            $("#coin").text("- " + cooked_coin_init + " {{ $coin }}");
            $("#tx-fee").text("- " + cooked_tx_fee_init + " {{ $coin }}");
            $("#net").text("- " + cooked_net_init + " {{ $coin }}");

            // Live Calculation
            $("#amount").keyup(function() {
                var raw_coin_string = $(this).val();
                var raw_coin = parseFloat(raw_coin_string);
                var raw_tx_fee_string = "{{ $tx_fee }}";
                var raw_tx_fee = parseFloat(raw_tx_fee_string);

                if ((jQuery.type(raw_coin) !== 'number') || (isNaN(raw_coin))) {
                    var raw_coin = 0;
                }

                var cooked_net = (raw_coin + raw_tx_fee).toFixed(8);
                var cooked_coin = raw_coin.toFixed(8);
                var cooked_tx_fee = raw_tx_fee.toFixed(8);

                $("#coin").text("- " + cooked_coin + " {{ $coin }}");
                $("#tx-fee").text("- " + cooked_tx_fee + " {{ $coin }}");
                $("#net").text("- " + cooked_net + " {{ $coin }}");
            });
            $("#amount").focusout(function() {
                var raw = $(this).val();
                var raw = parseFloat(raw);
                if ((jQuery.type(raw) !== 'number') || (isNaN(raw))) {
                    var raw = 0;
                }
                var cooked_b = raw.toFixed(8);
                $(this).val(cooked_b);
            });
        });
    </script>
@stop
<?php

// TODO: Show available balance
// TODO: Show table of withdrawl history, with sortable link and pagination

?>