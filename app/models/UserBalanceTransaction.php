<?php

class UserBalanceTransaction extends Eloquent {

    protected $table = 'user_balance_transactions';

    // There exists a 1 to 1 relationship between UserBalanceTransaction{Entity} and Desposit{Entity}
    public function deposit()
    {
        return $this->belongsTo('Deposit');
    }

    // There exists a 1 to 1 relationship between UserBalanceTransaction{Entity} and Withdrawal{Entity}
    public function withdrawal()
    {
        return $this->belongsTo('Withdrawal');
    }

    // There exists a N to 1  relationship between UserBalanceTransaction{Entity} and Order{Entity}
    public function order()
    {
        return $this->belongsTo('Order');
    }

    // There exists a N to 1  relationship between UserBalanceTransaction{Entity} and User{Entity}
    public function user()
    {
        return $this->belongsTo('User');
    }

}