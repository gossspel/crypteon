<div class="row">
    <div class="col-md-12 basic-mainframe">
        <!-- Include Alert Message here, better than showing it in the layout to more easily control css -->
        @include('alert')
    </div>
    <div class="col-lg-6 col-lg-offset-3">
        <div class="panel panel-default div-form-top">
            <div class="panel-heading div-form-title">
                Update Account Password
            </div>
            <div class="panel-body">
                {{ Form::open(array('url'=>'password/reset', 'class'=>'form-signin', 'role'=>'form')) }}
                {{ Form::hidden('token', $token, array('class'=>'form-control')) }}
                <div class="form-group">
                    {{ Form::label('email', 'Account Email') }}
                    {{ Form::text('email', null, array('class'=>'form-control', 'placeholder'=>'Enter Email Address')) }}
                </div>
                <div class="form-group">
                    {{ Form::label('password', 'New Password') }}
                    {{ Form::password('password', array('class'=>'form-control', 'placeholder'=>'Enter New Password')) }}
                </div>
                @if (!($errors->isEmpty()) && !empty($errors->first('password')))
                <div class="popover bottom">
                    <div class="arrow"></div>
                    <h3 class="popover-title">New Password Error</h3>
                    <div class="popover-content">
                        <p>{{ $errors->first('password') }}</p>
                    </div>
                </div>
                @endif
                <div class="form-group">
                    {{ Form::label('password_confirmation', 'New Password Confirmation') }}
                    {{ Form::password('password_confirmation', array('class'=>'form-control', 'placeholder'=>'Confirm New Password')) }}
                </div>
                @if (!($errors->isEmpty()) && !empty($errors->first('password_confirmation')))
                <div class="popover bottom">
                    <div class="arrow"></div>
                    <h3 class="popover-title">New Password Confirmation Error</h3>
                    <div class="popover-content">
                        <p>{{ $errors->first('password_confirmation') }}</p>
                    </div>
                </div>
                @endif
                {{ Form::submit('Reset Password', array('class'=>'btn btn-success btn-block'))}}
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>