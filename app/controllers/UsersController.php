<?php

class UsersController extends BaseController {

    protected $layout = "layouts.test";

    public function __construct() {
        $this->beforeFilter('csrf', array('on'=>'post'));
        $this->beforeFilter('auth', array('except'=>array('getActivate', 'getRegister', 'getLogin', 'getLogout', 'getLoginVerify', 'postLogin', 'postLoginVerify', 'postCreate', 'getAbout', 'getFees', 'getTerms')));
    }

    public function getAbout() {
        $this->layout->content = View::make('users.about');
    }

    public function getFees() {
        $this->layout->content = View::make('users.fees');
    }

    public function getTerms() {
        $this->layout->content = View::make('users.terms');
    }

    public function getRegister() {
        // Set content for layout
        $this->layout->content = View::make('users.register');
    }

    public function getLogin() {
        // Check to see if user is already logged in
        if (Auth::check()) {
            return Redirect::intended('/')->with('message', 'You are logged in!')
                ->with('message-level', 'alert-success');
        }

        $this->layout->content = View::make('users.login');
    }

    public function getLogout() {
        Auth::logout();
        return Redirect::to('/')
            ->with('message', 'Your are now logged out!')
            ->with('message-level', 'alert-info');
    }

    public function getDepositHistory() {
        $user = Auth::user();
        $user_id = $user->id;

        $deposits_select = array('id',
            'coin',
            'deposit_amount',
            'status',
            'txid',
            'created_at');

        $deposits = DB::table('deposits')
            ->select($deposits_select)
            ->where('user_id', $user_id)
            ->orderBy('created_at', 'desc')
            ->get();

        $this->layout->content = View::make('users.deposit-history')
            ->with('deposits', $deposits);
    }

    public function getWithdrawalHistory() {
        $user = Auth::user();
        $user_id = $user->id;

        $withdrawals_select = array('id',
            'coin',
            'to_address',
            'withdrawal_amount',
            'tx_fee',
            'status',
            'created_at');

        $withdrawals = DB::table('withdrawals')
            ->select($withdrawals_select)
            ->where('user_id', $user_id)
            ->orderBy('created_at', 'desc')
            ->get(); // The return result is an array of php standard class (object).

        $this->layout->content = View::make('users.withdrawal-history')
            ->with('withdrawals', $withdrawals);
    }

    public function getOrderHistory($order_type = null, $sort_direction = null) {
        $user = Auth::user();
        $user_id = $user->id;

        // Handling parameters
        switch ($order_type) {
            case 'open':
                $open_class = 'active';
                $canceled_class = '';
                $completed_class = '';
                break;
            case 'canceled':
                $open_class = '';
                $canceled_class = 'active';
                $completed_class = '';
                break;
            case 'completed':
                $open_class = '';
                $canceled_class = '';
                $completed_class = 'active';
                break;
            default:
                $open_class = 'active';
                $canceled_class = '';
                $completed_class = '';
        }

        // Get all orders of that user, then group them into "open" orders, "canceled" orders, "completed" orders
        $orders_select = array('orders.id',
            'markets.pair',
            'orders.buy_coin',
            'orders.sell_coin',
            'orders.sold_amount',
            'orders.bought_amount',
            'orders.original_sell_amount',
            'orders.original_buy_amount',
            'orders.current_sell_amount',
            'orders.current_buy_amount',
            'orders.completed_percentage',
            'orders.pending_fee',
            'orders.collected_fee',
            'orders.created_at',
            'orders.mode',
            'orders.status',
            'orders.rate');

        if (is_null($sort_direction)) {
            $sort = 'asc';
            $orders = DB::table('orders')
                ->join('markets', 'orders.market_id', '=', 'markets.id')
                ->select($orders_select)
                ->where('orders.user_id', $user_id)
                ->orderBy('created_at', 'desc')
                ->get(); // The return result is an array of php standard class (object).
        } elseif ($sort_direction == 'desc') {
            $sort = 'desc';
            $orders = DB::table('orders')
                ->join('markets', 'orders.market_id', '=', 'markets.id')
                ->select($orders_select)
                ->where('orders.user_id', $user_id)
                ->orderBy('markets.pair', $sort)
                ->orderBy('created_at', 'desc')
                ->get(); // The return result is an array of php standard class (object).
        } else {
            $sort = 'asc';
            $orders = DB::table('orders')
                ->join('markets', 'orders.market_id', '=', 'markets.id')
                ->select($orders_select)
                ->where('orders.user_id', $user_id)
                ->orderBy('markets.pair', $sort)
                ->orderBy('created_at', 'desc')
                ->get(); // The return result is an array of php standard class (object).
        }

        $open_orders = array();
        $canceled_orders = array();
        $completed_orders = array();

        // Divide the orders into groups:
        foreach ($orders as $order) {
            switch ($order->status) {
                case 'open':
                    $open_orders[] = $order;
                    break;
                case 'canceled':
                    $canceled_orders[] = $order;
                    break;
                case 'completed':
                    $completed_orders[] = $order;
                    break;
            }
        }

        $this->layout->content = View::make('users.order-history')
            ->with('sort', $sort)
            ->with('open_class', $open_class)
            ->with('canceled_class', $canceled_class)
            ->with('completed_class', $completed_class)
            ->with('open_orders', $open_orders)
            ->with('canceled_orders', $canceled_orders)
            ->with('completed_orders', $completed_orders);
    }

    public function getWallet() {
        $redis = Redis::connection();
        $available_coins = $redis->lrange('available_coins', 0, -1);
        sort($available_coins);
        $user = Auth::user();
        $auth_user_id = $user->getAuthIdentifier();
        $profile = DB::table('profiles')
            ->where('id', $auth_user_id)
            ->first();
        $this->layout->content = View::make('users.wallet')
            ->with('available_coins', $available_coins)
            ->with('profile', $profile);
    }

    public function getWithdraw($coin = null) {
        $redis = Redis::connection();
        $available_coins = $redis->lrange('available_coins', 0, -1);
        $tx_fee = $redis->hget('coin_tx_fee', $coin);

        // Redirect if the coin parameter is not in the list of available coin
        if (!in_array($coin, $available_coins)) {
            return Redirect::action('UsersController@getWallet')
                ->with('message', 'The parameter is invalid, please try again without altering the parameter')
                ->with('message-level', 'alert-danger');
        }

        $this->layout->content = View::make('users.withdraw')
            ->with('tx_fee', $tx_fee)
            ->with('coin', $coin);
    }

    public function postAjaxGenerate() {
        // DONETODO: Implement JSON alert message after returning response
        // DONETODO: assign all redirects with message-level
        // DONETODO: Apply Bootstrap style on all alert message (Session or JSON)
        // DONETODO: Only allow AJAX call in this action.
        // DONETODO: Need to check to see if Litecoind is running, if not, redirect to Registration Temporarily Closed.

        // Check if Ajax Request
        if (!(Request::ajax())) {
            return Redirect::action('UsersController@getWallet')
                ->with('message', 'Error detected, please refresh and try again')
                ->with('message-level', 'alert-danger');
        }

        $json_error = false;
        $skip_to_end = false;
        $message = 'Error detected, please refresh and try again.';
        $redis = Redis::connection();
        $user = Auth::user();

        // Check if server is in Maintenance mode
        if ($redis->get('server_mode') == 'maintenance') {
            $json_error = true;
            $skip_to_end = true;
            $message = 'Crypteon is currently in maintenance mode. Most user related functions are disabled.';
        }

        // Check if User is logged in, if not, set Error Message.
        if (Auth::check() == false) {
            $json_error = true;
            $skip_to_end = true;
            $message = 'You have been logged out, please login and try again.';
        }

        // Check if deposit address spam
        if ($skip_to_end == false) {
            $auth_user_id = $user->id;
            $daemon_server_id = $user->daemon_server_id;
            $cdau_key = 'create_deposit_address_user_' . $auth_user_id;
            $cdau_key_count = $redis->incr($cdau_key);
            $redis->expire($cdau_key, 2);

            if ($cdau_key_count > 1) {
                $json_error = true;
                $skip_to_end = true;
                $message = 'For the sake of security and data integrity, deposit-address request is limited to 1 per 2 seconds. Please try again later.';
            }
        }

        // Check if User is activated
        if ($user->status != 'active') {
            $json_error = true;
            $skip_to_end = true;
            $link = HTML::link('users/activation-email', 'Resend activation email.');
            $message = 'Please activate your account to have full access on Crypteon. ' . $link;
        }

        // Check if COIN is valid
        if ($skip_to_end == false) {
            $available_coins = $redis->lrange('available_coins', 0, -1);
            $coin = Input::get('coin');

            if (!in_array($coin, $available_coins)) {
                $json_error = true;
                $skip_to_end = true;
            }
        }

        // Generate / Get the Deposit Address
        if ($skip_to_end == false) {

            // Check to see if {COIN_deposit_addr} is not null
            $string_deposit_addr = $coin . '_deposit_addr';
            $string_coin_activity = $coin . '_activity';

            $profile = DB::table('profiles')
                ->select($string_deposit_addr)
                ->where('id', $auth_user_id)
                ->first();

            if (!empty($profile->{$string_deposit_addr})) {
                $deposit_addr = $profile->{$string_deposit_addr};
                $message = 'The ' . $coin . ' deposit address has been generated!';
            } else {
                // Getting Coin Daemon
                $coin_json_rpc = $coin . '_JSON_RPC_DS_' . $daemon_server_id;
                $json_rpc_url = 'http://' . $_ENV[$coin_json_rpc] . '/';
                $coind = new jsonRPCClient($json_rpc_url);
                $daemon_error = false;
                $message = 'The ' . $coin . ' deposit address has been generated!';

                try {
                    $deposit_addr = $coind->getaccountaddress($user->email);
                } catch (Exception $e) {
                    // Log Error Redirect
                    $error = $e->getMessage();
                    $custom_error = $coin . '_deposit_address_generation_aborted: failed getaccountaddress on user ' . $auth_user_id . ' Daemon at Daemon Server '. $daemon_server_id;
                    Log::error($custom_error);
                    Log::error($error);
                    $daemon_error = true;
                    $json_error = true;
                    $message = 'The ' . $coin . ' daemon is busy. Please try again later.';
                }

                if ($daemon_error == false) {
                    // Update {COIN_deposit_addr} in profiles
                    DB::table('profiles')
                        ->where('id', $auth_user_id)
                        ->update(array($string_deposit_addr => $deposit_addr, $string_coin_activity => 'active'));
                }
            }
        }

        // Can't redirect in Ajax Request because it expects JSON
        if ($json_error == false) {
            return Response::json(array('deposit_addr' => $deposit_addr, 'message' => $message, 'mode' => 'alert-success'));
        } else {
            return Response::json(array('message' => $message, 'mode' => 'alert-danger'));
        }
    }

    public function postCreate() {
        // Check if server is in Maintenance mode
        $redis = Redis::connection();

        if ($redis->get('server_mode') == 'maintenance') {
            return View::make('maintenance');
        }

        // Recaptcha Validation
        $private_key = $_ENV['RECAPTCHA_PRIVATE'];
        $remote_addr = $_SERVER["REMOTE_ADDR"];
        $recaptcha_challenge_field = Input::get('recaptcha_challenge_field');
        $recaptcha_response_field = Input::get('recaptcha_response_field');

        $resp = recaptcha_check_answer ($private_key,
            $remote_addr,
            $recaptcha_challenge_field,
            $recaptcha_response_field);

        if (!$resp->is_valid) {
            // What happens when the CAPTCHA was entered incorrectly
            $error_message = "The reCAPTCHA wasn't entered correctly. Please try it again." . "(reCAPTCHA said: " . $resp->error . ")";
            return Redirect::back()->with('message', $error_message)
                ->with('message-level', 'alert-danger')
                ->withInput();
        }

        // Invitation Code Validation
        $code = Input::get('invitation_code');
        $invitation = DB::table('invitations')->select('id', 'status')->where('invitation_code', $code)->first();

        if (empty($invitation)) {
            // Invalid invitation code
            return Redirect::to('users/register')
                ->with('message', 'Invalid invitation code, please try again!')
                ->with('message-level', 'alert-danger')
                ->withInput();
        } else {
            // Invitation code has been used before
            if ($invitation->status != 'new') {
                return Redirect::to('users/register')
                    ->with('message', 'Invitation code has already been used before.')
                    ->with('message-level', 'alert-danger')
                    ->withInput();
            }
        }

        // Form Fields Validation
        $validator = Validator::make(Input::all(), User::$rules);

        if ($validator->passes()) {
            // Update invitation code status
            $invitation_update = Invitation::find($invitation->id);
            $invitation_update->status = 'used';
            $invitation_update->save();

            // Prepare Activation Token
            $email = Input::get('email');
            $last_name = Input::get('lastname');
            $first_name = Input::get('firstname');
            $password = Input::get('password');
            $value = str_shuffle(sha1($email.$password.microtime(true)));
            $hash_key = $GLOBALS['config']['key'];
            $activation_token =  hash_hmac('sha1', $value, $hash_key);
            $activation_time = time();

            // validation has passed, save user in DB
            $user = new User;
            $user->firstname = $first_name;
            $user->lastname = $last_name;
            $user->email = $email;
            $user->daemon_server_id = 1; // Will change in the future when we have more servers
            $user->password = Hash::make(Input::get('password'));
            $user->save();

            // Create Google Two Factor Authentication Secret
            $secret = GoogleAuthenticator::userRandomKey();

            // Create the associated profile
            $profile = new Profile;
            $profile->id = $user->id;
            $profile->two_factor_secret = $secret;
            $profile->email = $user->email;
            $profile->activation_time = $activation_time;
            $profile->activation_token = $activation_token;
            $profile->daemon_server_id = 1; // Will change in the future when we have more servers

            // Check to see if promo applies
            if ($user->id <= 500) {
                $profile->fee_rate = 0.0012;
            } else {
                $profile->fee_rate = 0.0014;
            }

            $profile->save();

            // Create Redis Cache Profile
            $account_cache_key = $email . '_cache';
            $redis->hset($account_cache_key, 'two_factor_authentication', 'off');
            $redis->hset($account_cache_key, 'two_factor_secret', $secret);
            $redis->hset($account_cache_key, 'fee_rate', $profile->fee_rate);
            $redis->hset($account_cache_key, 'id', $user->id);
            $redis->bgsave();

            // Send Activation Email
            $activation_link = URL::to('users/activate', array($activation_time, $activation_token));

            $user_data = array('last_name' => $last_name,
                'first_name' => $first_name,
                'link' => $activation_link);

            $email_data = array('email' => $email,
                'name' => $first_name . ' ' . $last_name);

            Mail::later(10, 'emails.auth.welcome', $user_data, function($message) use ($email_data)
            {
                $message->to($email_data['email'], $email_data['name'])->subject('Welcome to Crypteon!');
            });

            return Redirect::to('/')->with('message', 'Thanks for registering! Please activate your account through the activation link sent to your email, it might take a couple minutes to arrive.')
                ->with('message-level', 'alert-success');
        } else {
            // validation has failed, display error messages
            return Redirect::to('users/register')
                ->with('message', 'Unsuccessful form submit, please check the errors below.')
                ->with('message-level', 'alert-danger')
                ->withErrors($validator)
                ->withInput();
        }
    }

    public function getActivate($time = null, $token = null) {
        // Check access right
        if (Auth::check()) {
            $redirect_uri = 'markets/secure-view';
        } else {
            $redirect_uri = '/';
        }

        if (is_null($time) || is_null($token)) {
            return Redirect::to($redirect_uri)
                ->with('message', "Oops, we couldn't find what you're looking for.")
                ->with('message-level', 'alert-info');
        }

        $user = DB::table('users')
            ->join('profiles', 'users.id', '=', 'profiles.id')
            ->select('users.id', 'users.status', 'profiles.fee_rate')
            ->where('profiles.activation_time', $time)
            ->where('profiles.activation_token', $token)
            ->first();

        if (empty($user)) {
            sleep(2); // Simple approach to slow down brute-force activate
            return Redirect::to($redirect_uri)
                ->with('message', "Oops, we couldn't find what you're looking for.")
                ->with('message-level', 'alert-info');
        } elseif ($user->status == 'active') {
            return Redirect::to($redirect_uri)
                ->with('message', "This account activation link has been used and is no longer valid.")
                ->with('message-level', 'alert-info');
        }

        // Update status
        DB::table('users')
            ->where('id', $user->id)
            ->update(array('status' => 'active', 'fee_rate' => $user->fee_rate));

        // Login and Redirect
        If (!Auth::check()) {
            Auth::loginUsingId($user->id);
        }

        return Redirect::to('users/wallet')
            ->with('message', 'You have been successfully activated. Have fun trading on Crypteon!')
            ->with('message-level', 'alert-success');
    }

    public function getActivationEmail() {
        // Variable declaration
        $redis = Redis::connection();
        $user = Auth::user();
        $email = $user->email;
        $key = $email . '_activation_token';
        $lock_key = $email . '_activation_token_disabled';

        // Check if user is activated
        if ($user->status == 'active') {
            $message = 'This account has already been activated.';
            return Redirect::to('markets/secure-view')->with('message', $message)
                ->with('message-level', 'alert-info');
        }

        // Check if password reset is disabled
        if ($redis->exists($lock_key)) {
            $message = 'Activation token generation is temporarily disabled in this account, please try again later.';
            return Redirect::to('markets/secure-view')->with('message', $message)
                ->with('message-level', 'alert-danger');
        }


        // Temporarily lock account if there are at least 5 activation email resend attempts.
        if ($redis->exists($key)) {
            $current_count = $redis->get($key);
            if ($current_count >= 5) {
                $message = 'Activation token generation has been disabled for 30 minutes in this account because there are too many activation email requests.';
                $ip = Request::getClientIp();
                $redis->setex($lock_key, 1800, '1');
                $redis->rpush('activation_token_spammers', $email . ', ' . $ip);
                return Redirect::to('markets/secure-view')->with('message', $message)
                    ->with('message-level', 'alert-danger');
            } else {
                $redis->incr($key);
            }
        } else {
            $redis->incr($key);
            $redis->expire($key, 3600);
        }

        // Resend activation Email
        // DONETODO: Finish the detail of resending activation email
        $user_id = $user->id;
        $first_name = $user->firstname;
        $last_name = $user->lastname;
        $profile = DB::table('profiles')->select('activation_token', 'activation_time')->where('id', $user_id)->first();
        $activation_token =  $profile->activation_token;
        $activation_time = $profile->activation_time;
        $activation_link = URL::to('users/activate', array($activation_time, $activation_token));

        $user_data = array('last_name' => $last_name,
            'first_name' => $first_name,
            'link' => $activation_link);

        $email_data = array('email' => $email,
            'name' => $first_name . ' ' . $last_name);

        Mail::later(10, 'emails.auth.welcome', $user_data, function($message) use ($email_data)
        {
            $message->to($email_data['email'], $email_data['name'])->subject('Crypteon Activation');
        });

        return Redirect::to('markets/secure-view')->with('message', 'Activation email sent, it might take a couple minutes to arrive.')
            ->with('message-level', 'alert-success');
    }

    public function getSetting() {
        $user = Auth::user();
        $email = $user->email;
        $redis = Redis::connection();
        $account_cache_key = $email . '_cache';
        $secret = $redis->hget($account_cache_key, 'two_factor_secret');
        $mode = $redis->hget($account_cache_key, 'two_factor_authentication');
        $qr_link = GoogleAuthenticator::getQRcodeURL($email, "Crypteon", $secret);

        $this->layout->content = View::make('users.setting')
            ->with('secret', $secret)
            ->with('mode', $mode)
            ->with('qr_link', $qr_link);
    }

    public function postEnableAuthenticator() {
        // Check if server is in Maintenance mode
        $redis = Redis::connection();

        if ($redis->get('server_mode') == 'maintenance') {
            return View::make('maintenance');
        }

        // Form Fields Validation
        $user = Auth::user();
        $account_cache_key = $user->email . '_cache';
        $secret = $redis->hget($account_cache_key, 'two_factor_secret');
        $user_otp = Input::get('user_otp');
        $otp = GoogleAuthenticator::generate($secret, 30);

        if (empty($user_otp)) {
            $message = 'The 6 digit codes is required.';
        } else {
            $message = 'The 6 digits code you entered is invalid/expired (it is only valid for 30 seconds), please try again.';
        }

        if ($user_otp == $otp) {
            $redis->hset($account_cache_key, 'two_factor_authentication', 'on');
            return Redirect::to('users/setting')->with('message', 'Two factor authentication has been turned on successfully!')
                ->with('message-level', 'alert-success');
        } else {
            return Redirect::to('users/setting')->with('message', $message)
                ->with('message-level', 'alert-danger')
                ->with('two_factor_authentication_div', 'show');
        }
    }

    public function postDisableAuthenticator() {
        // Check if server is in Maintenance mode
        $redis = Redis::connection();

        if ($redis->get('server_mode') == 'maintenance') {
            return View::make('maintenance');
        }

        // Form Fields Validation
        $user = Auth::user();
        $account_cache_key = $user->email . '_cache';
        $secret = $redis->hget($account_cache_key, 'two_factor_secret');
        $user_otp= Input::get('user_otp');
        $otp = GoogleAuthenticator::generate($secret, 30);

        if (empty($user_otp)) {
            $message = 'The 6 digit codes is required.';
        } else {
            $message = 'The 6 digits code you entered is invalid/expired (it is only valid for 30 seconds), please try again.';
        }

        if ($user_otp == $otp) {
            $redis->hset($account_cache_key, 'two_factor_authentication', 'off');
            return Redirect::to('users/setting')->with('message', 'Two factor authentication has been turned off successfully!')
                ->with('message-level', 'alert-success');
        } else {
            return Redirect::to('users/setting')->with('message', $message)
                ->with('message-level', 'alert-danger')
                ->with('two_factor_authentication_div', 'show');
        }
    }

    public function postLogin() {
        // Check if server is in Maintenance mode
        $redis = Redis::connection();

        if ($redis->get('server_mode') == 'maintenance') {
            return View::make('maintenance');
        }

        $email = Input::get('email');
        $password = Input::get('password');
        $account_cache_key = $email . '_cache';
        $two_factor = $redis->hget($account_cache_key, 'two_factor_authentication');

        if ($two_factor == 'on') {
            if (Auth::validate(array('email'=>$email, 'password'=>$password))) {
                Session::set('login_verify_email', $email);
                return Redirect::to('users/login-verify')
                    ->with('message', 'Two factor authentication is enabled, please enter the 6 digits code generated by Google Authenticator to continue.')
                    ->with('message-level', 'alert-info');
            } else {
                sleep(2); // Simple approach to slow down brute-force login
                return Redirect::to('users/login')
                    ->with('message', 'Your username/password combination was incorrect.')
                    ->with('message-level', 'alert-danger')
                    ->withInput();
            }
        } else {
            if (Auth::attempt(array('email'=>$email, 'password'=>$password))) {
                return Redirect::intended('users/order-history/open/desc')
                    ->with('message', 'You are now logged in!')
                    ->with('message-level', 'alert-success');
            } else {
                sleep(2); // Simple approach to slow down brute-force login
                return Redirect::to('users/login')
                    ->with('message', 'Your username/password combination was incorrect.')
                    ->with('message-level', 'alert-danger')
                    ->withInput();
            }
        }
    }

    public function getLoginVerify() {
        if (Auth::check()) {
            $redirect_uri = 'markets/secure-view';
        } else {
            $redirect_uri = '/';
        }

        if (!Session::has('login_verify_email')) {
            return Redirect::to($redirect_uri)
                ->with('message', "Oops, we couldn't find what you're looking for.")
                ->with('message-level', 'alert-info');
        } else {
            $this->layout->content = View::make('users.login-verify');
        }
    }

    public function postLoginVerify() {
        // Check if server is in Maintenance mode
        $redis = Redis::connection();

        if ($redis->get('server_mode') == 'maintenance') {
            return View::make('maintenance');
        }

        if (!Session::has('login_verify_email')) {
            return Redirect::to('users/login')
                ->with('message', "Session has expired, please try again.")
                ->with('message-level', 'alert-info');
        } else {
            $email = Session::get('login_verify_email');
            $account_cache_key = $email . '_cache';
            $secret = $redis->hget($account_cache_key, 'two_factor_secret');
            $user_id = $redis->hget($account_cache_key, 'id');
            $user_otp = Input::get('user_otp');
            $otp = GoogleAuthenticator::generate($secret, 30);

            if ($user_otp == $otp) {
                Auth::loginUsingId($user_id);
                Session::forget('login_verify_email');
                return Redirect::intended('users/order-history/open/desc')
                    ->with('message', 'You are now logged in!')
                    ->with('message-level', 'alert-success');
            } else {
                Session::set('login_verify_email', $email);
                return Redirect::to('users/login-verify')->with('message', 'The 6 digits code you entered is invalid/expired (it is only valid for 30 seconds), please try again')
                    ->with('message-level', 'alert-danger');
            }
        }
    }

    public function postReset() {
        // Check if server is in Maintenance mode
        $redis = Redis::connection();

        if ($redis->get('server_mode') == 'maintenance') {
            return View::make('maintenance');
        }

        $user = Auth::user();
        $email = $user->email;
        $lock_key = $email . '_password_reset_disabled';
        $key = $email . '_password_reset';

        // Check if password reset is disabled
        if ($redis->exists($lock_key)) {
            $message = 'Password reset on this account is temporarily disabled, please try again later.';
            return Redirect::to('markets/secure-view')->with('message', $message)
                ->with('message-level', 'alert-danger');
        }

        // Temporarily lock account if there are at least 5 password reset attempts
        if ($redis->exists($key)) {
            $current_count = $redis->get($key);
            if ($current_count >= 5) {
                $message = 'Password reset on this account has been disabled for 30 minutes because there are too many password reset attempts.';
                $ip = Request::getClientIp();
                $redis->setex($lock_key, 1800, '1');
                $redis->rpush('password_reset_spammers', $email . ', ' . $ip);
                return Redirect::to('markets/secure-view')->with('message', $message)
                    ->with('message-level', 'alert-danger');
            } else {
                $redis->incr($key);
            }
        } else {
            $redis->incr($key);
            $redis->expire($key, 3600);
        }

        // Form Fields Validation
        $validator = Validator::make(Input::all(), User::$reset_rules);

        if (!($validator->passes())) {
            return Redirect::action('UsersController@getSetting')
                ->with('message', 'Unsuccessful form submit, please check the errors below.')
                ->with('message-level', 'alert-danger')
                ->withErrors($validator);
        }

        $new_password = Input::get('new_password');
        $user->password = Hash::make($new_password);
        $user->save();

        $message = 'Password has been updated successfully!';
        return Redirect::to('users/setting')->with('message', $message)
            ->with('message-level', 'alert-success');
    }
}
