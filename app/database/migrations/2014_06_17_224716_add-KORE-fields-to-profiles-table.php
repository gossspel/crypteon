<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKOREFieldsToProfilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('profiles', function(Blueprint $table)
        {
            $table->string('KORE_deposit_addr', 100)->nullable();
            $table->decimal('KORE_total_received', 20, 8)->default(0); // used for checking and inserting deposit records
            $table->decimal('KORE_available_balance', 20, 8)->default(0); // combine with KORE_order_balance, you will get KORE_current_balance
            $table->decimal('KORE_order_balance', 20, 8)->default(0);
            $table->string('KORE_activity', 20)->default('inactive');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('profiles', function(Blueprint $table)
        {
            $table->dropColumn('KORE_deposit_addr');
            $table->dropColumn('KORE_total_received');
            $table->dropColumn('KORE_available_balance');
            $table->dropColumn('KORE_order_balance');
            $table->dropColumn('KORE_activity');
        });
	}

}
