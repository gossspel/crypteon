<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Crypteon</title>

        <!-- CSS Section: Notice that the order is very important. -->
        <!-- The most bottom css file will hold priority if there exists identical selectors among these css files. -->

        <!-- Core CSS - Include with every page -->
        {{ HTML::style('css/bootstrap.min.css') }}
        {{ HTML::style('font-awesome/css/font-awesome.css') }}

        <!-- Main Crypteon CSS -->
        {{ HTML::style('css/main.css') }}

        <!-- SB Admin CSS - Include with every page -->
        {{ HTML::style('css/sb-admin.css') }}

        <!-- Custom Crypteon CSS for overwriting SB Admin CSS -->
        {{ HTML::style('css/custom.css') }}

        <!-- Page Level CSS -->
        @yield('css')
    </head>
    <body>
        <div id="wrapper">
            <nav class="navbar navbar-default navbar-fixed-top main-nav-bar" role="navigation" style="margin-bottom: 0">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{{ url('/') }}">
                        Crypteon
                        <i class="fa fa-random fa-fw" style="color: #FF6600;"></i>
                    </a>
                </div>
                <!-- /.navbar-header -->

                <ul class="nav navbar-top-links pull-left hidden-xs">
                    <li>{{ HTML::link('users/about', 'About') }}</li>
                    <li>{{ HTML::link('users/fees', 'Fees') }}</li>
                    <li>{{ HTML::link('http://crypteon.freshdesk.com/support/home', 'Support', array('target'=>'_blank')) }}</li>
                    <li>{{ HTML::link('users/terms', 'Terms') }}</li>
                </ul>

                <ul class="nav navbar-top-links navbar-right pull-right">
                    <li class="dropdown">
                        <a class="dropdown-toggle visible-xs" href="#" data-toggle="dropdown">
                            <i class="fa fa-link fa-fw"></i>
                            <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-crypteon-links">
                            <li>{{ HTML::link('users/about', 'About') }}</li>
                            <li>{{ HTML::link('users/fees', 'Fees') }}</li>
                            <li>{{ HTML::link('http://crypteon.freshdesk.com/support/home', 'Support', array('target'=>'_blank')) }}</li>
                            <li>{{ HTML::link('users/terms', 'Terms') }}</li>
                        </ul>
                    </li>
                    <?php $auth_check = Auth::check(); ?>
                    @if($auth_check == false)
                        <li>{{ HTML::link('users/register', 'Register') }}</li>
                        <li>{{ HTML::link('users/login', 'Login') }}</li>
                    @else
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-user fa-fw"></i>
                                    <?php $auth_user = Auth::user(); ?>
                                    {{ $auth_user->firstname . ' ' . $auth_user->lastname }}
                                <i class="fa fa-caret-down" style="padding-left: 5px;"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li>
                                    <a href="{{ url('users/wallet') }}">
                                        <i class="fa fa-money fa-fw"></i> Wallet
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('users/order-history') }}">
                                        <i class="fa fa-folder-open-o fa-fw"></i> Order History
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('users/deposit-history') }}">
                                        <i class="fa fa-folder-open-o fa-fw"></i> Deposit History
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('users/withdrawal-history') }}">
                                        <i class="fa fa-folder-open-o fa-fw"></i> Withdrawal History
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('users/setting') }}">
                                        <i class="fa fa-gear fa-fw"></i> Setting
                                    </a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="{{ url('users/logout') }}">
                                        <i class="fa fa-sign-out fa-fw"></i> Logout
                                    </a>
                                </li>
                            </ul>
                            <!-- /.dropdown-user -->
                        </li>
                        <!-- /.dropdown -->
                    @endif
                </ul>
            </nav>
            <!-- /.navbar-fixed-top -->
            <?php
                $redis = Redis::connection();

                // BTC MARKET DATA
                $btc_mrck = array(
                    'BTC_DOGE_rate_current',
                    'BTC_LTC_rate_current',
                    'BTC_KIWI_rate_current',
                    'BTC_OZC_rate_current',
                    'BTC_DRK_rate_current',
                    'BTC_CINNI_rate_current',
                    'BTC_KORE_rate_current'
                );
                $btc_market_rate = $redis->hmget('market_rate_current', $btc_mrck);
                $btc_dpk = array(
                    'BTC_DOGE_delta_percent',
                    'BTC_LTC_delta_percent',
                    'BTC_KIWI_delta_percent',
                    'BTC_OZC_delta_percent',
                    'BTC_DRK_delta_percent',
                    'BTC_CINNI_delta_percent',
                    'BTC_KORE_delta_percent'
                );
                $btc_delta_percent = $redis->hmget('market_delta_percent', $btc_dpk);
                $btc_mk = array(
                    'BTC_DOGE_movement',
                    'BTC_LTC_movement',
                    'BTC_KIWI_movement',
                    'BTC_OZC_movement',
                    'BTC_DRK_movement',
                    'BTC_CINNI_movement',
                    'BTC_KORE_movement'
                );
                $btc_movement = $redis->hmget('market_movement', $btc_mk);

                // LTC MARKET DATA
                $ltc_mrck = array(
                    'LTC_DOGE_rate_current'
                );
                $ltc_market_rate = $redis->hmget('market_rate_current', $ltc_mrck);
                $ltc_dpk = array(
                    'LTC_DOGE_delta_percent'
                );
                $ltc_delta_percent = $redis->hmget('market_delta_percent', $ltc_dpk);
                $ltc_mk = array(
                    'LTC_DOGE_movement'
                );
                $ltc_movement = $redis->hmget('market_movement', $ltc_mk);
            ?>
            <nav class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                    <h2>
                        <i class="fa fa-bar-chart-o fa-fw" style="color: #0088CC;"></i>
                        BTC Markets
                    </h2>
                    <ul class="nav nav-side-menu" id="side-menu">
                        <li class="active">
                            <a href="{{ url('markets/view', array('7')) }}">
                                <span class="coin">CINNI</span>
                                <span class="rate">{{ $btc_market_rate[5] }}</span>
                                <span class="pull-right {{ $btc_movement[5] }}">
                                    @if ($btc_movement[5] == 'up')
                                        <i class="fa fa-arrow-up fa-fw"></i>
                                    @elseif ($btc_movement[5] == 'down')
                                        <i class="fa fa-arrow-down fa-fw"></i>
                                    @endif
                                    {{ $btc_delta_percent[5] }}%
                                </span>
                            </a>
                        </li>
                        <li class="active">
                            <a href="{{ url('markets/view', array('1')) }}">
                                <span class="coin">DOGE</span>
                                <span class="rate">{{ $btc_market_rate[0] }}</span>
                                <span class="pull-right {{ $btc_movement[0] }}">
                                    @if ($btc_movement[0] == 'up')
                                        <i class="fa fa-arrow-up fa-fw"></i>
                                    @elseif ($btc_movement[0] == 'down')
                                        <i class="fa fa-arrow-down fa-fw"></i>
                                    @endif
                                    {{ $btc_delta_percent[0] }}%
                                </span>
                            </a>
                        </li>
                        <li class="active">
                            <a href="{{ url('markets/view', array('6')) }}">
                                <span class="coin">DRK</span>
                                <span class="rate">{{ $btc_market_rate[4] }}</span>
                                <span class="pull-right {{ $btc_movement[4] }}">
                                    @if ($btc_movement[4] == 'up')
                                        <i class="fa fa-arrow-up fa-fw"></i>
                                    @elseif ($btc_movement[4] == 'down')
                                        <i class="fa fa-arrow-down fa-fw"></i>
                                    @endif
                                    {{ $btc_delta_percent[4] }}%
                                </span>
                            </a>
                        </li>
                        <li class="active">
                            <a href="{{ url('markets/view', array('2')) }}">
                                <span class="coin">LTC</span>
                                <span class="rate">{{ $btc_market_rate[1] }}</span>
                                <span class="pull-right {{ $btc_movement[1] }}">
                                    @if ($btc_movement[1] == 'up')
                                        <i class="fa fa-arrow-up fa-fw"></i>
                                    @elseif ($btc_movement[1] == 'down')
                                        <i class="fa fa-arrow-down fa-fw"></i>
                                    @endif
                                    {{ $btc_delta_percent[1] }}%
                                </span>
                            </a>
                        </li>
                        <li class="active">
                            <a href="{{ url('markets/view', array('4')) }}">
                                <span class="coin">KIWI</span>
                                <span class="rate">{{ $btc_market_rate[2] }}</span>
                                <span class="pull-right {{ $btc_movement[2] }}">
                                    @if ($btc_movement[2] == 'up')
                                        <i class="fa fa-arrow-up fa-fw"></i>
                                    @elseif ($btc_movement[2] == 'down')
                                        <i class="fa fa-arrow-down fa-fw"></i>
                                    @endif
                                    {{ $btc_delta_percent[2] }}%
                                </span>
                            </a>
                        </li>
                        <li class="active">
                            <a href="{{ url('markets/view', array('8')) }}">
                                <span class="coin">KORE</span>
                                <span class="rate">{{ $btc_market_rate[6] }}</span>
                                <span class="pull-right {{ $btc_movement[6] }}">
                                    @if ($btc_movement[6] == 'up')
                                        <i class="fa fa-arrow-up fa-fw"></i>
                                    @elseif ($btc_movement[6] == 'down')
                                        <i class="fa fa-arrow-down fa-fw"></i>
                                    @endif
                                    {{ $btc_delta_percent[6] }}%
                                </span>
                            </a>
                        </li>
                        <li class="active">
                            <a href="{{ url('markets/view', array('5')) }}">
                                <span class="coin">OZC</span>
                                <span class="rate">{{ $btc_market_rate[3] }}</span>
                                <span class="pull-right {{ $btc_movement[3] }}">
                                    @if ($btc_movement[3] == 'up')
                                        <i class="fa fa-arrow-up fa-fw"></i>
                                    @elseif ($btc_movement[3] == 'down')
                                        <i class="fa fa-arrow-down fa-fw"></i>
                                    @endif
                                    {{ $btc_delta_percent[3] }}%
                                </span>
                            </a>
                        </li>
                    </ul>
                    <h2>
                        <i class="fa fa-bar-chart-o fa-fw" style="color: #0088CC;"></i>
                        LTC Markets
                    </h2>
                    <ul class="nav nav-side-menu">
                        <li class="active">
                            <a href="{{ url('markets/view', array('3')) }}">
                                <span class="coin">DOGE</span>
                                <span class="rate">{{ $ltc_market_rate[0] }}</span>
                                <span class="pull-right {{ $ltc_movement[0] }}">
                                    @if ($ltc_movement[0] == 'up')
                                        <i class="fa fa-arrow-up fa-fw"></i>
                                    @elseif ($ltc_movement[0] == 'down')
                                        <i class="fa fa-arrow-down fa-fw"></i>
                                    @endif
                                    {{ $ltc_delta_percent[0] }}%
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </nav>
            <!-- /.navbar-static-side -->

            <div id="page-wrapper">
                {{ $content }}
            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->

        <!-- Core Scripts - Include with every page -->
        {{ HTML::script('js/jquery-1.10.2.js') }}
        {{ HTML::script('js/bootstrap.min.js') }}
        {{ HTML::script('js/plugins/metisMenu/jquery.metisMenu.js') }}

        <!-- SB Admin Scripts - Include with every page -->
        {{ HTML::script('js/sb-admin.js') }}

        <!-- Page-Level Scripts -->
        @yield('plscripts')
    </body>
</html>